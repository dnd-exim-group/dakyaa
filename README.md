# Dakyaa


## Contents
- [About The Project](#about-the-project)
    - [Built With](#built-with)
- [Getting Started](#getting-started)
    - [Prerequisties](#prerequisties)
    - [Installation](#installation)
- [Git Flow](#git-flow)
- [Acknowledgements](#acknowledgements)
- [Contact](#contact)
- [License](#license)

<br/>

## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

Dakyaa is an aggregator website for International Courier Service. It eases and makes the process of international transport hassle-free and provides the best price in the market for individuals and small businesses.

### Built With

* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Laravel](https://laravel.com)

<br/>

## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

List of prerequisites you need to use the software and how to install them.

<!-- Download composer from [here](https://getcomposer.org/download/) -->

- [Download](https://getcomposer.org/download/) & install composer

```bash
composer install
```
- [Download](https://nodejs.org/en/download/) & install npm

- [Download](https://www.apachefriends.org/download.html) & install xampp
    - Start `Apache` & `MySQL` modules


## Installation

1. Copy the path from `path_to_xampp/php` folder and paste it to `Path` in system environment.

2. Clone the repo
   ```bash
   git clone https://gitlab.com/dnd-exim-group/dakyaa.git
   ```

3. Cd into project folder and initialize git
   ```bash
   git cd dnd-express
   git init
   ```

4. Create `.env` file & generate `APP_KEY`
   ```bash
   php artisan key:generate
   ```

5. Install NPM packages
   ```bash
   npm install
   ```
## Git Flow

**Note: Do Not merge any other branch to `master` branch other than `staging` branch**.

Follow below steps for pushing code to git:

1. When you create something you should create your `Feature Branch`

    ```bash
    git checkout -b feature/YourFeatureName
    ```
2. When you change or fix something you should create `Fix Branch`

    ```bash
    git checkout -b fix/YourFixtureName
    ```
3. Add files

    ```bash
    git add .
    ```
4. Commit your Changes

    ```bash
    git commit -m 'Add some AmazingFeature'
    ```
5. Push to the Branch

    ```bash
    git push origin feature/YourFeatureName
    ```
6. Create a merge request & **merge with `staging` branch only**
7. Test on [Staging Live Server](https://staging.dakyaa.com)
8. After successful testing merge `staging` branch to `master` branch

## Acknowledgements
* [Feather Icons](https://feathericons.com)
* [SimpleBar](https://grsmto.github.io/simplebar)


## License
[MIT](https://choosealicense.com/licenses/mit/)
