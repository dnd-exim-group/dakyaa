<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

use App\Imports\CountriesImport;

class ImportCountries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:ImportCountries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports countries in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->filePath = public_path('storage/Excel/Countries.xls');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Excel::import(new CountriesImport(), $this->filePath);
        return true;
    }
}
