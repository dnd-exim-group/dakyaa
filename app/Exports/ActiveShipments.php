<?php

namespace App\Exports;

use App\Models\Shipment;
use App\Models\Status;
use App\Models\Tracking;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithBorders;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ActiveShipments implements FromCollection, WithHeadings, WithStyles, WithColumnWidths
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // $shipments = Shipment::whereHas('tracking', function($query){
        //     $query->whereHas('statuses', function($query){
        //         $query->where('status_id', '!=', 4);
        //     });
        // })->get();

        // $tracking = Tracking::whereHas('status_tracking', function($query){
        //     $query->where('status_tracking.tracking_id', '=', 'tracking.id');
        // })->get();

        $statuses = Status::where('id', '!=', 4)->get();

        $shipments = [];
            $i=0;
        foreach($statuses as $status) {
            $trackings = $status->trackings;
            // dd($trackings);
            foreach($trackings as $tracking) {
                // dd($tracking->shipment);
                $shipments[$i++] = $tracking->shipment;
            }
        }

        $shipmentsArray = [];
        $i = 0;
        foreach($shipments as $shipment){
            $shipmentObject['srno'] = $i+1;
            $shipmentObject['date'] = $shipment->created_at;
            $shipmentObject['sender'] = $shipment->senderAddress->firstname . ' ' . $shipment->senderAddress->lastname;
            $shipmentObject['receiver'] = $shipment->receiverAddress->firstname . ' ' . $shipment->receiverAddress->lastname;
            $shipmentObject['origin'] = $shipment->senderAddress->country->name;
            $shipmentObject['destination'] = $shipment->receiverAddress->country->name;
            $shipmentObject['status'] = $shipment->tracking->lastStatus()->name;
            $shipmentsArray[$i] = $shipmentObject;
            $i++;
        }
        return collect($shipmentsArray);
    }

    public function headings(): array
    {
        return [
            'SR. NO.',
            'BOOKING DATE',
            'SENDER',
            'RECEIVER',
            'ORIGIN',
            'DESTINATION',
            'CURRENT STATUS',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => [
                'font' => ['bold' => true],
            ],
        ];

    }
    public function columnWidths(): array
    {
        return [
            'A' => 8,
            'B' => 20,
            'C' => 30,
            'D' => 30,
            'E' => 16,
            'F' => 25,
            'G' => 20,
        ];
    }
}
