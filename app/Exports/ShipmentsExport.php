<?php

namespace App\Exports;

use App\Models\Shipment;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ShipmentsExport implements FromCollection, WithHeadings, WithStyles, WithColumnWidths
{
    use Exportable;

    protected $from_date;
    protected $to_date;

    function __construct($from_date, $to_date)
    {
        $this->from_date = $from_date;
        $this->to_date = $to_date;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        $shipments = Shipment::whereBetween('created_at', [$this->from_date, $this->to_date])->get();

        $shipmentsArray = [];
        $i = 0;
        foreach($shipments as $shipment){
            // $shipmentObject['srno'] = $i+1;
            // $shipmentObject['date'] = $shipment->created_at;
            // $shipmentObject['destination'] = $shipment->receiverAddress->country->name;
            // if($shipment->tracking) {
            //     $shipmentObject['awb_no'] = $shipment->tracking->tracking_number;
            // } else {
            //     $shipmentObject['awb_no'] = "-";
            // }
            // $shipmentObject['name'] = $shipment->receiverAddress->firstname . ' ' . $shipment->receiverAddress->lastname;
            // if($shipment->tracking) {
            //     $shipmentObject['fwd_no'] = $shipment->tracking->awb_number;
            // } else {
            //     $shipmentObject['fwd_no'] = "-";
            // }
            // $shipmentObject['box'] = $shipment->boxes->count();
            // $shipmentObject['weight'] = "10 Kg";
            // $shipmentObject['contact'] = $shipment->receiverAddress->phone_no;
            // $shipmentsArray[$i] = $shipmentObject;
            // $i++;
            $shipmentObject['srno'] = $i+1;
            $shipmentObject['date'] = $shipment->created_at;
            $shipmentObject['sender'] = $shipment->senderAddress->firstname . ' ' . $shipment->senderAddress->lastname;
            $shipmentObject['receiver'] = $shipment->receiverAddress->firstname . ' ' . $shipment->receiverAddress->lastname;
            $shipmentObject['origin'] = $shipment->senderAddress->country->name;
            $shipmentObject['destination'] = $shipment->receiverAddress->country->name;
            if($shipment->tracking) {
                $shipmentObject['tracking_number'] = $shipment->tracking->tracking_number;
            } else {
                $shipmentObject['tracking_number'] = "-";
            }
            if($shipment->awb_no) {
                $shipmentObject['awb_no'] = $shipment->tracking->awb_no;
            } else {
                $shipmentObject['awb_no'] = "-";
            }
            $shipmentObject['status'] = $shipment->tracking->lastStatus()->name;
            $shipmentObject['box'] = $shipment->boxes->count();
            $boxes = $shipment->boxes;
            $approxWeight=0;
            $volumeWeight=0;
            foreach($boxes as $box) {
                $approxWeight = $box->Weight;
                $volumeWeight = ($box->length*$box->width*$box->height)/5000;
            }
            $weight = $approxWeight > $volumeWeight ? $approxWeight : $volumeWeight;
            $shipmentObject['weight'] = $weight;
            $shipmentsArray[$i] = $shipmentObject;
            $i++;
        }
        return collect($shipmentsArray);
    }

    public function headings(): array
    {
        return [
            // 'SR. NO.',
            // 'DATE',
            // 'DESTINATION',
            // 'AWB NO.',
            // 'NAME',
            // 'FWD NO.',
            // 'BOX',
            // 'WEIGHT',
            // 'CONTACT',
            'SR. NO.',
            'BOOKING DATE',
            'SENDER',
            'RECEIVER',
            'ORIGIN',
            'DESTINATION',
            'TRACKING No',
            'AWB NO',
            'CURRENT STATUS',
            'BOX',
            'WEIGHT',
        ];
    }
    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],
        ];

    }
    public function columnWidths(): array
    {
        return [
            'A' => 8,
            'B' => 20,
            'C' => 25,
            'D' => 30,
            'E' => 16,
            'F' => 30,
            'G' => 20,
            'H' => 20,
            'I' => 16,
            'J' => 8,
            'k' => 8,
        ];
    }
}
