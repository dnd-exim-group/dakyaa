<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\Address\CreateAddressRequest;
use App\Http\Requests\Customer\Address\UpdateAddressRequest;
use App\Models\Address;
use App\Models\Country;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AddressesController extends Controller
{
    public function index(){
        // $user = User::where("id", "=", Auth::user()->id)
        //                 ->where("role_id", "=", 3)->get()->first();
        // $countries = Country::all();
        // return view('customer.address.address',compact(['user', 'countries']));
    }

    public function create(){
        $countries = Country::all();
        return view('customer.address.create',compact(['countries']));
    }

    public function store(CreateAddressRequest $request){
        // dd($request);
        DB::transaction(function () use($request) {
            $is_billing_address = $request->billing_address==1 ? true : false;

            $address = Address::create([
                'user_id' => Auth::user()->id,
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'country_code' => $request->dial_code,
                'phone_no' => $request->phone_no,
                'address_line1' => $request->address_line1,
                'address_line2' => $request->address_line2,
                'city' => $request->city,
                'pincode' => $request->pincode,
                'state_country' => $request->state,
                'country_id' => $request->country,
                'address_type' => $request->address_type,
                'is_billing_address' => $is_billing_address,
                'is_saved' => 1,
            ]);

            if ($address) {
                session()->flash('success', 'Address added successfully');
            } else {
                session()->flash('error', 'Address could not be added!');
            }

        });
        return redirect()->route('customer.profile');
    }

    public function edit(Address $address){
        $countries = Country::all();
        return view('customer.address.edit',compact(['address', 'countries']));
    }

    public function Update(UpdateAddressRequest $request, Address $address){
        DB::transaction(function () use($request, $address) {
            $is_billing_address = $request->billing_address==1 ? true : false;
            $status = $address->update([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'country_code' => $request->dial_code,
                'phone_no' => $request->phone_no,
                'address_line1' => $request->address_line1,
                'address_line2' => $request->address_line2,
                'city' => $request->city,
                'pincode' => $request->pincode,
                'state_country' => $request->state,
                'country_id' => $request->country,
                'address_type' => $request->address_type,
                'is_billing_address' => $is_billing_address,
            ]);

            if ($status) {
                session()->flash('success', 'Address updated successfully');
            } else {
                session()->flash('error', 'Address updation failed');
            }
        });

        return redirect()->route('customer.profile');
    }

    public function delete(Address $address){
        // dd("hello");
        // dd($address);
        $status = $address->delete();
        if ($status) {
            session()->flash('success', 'Address deleted successfully');
        } else {
            session()->flash('error', 'Address could not be deleted');
        }
        return redirect()->route('customer.profile');
    }
}
