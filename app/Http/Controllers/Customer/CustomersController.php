<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\Customer\UpdateCustomerPasswordRequest;
use App\Http\Requests\Customer\Customer\UpdateCustomerRequest;
use App\Models\Address;
use App\Models\Country;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    public function index() {
        $user = User::where("id", "=", Auth::user()->id)
                        ->where("role_id", "=", 3)->get()->first();
        $countries = Country::all();

        $addresses = Address::where("user_id", "=", Auth::user()->id)->get();
        return view('customer.profile.index',compact(['user', 'countries', 'addresses']));
    }

    public function update(UpdateCustomerRequest $request, User $user){
        $status = $user->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone_no' => $request->phone_no,
            'country_id' => $request->dial_code,
        ]);
        if ($status) {
            session()->flash('success', 'Details updated successfully');
        } else {
            session()->flash('error', 'Details updation failed');
        }

        return redirect()->route('customer.profile');
    }

    public function updatePassword(UpdateCustomerPasswordRequest $request, User $user){
        dd('hello');
        $status = $user->update([
            'password' => Hash::make($request->password),
        ]);
        dd($status);
        if ($status) {
            session()->flash('success', 'Password updated successfully');
        } else {
            session()->flash('error', 'Password updation failed');
        }
        return redirect()->route('customer.profile');
    }

}
