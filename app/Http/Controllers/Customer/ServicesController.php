<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Quote;
use App\Models\Service;

class ServicesController extends Controller
{
    public function index(Quote $quote) {
        $services = Service::where('active', '=', '1')
                    ->whereHas('countries', function ($query) use($quote){
                        $query->where('country_service.country_id', '=', $quote->destination_country_id);
                    })->get();

        return view('customer.services.index', compact('services', 'quote'));
    }
}
