<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Country;
use App\Models\Quote;
use App\Models\Service;
use App\Models\Shipment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShipmentsController extends Controller
{
    public function index() {
        $shipments = auth()->user()->shipments;
        // dd($shipments);
        return view('customer.shipment.index', compact('shipments'));
    }
    public function create(Quote $quote, Service $service) {
        $addresses = auth()->user()->addresses;
        $countries = Country::all();
        return view('customer.shipment.create', compact(['countries', 'quote', 'service', 'addresses']));
    }

    public function store(Request $request, Quote $quote, Service $service) {
        DB::transaction(function () use($request, $quote, $service) {
            $senderAddress = null;
            $receiverAddress = null;

            if($request->senderAddress == "new") {
                $validated = $request->validate([
                    'senderFirstname' => 'required|min:3',
                    'senderLastname' => 'required|min:1',
                    'senderCountryCode' => 'required',
                    'senderPhone' => 'required',
                    'senderEmail' => 'email',
                    'senderAddress1' => 'required',
                    'senderCity' => 'required',
                    'senderPincode' => 'required',
                    'senderState' => 'required',
                    'senderCountry' => 'required',
                    'senderAddressType' => 'required',
                ]);

                $senderAddress = Address::create([
                    "user_id" => auth()->user()->id,
                    "firstname" => $request->senderFirstname,
                    "lastname" => $request->senderLastname,
                    "country_code" => $request->senderCountryCode,
                    "phone_no" => $request->senderPhone,
                    "email" => $request->senderEmail,
                    "address_line1" => $request->senderAddress1,
                    "address_line2" => $request->senderAddress2,
                    "city" => $request->senderCity,
                    "pincode" => $quote->pickup_pincode,
                    "state_country" => $request->senderState,
                    "country_id" => $quote->pickup_country_id,
                    "address_type" => $request->senderAddressType,
                    "is_saved" => 1,
                    "is_billing_address" => 0,
                ]);
            } else {
                $senderAddress = Address::find($request->senderStoredAddress);
            }

            if($request->receiverAddress == "new") {
                $validated = $request->validate([
                    'receiverFirstname' => 'required|min:3',
                    'receiverLastname' => 'required|min:1',
                    'receiverCountryCode' => 'required',
                    'receiverPhone' => 'required',
                    'receiverEmail' => 'email',
                    'receiverAddress1' => 'required',
                    'receiverCity' => 'required',
                    'receiverPincode' => 'required',
                    'receiverState' => 'required',
                    'receiverCountry' => 'required',
                    'receiverAddressType' => 'required',
                ]);

                $receiverAddress = Address::create([
                    "user_id" => auth()->user()->id,
                    "firstname" => $request->receiverFirstname,
                    "lastname" => $request->receiverLastname,
                    "country_code" => $request->receiverCountryCode,
                    "phone_no" => $request->receiverPhone,
                    "email" => $request->receiverEmail,
                    "address_line1" => $request->receiverAddress1,
                    "address_line2" => $request->receiverAddress2,
                    "city" => $request->receiverCity,
                    "pincode" => $quote->destination_pincode,
                    "state_country" => $request->receiverState,
                    "country_id" => $quote->destination_country_id,
                    "address_type" => $request->receiverAddressType,
                    "is_saved" => 1,
                    "is_billing_address" => 0,
                ]);
            } else {
                $receiverAddress = Address::find($request->receiverStoredAddress);
            }

            $shipment = Shipment::create([
                "user_id" => auth()->user()->id,
                "sender_address_id" => $senderAddress->id,
                "receiver_address_id" => $receiverAddress->id,
                "payment_status" => 0,
                "service_id" => $service->id,
                "status" => config('constants.SHIPMENT_PENDING'),
                "shipment_type" => $quote->shipment_type,
                "quote_id" => $quote->id
            ]);

            for($i = 1; $i <= count($request->length); $i++){
                $box = $shipment->boxes()->create([
                    "width" => $request->width[$i-1],
                    "height" => $request->height[$i-1],
                    "length" => $request->length[$i-1],
                    "weight" => $request->weight[$i-1],
                ]);

                $name = $request->input('itemName_'.$i);
                $quantity = $request->input('itemQuantity_'.$i);
                $value = $request->input('value_'.$i);

                for($j = 0; $j<count($name); $j++){
                    $items = $box->items()->create([
                        "name" => $name[$j],
                        "count" => $quantity[$j],
                        "value" => $value[$j]
                    ]);
                }
            }
            if ($shipment) {
                session()->flash('success', 'Shipment created successfully');
            } else {
                session()->flash('error', 'Shipment creation failed');
            }
        });

        return redirect()->route('customer');
    }

    public function track(Shipment $shipment)
    {
        return view('customer.shipment.track',compact(['shipment']));
    }

    public function show(Shipment $shipment) {
        $this->authorize('canViewShipment', $shipment);
        return view('customer.shipment.show', compact(['shipment']));
    }

    public function edit(Shipment $shipment) {
        $this->authorize('canViewShipment', $shipment);
        $addresses = auth()->user()->addresses;
        $countries = Country::all();
        return view('customer.shipment.edit', compact(['shipment', 'addresses', 'countries']));
    }

    public function update(Request $request, Shipment $shipment) {
        DB::transaction(function () use($request, $shipment) {
            $senderAddress = null;
            $receiverAddress = null;

            if($request->senderAddress == "new") {
                $validated = $request->validate([
                    'senderFirstname' => 'required|min:3',
                    'senderLastname' => 'required|min:1',
                    'senderCountryCode' => 'required',
                    'senderPhone' => 'required',
                    'senderEmail' => 'email',
                    'senderAddress1' => 'required',
                    'senderCity' => 'required',
                    'senderPincode' => 'required',
                    'senderState' => 'required',
                    'senderCountry' => 'required',
                    'senderAddressType' => 'required',
                ]);

                $senderAddress = $shipment->senderAddress->update([
                    "user_id" => auth()->user()->id,
                    "firstname" => $request->senderFirstname,
                    "lastname" => $request->senderLastname,
                    "country_code" => $request->senderCountryCode,
                    "phone_no" => $request->senderPhone,
                    "email" => $request->senderEmail,
                    "address_line1" => $request->senderAddress1,
                    "address_line2" => $request->senderAddress2,
                    "city" => $request->senderCity,
                    "pincode" => $shipment->quote->pickup_pincode,
                    "state_country" => $request->senderState,
                    "country_id" => $shipment->quote->pickup_country_id,
                    "address_type" => $request->senderAddressType,
                    "is_saved" => 1,
                    "is_billing_address" => 1,
                ]);
                $senderAddress = $shipment->senderAddress;
            } else {
                $senderAddress = Address::find($request->senderStoredAddress);
            }

            if($request->receiverAddress == "new") {
                $validated = $request->validate([
                    'receiverFirstname' => 'required|min:3',
                    'receiverLastname' => 'required|min:1',
                    'receiverCountryCode' => 'required',
                    'receiverPhone' => 'required',
                    'receiverEmail' => 'email',
                    'receiverAddress1' => 'required',
                    'receiverCity' => 'required',
                    'receiverPincode' => 'required',
                    'receiverState' => 'required',
                    'receiverCountry' => 'required',
                    'receiverAddressType' => 'required',
                ]);

                $receiverAddress = $shipment->receiverAddress->update([
                    "user_id" => auth()->user()->id,
                    "firstname" => $request->receiverFirstname,
                    "lastname" => $request->receiverLastname,
                    "country_code" => $request->receiverCountryCode,
                    "phone_no" => $request->receiverPhone,
                    "email" => $request->receiverEmail,
                    "address_line1" => $request->receiverAddress1,
                    "address_line2" => $request->receiverAddress2,
                    "city" => $request->receiverCity,
                    "pincode" => $shipment->quote->destination_pincode,
                    "state_country" => $request->receiverState,
                    "country_id" => $shipment->quote->destination_country_id,
                    "address_type" => $request->receiverAddressType,
                    "is_saved" => 1,
                    "is_billing_address" => 0,
                ]);

                $receiverAddress = $shipment->receiverAddress;
            } else {
                $receiverAddress = Address::find($request->receiverStoredAddress);
            }

            $updatedShipment = $shipment->update([
                "sender_address_id" => $senderAddress->id,
                "receiver_address_id" => $receiverAddress->id
            ]);

            $shipment->boxes()->delete();

            for($i = 1; $i <= count($request->length); $i++){
                $box = $shipment->boxes()->create([
                    "width" => $request->width[$i-1],
                    "height" => $request->height[$i-1],
                    "length" => $request->length[$i-1],
                    "weight" => $request->weight[$i-1],
                ]);

                $name = $request->input('itemName_'.$i);
                $quantity = $request->input('itemQuantity_'.$i);
                $value = $request->input('value_'.$i);

                for($j = 0; $j<count($name); $j++){
                    $items = $box->items()->create([
                        "name" => $name[$j],
                        "count" => $quantity[$j],
                        "value" => $value[$j]
                    ]);
                }
            }
            if ($updatedShipment) {
                session()->flash('success', 'Shipment updated successfully');
            } else {
                session()->flash('error', 'Shipment updation failed');
            }
        });
        return redirect()->route('quotes.index');
    }
}
