<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        return true;
    }

    public function greet()
    {
        return view('index', ['countries' => Country::all()]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $countries = Country::all();
        if(Auth::user()->isAdmin()) {
            return view('admin.index');
        } else if(Auth::user()->isCustomer()) {
            return view('customer.index');
        } else {
            return view('index', ['countries' => Country::all()]);
        }
    }
}
