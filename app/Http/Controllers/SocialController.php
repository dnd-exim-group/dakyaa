<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Laravel\Socialite\Facades\Socialite;


class SocialController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();
        $users = User::where(['email' => $userSocial->getEmail()])->first();

        // $name = $userSocial->getName();
        $splitName = explode(" ", $userSocial->getName());
        // print_r($users);
        if($users) {
            FacadesAuth::login($users);
            return redirect('home');
        } else {
            $user = User::create([
                'firstname'     => $splitName[0],
                'lastname'      => $splitName[1],
                'email'         => $userSocial->getEmail(),
                'provider_id'   => $userSocial->getId(),
                'provider'      => $provider,
                'role_id'       => 3,
                'admin_created' => 0,
            ]);
            return redirect()->route('home');
        }
    }

    // public function Callback($provider)
    // {
    //     var_dump("hello");
    //     die();
    //     // if('facebook' == $provider)
    //     //     $userSocial = Socialite::driver('facebook')->fields(['name', 'first_name', 'last_name', 'email'])->user();
    //     // else
    //         $userSocial = Socialite::driver($provider)->stateless()->user();
    //     $users = User::where(['email' => $userSocial->getEmail()])->first();
    //     // print_r($users);
    //     if($users){
    //         Auth::login($users);
    //         return redirect('home');
    //     } else{
    //         // print_r($userSocial);
    //         $user = User::create([
    //             'name'          => $userSocial->getName(), // $user->user['first_name']
    //             'email'         => $userSocial->getEmail(),
    //             'image'         => $userSocial->getAvatar(),
    //             'provider_id'   => $userSocial->getId(),
    //             'provider'      => $provider,
    //         ]);
    //         return redirect()->route('home');
    //     }
    // }
}
