<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Customer\CreateCustomerRequest;
use App\Http\Requests\Admin\Customer\UpdateCustomerRequest;
use App\Models\User;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    public function index() {
        $customers = User::where("active", "=", 1)
                        ->where("role_id", "=", 3)->get();
        return view('admin.customer.index',compact(['customers']));
    }

    public function create(){
        $countries = Country::all();
        return view('admin.customer.create',compact(['countries']));
    }

    public function store(CreateCustomerRequest $request) {
        DB::transaction(function () use($request) {
            $user = User::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
                'phone_no' => $request->phone,
                'provider' => null,
                'provider_id' => null,
                'password' => Hash::make($request->password),
                'admin_created' => 1,
                'role_id' => 3,
                'active' => 1,
                'country_id' => $request->dial_code,
            ]);

            if ($user) {
                session()->flash('success', 'Customer created successfully');
            } else {
                session()->flash('error', 'Customer creation failed');
            }

        });
        return redirect()->route('admin.customers');
    }

    public function edit(User $user) {
        $countries = Country::all();
        return view('admin.customer.edit', compact(['user', 'countries']));
    }

    public function update(UpdateCustomerRequest $request, User $user){
        $status = $user->update([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'phone_no' => $request->phone,
            'provider' => null,
            'provider_id' => null,
            'password' => Hash::make($request->password),
            'admin_created' => 1,
            'role_id' => 3,
            'active' => 1,
            'country_id' => $request->dial_code,
        ]);

        if ($status) {
            session()->flash('success', 'Customer updated successfully');
        } else {
            session()->flash('error', 'Customer updation failed');
        }

        return redirect()->route('admin.customers');
    }

    public function show(User $user) {
        return view('admin.customer.show', compact(['user']));
    }

    public function customerTrash() {
        $users = User::where('active', '=', '0')->get();
        return view('admin.customer.restore', compact(['users']));
    }

    public function inactive(Request $request, User $user) {
        $user->update([
            'active' => 0,
        ]);
        return redirect()->route('admin.customers');
    }

    public function restore(Request $request,User $user) {
        $user->update([
            'active' => 1,
        ]);
        return redirect()->route('admin.customer.trash');
    }
}
