<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Quote;
use Illuminate\Http\Request;

class QuotesController extends Controller
{
    public function index(){
        $quotes = Quote::all();
        return view('admin.quote.index',compact(['quotes']));
    }
}
