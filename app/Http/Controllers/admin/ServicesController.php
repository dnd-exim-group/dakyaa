<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Service\CreateServiceRequest;
use App\Http\Requests\Admin\Service\UpdateServiceRequest;
use App\Models\Country;
use App\Models\Service;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicesController extends Controller

{
    public function index() {
        // dd("aya");
        $services = Service::where("active","=","1")
                    ->whereHas('vendor', function ($query) {
                        $query->where('active', '=', 1);
                    })->get();
        return view('admin.service.index',compact('services'));
    }

    public function create() {
        $vendors = Vendor::all();
        $countries = Country::all();
        return view('admin.service.create',compact(['vendors','countries']));
    }

    public function store(CreateServiceRequest $request) {
        $status = false;

        DB::transaction(function () use ($request, &$status) {
            $sameRate = $request->further_rate_same_as_late_rate=="on" ? true : false;
            $vendor = Vendor::find($request->vendor);
            $duty_paid = !$request->duty_paid ? 0:1;
            $service = $vendor->services()->create([
                'name' => $request->name,
                'duty_paid' => $duty_paid,
                // 'base_kg' => $request->base_kg,
                // 'price' => $request->base_price,
                // 'additional_kg' => $request->additional_kg,
                // 'additional_kg_price' => $request->additional_kg_price,
                'active' => 1,
                'further_rate_same_as_late_rate' => $sameRate,
            ]);
            for($i = 0; $i < count($request->price) ; $i++){
                $service->pricings()->create([
                    "end_kg" => $request->endKg[$i],
                    "per_kg" => 1,
                    "price" => $request->price[$i],
                ]);
            }

            for($i = 0; $i < count($request->countries) ; $i++){
                $service->countries()->attach("country_id",[
                    "country_id" => $request->countries[$i],
                    "duration" => $request->duration
                ]);
            }

            $serviceLog = $service->serviceLog()->create([
                'duty_paid' => $service->duty_paid,
                // 'base_kg' => $service->base_kg,
                // 'price' => $service->price,
                // 'additional_kg' => $request->additional_kg,
                // 'additional_kg_price' => $request->additional_kg_price,
                'further_rate_same_as_late_rate' => $service->further_rate_same_as_late_rate,
            ]);

            for($i = 0; $i < count($request->price) ; $i++){
                $serviceLog->pricingLog()->create([
                    "end_kg" => $request->endKg[$i],
                    "per_kg" => 1,
                    "price" => $request->price[$i],
                ]);
            }

            $status = true;
        });
        if ($status) {
            session()->flash('success', 'Service created successfully');
        } else {
            session()->flash('error', 'Service creation failed');
        }

        return redirect()->route('admin.services');
    }

    public function show(Service $service) {
        $vendors = Vendor::all();
        $countries = Country::all();
        return view('admin.service.show',compact(['service','vendors','countries']));
    }

    public function edit(Service $service) {
        $vendors = Vendor::all();
        $countries = Country::all();
        return view('admin.service.edit',compact(['service','vendors','countries']));
    }

    public function update(UpdateServiceRequest $request, Service $service) {
        // dd($request);
        $status = false;
        DB::transaction(function () use ($request, $service, &$status) {
            $sameRate = $request->further_rate_same_as_late_rate=="on" ? true : false;
            $duty_paid = !$request->duty_paid ? 0:1;
            $service->update([
                "vendor_id" => $request->vendor,
                'name' => $request->name,
                'duty_paid' => $duty_paid,
                // 'base_kg' => $request->base_kg,
                // 'price' => $request->base_price,
                // 'additional_kg' => $request->additional_kg,
                // 'additional_kg_price' => $request->additional_kg_price,
                'active' => 1,
                'further_rate_same_as_late_rate' => $sameRate,
            ]);

            $service->pricings()->delete();
            for($i = 0; $i < count($request->price) ; $i++) {
                $service->pricings()->create([
                    "end_kg" => $request->endKg[$i],
                    "per_kg" => 1,
                    "price" => $request->price[$i],
                ]);
            }

            $service->countries()->detach();
            for($i = 0; $i < count($request->countries) ; $i++) {
                $service->countries()->attach("country_id",[
                    "country_id" => $request->countries[$i],
                    "duration" => $request->duration
                ]);
            }

            $serviceLog = $service->serviceLog()->create([
                'duty_paid' => $service->duty_paid,
                // 'base_kg' => $service->base_kg,
                // 'price' => $service->price,
                // 'additional_kg' => $request->additional_kg,
                // 'additional_kg_price' => $request->additional_kg_price,
                'further_rate_same_as_late_rate' => $service->further_rate_same_as_late_rate,
            ]);

            for($i = 0; $i < count($request->price) ; $i++){
                $serviceLog->pricingLog()->create([
                    "end_kg" => $request->endKg[$i],
                    "per_kg" => 1,
                    "price" => $request->price[$i],
                ]);
            }

            $status = true;
        });

        if ($status) {
            session()->flash('success', 'Service updated successfully');
        } else {
            session()->flash('error', 'Service updation failed');
        }

        return redirect()->route('admin.services');
    }

    public function serviceByCountryId(Request $request) {
        $country = Country::find($request->country);
        // dd($country->services()->where('active', '1')->get());
        return json_encode($country->services()->where('active', '1')->get());
    }

    public function serviceTrash() {
        $services = Service::where('active', '=', '0')
                    ->whereHas('vendor', function ($query) {
                        $query->where('active', '=', 1);
                    })->get();
        return view('admin.service.restore', compact(['services']));
    }

    public function inactive(Request $request, Service $service) {
        $service->update([
            'active' => 0,
        ]);
        return redirect()->route('admin.services');
    }

    public function restore(Request $request, Service $service) {
        $service->update([
            'active' => 1,
        ]);
        return redirect()->route('admin.service.trash');
    }
}
