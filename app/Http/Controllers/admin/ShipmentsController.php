<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ActiveShipments;
use App\Exports\ShipmentsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Shipment\CreateShipmentRequest;
use App\Http\Requests\Admin\Shipment\UpdateShipmentRequest;
use App\Models\Address;
use App\Models\Country;
use App\Models\Shipment;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ShipmentsController extends Controller
{
    public function index(){
        $shipments = Shipment::where('active', '=', '1')->get();
        return view('admin.shipment.index',compact(['shipments']));
    }

    public function create(){
        $trackingStatuses = Status::all();
        $countries = Country::all();
        $users = User::all();
        return view('admin.shipment.create',compact(['trackingStatuses','countries','users']));
    }


    public function store(CreateShipmentRequest $request){
        $shipment = null;
        DB::transaction(function () use ($request, &$shipment) {
            $paymentStatus = $request->paymentStatus=="on" ? true : false;
            $senderAddress = Address::create([
                "user_id" => $request->customer,
                "firstname" => $request->senderFirstname,
                "lastname" => $request->senderLastname,
                "country_code" => $request->senderCountryCode,
                "phone_no" => $request->senderPhone,
                "email" => $request->senderEmail,
                "address_line1" => $request->senderAddress1,
                "address_line2" => $request->senderAddress2,
                "city" => $request->senderCity,
                "pincode" => $request->originPincode,
                "state_country" => $request->senderState,
                "country_id" => $request->origin,
                "address_type" => $request->senderAddressType,
                "is_saved" => 1,
                "is_billing_address" => 1,
            ]);

            $receiverAddress = Address::create([
                "user_id" => $request->customer,
                "firstname" => $request->receiverFirstname,
                "lastname" => $request->receiverLastname,
                "country_code" => $request->receiverCountryCode,
                "phone_no" => $request->receiverPhone,
                "email" => $request->receiverEmail,
                "address_line1" => $request->receiverAddress1,
                "address_line2" => $request->receiverAddress2,
                "city" => $request->receiverCity,
                "pincode" => $request->destinationPincode,
                "state_country" => $request->receiverState,
                "country_id" => $request->destination,
                "address_type" => $request->receiverAddressType,
                "is_saved" => 1,
                "is_billing_address" => 1,
            ]);

            $shipment = Shipment::create([
                "user_id" => $request->customer,
                "sender_address_id" => $senderAddress->id,
                "receiver_address_id" => $receiverAddress->id,
                "payment_status" => $paymentStatus,
                "service_id" => $request->service,
                "status" => config('constants.SHIPMENT_PENDING'),
                "shipment_type" => $request->shipmentType
            ]);

            for($i = 1; $i <= count($request->length); $i++){
                $box = $shipment->boxes()->create([
                    "width" => $request->width[$i-1],
                    "height" => $request->height[$i-1],
                    "length" => $request->length[$i-1],
                    "weight" => $request->weight[$i-1],
                ]);

                $name = $request->input('itemName_'.$i);
                $quantity = $request->input('itemQuantity_'.$i);
                $value = $request->input('value_'.$i);

                for($j = 0; $j<count($name); $j++){
                    $items = $box->items()->create([
                        "name" => $name[$j],
                        "count" => $quantity[$j],
                        "value" => $value[$j]
                    ]);
                }
            }

        });
        if ($shipment) {
            session()->flash('success', 'Shipment created successfully');
        } else {
            session()->flash('error', 'Shipment creation failed');
        }

        return redirect(route('admin.shipments'));
    }

    public function edit(Shipment $shipment) {
        $trackingStatuses = Status::all();
        $countries = Country::all();
        $users = User::all();
        return view('admin.shipment.edit',compact(['shipment', 'trackingStatuses', 'countries', 'users']));
    }

    public function update(UpdateShipmentRequest $request, Shipment $shipment) {
        $status = false;
        DB::transaction(function () use ($request, $shipment, &$status) {
            $paymentStatus = $request->paymentStatus=="on" ? true : false;
            $shipment->senderAddress->update([
                "user_id" => $request->customer,
                "firstname" => $request->senderFirstname,
                "lastname" => $request->senderLastname,
                "phone_no" => $request->senderPhone,
                "email" => $request->senderEmail,
                "country_code" => $request->senderCountryCode,
                "address_line1" => $request->senderAddress1,
                "address_line2" => $request->senderAddress2,
                "city" => $request->senderCity,
                "pincode" => $request->originPincode,
                "state_country" => $request->senderState,
                "country_id" => $request->origin,
                "address_type" => $request->senderAddressType,
                "is_saved" => 1,
                "is_billing_address" => 1,
            ]);

            $shipment->receiverAddress->update([
                "user_id" => $request->customer,
                "firstname" => $request->receiverFirstname,
                "lastname" => $request->receiverLastname,
                "phone_no" => $request->receiverPhone,
                "email" => $request->receiverEmail,
                "country_code" => $request->receiverCountryCode,
                "address_line1" => $request->receiverAddress1,
                "address_line2" => $request->receiverAddress2,
                "city" => $request->receiverCity,
                "pincode" => $request->destinationPincode,
                "state_country" => $request->receiverState,
                "country_id" => $request->destination,
                "address_type" => $request->receiverAddressType,
                "is_saved" => 1,
                "is_billing_address" => 1,
            ]);

            $shipment->update([
                "user_id" => $request->customer,
                "sender_address_id" => $shipment->senderAddress->id,
                "receiver_address_id" => $shipment->receiverAddress->id,
                "payment_status" => $paymentStatus,
                "service_id" => $request->service,
                "shipment_type" => $request->shipmentType
            ]);

            $shipment->boxes()->delete();

            $i = 1;
            foreach($request->box_no as $j){
                $box = $shipment->boxes()->create([
                    "width" => $request->width[$i-1],
                    "height" => $request->height[$i-1],
                    "length" => $request->length[$i-1],
                    "weight" => $request->weight[$i-1],
                ]);

                $name = $request->input('itemName_'.$j);
                $quantity = $request->input('itemQuantity_'.$j);
                $value = $request->input('value_'.$j);
                // dd(count($name));
                for($j = 0; $j<count($name); $j++){
                    $items = $box->items()->create([
                        "name" => $name[$j],
                        "count" => $quantity[$j],
                        "value" => $value[$j]
                    ]);
                }
                $i++;
            }

            $status = true;
        });

        if ($status) {
            session()->flash('success', 'Shipment updated successfully');
        } else {
            session()->flash('error', 'Shipment updation failed');
        }

        return redirect(route('admin.shipments'));
    }

    public function show(Shipment $shipment){
        $trackingStatuses = Status::all();
        $countries = Country::all();
        $users = User::all();
        return view('admin.shipment.show',compact(['shipment','trackingStatuses','countries','users']));
    }

    public function shipmentPending(Request $request,Shipment $shipment){
        $shipment->update([
            "status" => config('constants.SHIPMENT_PENDING')
        ]);
        return redirect(route('admin.shipments'));
    }

    public function shipmentApproved(Request $request,Shipment $shipment){
        $shipment->update([
            "status" => config('constants.SHIPMENT_APPROVED')
        ]);
        return redirect(route('admin.shipments'));
    }

    public function shipmentReject(Request $request,Shipment $shipment){
        $shipment->update([
            "status" => config('constants.SHIPMENT_REJECTED')
        ]);
        return redirect(route('admin.shipments'));
    }

    public function shipmentTrash() {
        $shipments = Shipment::where('active', '=', '0')->get();
        return view('admin.shipment.restore', compact(['shipments']));
    }

    public function inactive(Request $request, Shipment $shipment) {
        $shipment->update([
            'active' => 0,
        ]);
        return redirect()->route('admin.shipments');
    }

    public function restore(Request $request,Shipment $shipment) {
        $shipment->update([
            'active' => 1,
        ]);
        return redirect()->route('admin.shipment.trash');
    }

    public function delete(Request $request, Shipment $shipment) {
        Shipment::destroy($shipment->id);
        return redirect()->route('admin.shipment.trash');
    }

    public function export(Request $request)
    {
        $from_date = $request->from_date;
        $to_date = $request->to_date;
        return Excel::download(new ShipmentsExport($from_date, $to_date), 'shipments.xlsx');
    }

    public function masterReportExport(Request $request)
    {
        return Excel::download(new ActiveShipments(), 'master-report.xlsx');
    }
}
