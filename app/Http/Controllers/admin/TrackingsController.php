<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Shipment;
use App\Models\Status;
use App\Models\Tracking;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrackingsController extends Controller
{
    public function index(){
        $trackings = Tracking::whereHas('shipment', function ($query) {
                        $query->where('active', '=', 1);
                    })->get();
        return view('admin.tracking.index',compact(['trackings']));
    }
    public function create(Shipment $shipment){
        $statuses = Status::all();
        $trackingNumbers = Tracking::pluck('tracking_number')->toArray();
        return view('admin.tracking.create',compact(['shipment','statuses','trackingNumbers']));
    }

    public function show(Tracking $tracking){
        return view('admin.tracking.show',compact(['tracking']));
    }

    public function edit(Tracking $tracking){
        $statuses = Status::all();
        return view('admin.tracking.edit',compact(['tracking','statuses']));
    }
    public function store(Request $request,Shipment $shipment){
        $tracking = $shipment->tracking()->create([
            "tracking_number" => $request->tracking_number,
            "awb_number" => $request->awb_number,
            "live_tracking_url" => $request->live_tracking_url,
        ]);
        $tracking->statuses()->attach("status_id",[
            "status_id" => $request->trackingStatus,
            "note" => $request->note,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        if ($tracking) {
            session()->flash('success', 'Tracking created successfully');
        } else {
            session()->flash('error', 'Tracking creation failed');
        }

        return redirect(route('admin.trackings'));
    }
    public function update(Request $request,Tracking $tracking){
        $status = $tracking->update([
            "tracking_number" => $request->tracking_number,
            "awb_number" => $request->awb_number,
            "live_tracking_url" => $request->live_tracking_url,
        ]);

        if ($status) {
            session()->flash('success', 'Tracking updated successfully');
        } else {
            session()->flash('error', 'Tracking updation failed');
        }

        return redirect(route('admin.trackings'));
    }
    public function editStatus(Tracking $tracking){
        $statuses = Status::all();
        return view('admin.tracking.update-status',compact(['tracking','statuses']));
    }
    public function updateStatus(Request $request,Tracking $tracking){
        $status = $tracking->statuses()->attach("status_id",[
            "status_id" => $request->trackingStatus,
            "note" => $request->note,
            "created_at" => Carbon::now(),
            "updated_at" => Carbon::now()
        ]);

        return redirect(route('admin.trackings'));
    }

    public function trackShipmentByTrackingNumber($trackingNumber)
    {
        $tracking = Tracking::where("tracking_number", $trackingNumber)->first();
        $data = null;
        if(!$tracking) {
            return response()->json($data, 404)
                        ->header('Access-Control-Allow-Origin', '*');
        }

        foreach($tracking->statuses as $status){
            $data['tracking'][] = [
                "tracking_status" => $status->name,
                "tracking_note" => $status->pivot->note,
                "tracking_date" => $status->pivot->created_at->format('d-m-Y'),
            ];
        }

        $data['name'] = $tracking->shipment->user->firstname . " " . $tracking->shipment->user->lastname;
        $data['service'] = $tracking->shipment->service->name;
        // $data['awb'] = $tracking->shipment->tracking->awb_number;
        $data['awb'] = $trackingNumber;
        $data['booking_date'] = $tracking->shipment->created_at->format('d-m-Y');
        $data['origin'] = $tracking->shipment->senderAddress->country->name;
        $data['destination'] = $tracking->shipment->receiverAddress->country->name;
        $data['last_status'] = $tracking->lastStatus()->name;

        return response()->json($data, 200)
                        ->header('Access-Control-Allow-Origin', '*');
    }

    public function trackShipment(Request $request)
    {
        $trackingNumber = $request->tracking_number;
        // return $this->trackShipmentByTrackingNumber($trackingNumber);
        $tracking = Tracking::where("tracking_number", $trackingNumber)->first();
        return view('track', compact(['tracking']));
    }
}
