<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Vendor\CreateVendorRequest;
use App\Http\Requests\Admin\Vendor\UpdateVendorRequest;
use App\Models\Vendor;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendorsController extends Controller
{
    public function index(){
        $vendors = Vendor::where("active","=","1")->get();
        return view('admin.vendor.index',compact(['vendors']));
    }

    public function create(){
        $countries = Country::all();
        return view('admin.vendor.create', compact(['countries']));
    }

    public function store(CreateVendorRequest $request){

        $imageAbsoluteUrl = null;

        if($request->file('logoUpload')){
            $file = $request->file('logoUpload');
            $filename = date('YmdHi').$file->getClientOriginalName();
            $uploaded = $file->move(public_path('assets/image/vendors'), $filename);
            $imageAbsoluteUrl = '/assets/image/vendors/' . $filename;
            // $data['logoUpload']= $filename;
        }

        // if(isset($request->logoUpload)) {
        //     $imageAbsoluteUrl = $this->storeImage($request->logoUpload);
        // }

        $vendor = Vendor::create([
            'name' => $request->name,
            'address' => $request->address,
            'country_id' => $request->country_code,
            'phone_no' => $request->phone,
            'email' => $request->email,
            'logo' => $imageAbsoluteUrl,
            'active' => 1,
        ]);

        if ($vendor) {
            session()->flash('success', 'Vendor created successfully');
        } else {
            session()->flash('error', 'Vendor created failed');
        }

        return redirect(route('admin.vendors'));
    }

    public function edit(Vendor $vendor){
        $countries = Country::all();
        return view('admin.vendor.edit',compact(['vendor', 'countries']));
    }

    public function update(UpdateVendorRequest $request, Vendor $vendor){

        $imageAbsoluteUrl = null;

        if (isset($request->logoUpload)) {
            $vendor->deleteImage();
            $file = $request->file('logoUpload');
            $filename = date('YmdHi').$file->getClientOriginalName();
            $uploaded = $file->move(public_path('assets/image/vendors'), $filename);
            $imageAbsoluteUrl = '/assets/image/vendors/' . $filename;
        } else {
            if(isset($vendor->logo)) {
                $imageAbsoluteUrl = $vendor->logo;
            }
        }

        $status = $vendor->update([
            'name' => $request->name,
            'address' => $request->address,
            'country_id' => $request->country_code,
            'phone_no' => $request->phone,
            'email' => $request->email,
            'logo' => $imageAbsoluteUrl,
            'active' => 1,
        ]);

        if ($status) {
            session()->flash('success', 'Vendor updated successfully');
        } else {
            session()->flash('error', 'e updation failed');
        }

        return redirect(route('admin.vendors'));
    }
    public function show(Vendor $vendor){
        return view('admin.vendor.show',compact(['vendor']));
    }

    public function vendorTrash() {
        $vendors = Vendor::where('active', '=', '0')->get();
        return view('admin.vendor.restore', compact(['vendors']));
    }

    public function inactive(Request $request, Vendor $vendor) {

        DB::transaction(function () use ($vendor) {
            $vendor->update([
                'active' => 0,
            ]);
        });
        return redirect()->route('admin.vendors');
    }

    public function restore(Request $request,Vendor $vendor) {

        DB::transaction(function () use ($vendor) {
            $vendor->update([
                'active' => 1,
            ]);
        });
        return redirect()->route('admin.vendor.trash');
    }

    public function storeImage($logo) {
        $imageAbsoluteUrl = '';
        if ($logo) {
            $image = $logo->store('vendorLogo');
            $imageAbsoluteUrl = asset('storage/' . $image);
        }
        return $imageAbsoluteUrl;
    }
}
