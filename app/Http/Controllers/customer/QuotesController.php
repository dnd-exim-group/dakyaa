<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Quote;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Twilio\Rest\Client;

class QuotesController extends Controller
{

    public function index()
    {
        $quotes = Quote::where('user_id', '=', Auth::user()->id)->get();
        $countries = Country::all();
        return view('customer.quote.index',compact(['quotes','countries']));
    }

    public function store(Request $request)
    {
        // dd($request);
        $quote = null;
        $phone_no = '';
        DB::transaction(function () use($request, &$quote, &$phone_no){
            if(Auth::user()) {
                $validated = $request->validate([
                    'senderCountry' => 'required',
                    'senderPin' => 'required',
                    'receiverCountry' => 'required',
                    'receiverPin' => 'required',
                    'weight' => 'required',
                    'shipmentType' => 'required',
                ]);
            } else {
                $validated = $request->validate([
                    'firstname' => 'required|min:3',
                    'lastname' => 'required|min:1',
                    'email' => 'email',
                    'phone' => 'required',
                    'senderCountry' => 'required',
                    'senderPin' => 'required',
                    'receiverCountry' => 'required',
                    'receiverPin' => 'required',
                    'weight' => 'required',
                    'shipmentType' => 'required',
                ]);
            }

            // if(Auth::user()->is_phone_verified) {
            //     // dd(Auth::user())
            //     // $phone_no =
            //     /* Get credentials from .env */
            //     $token = getenv("TWILIO_AUTH_TOKEN");
            //     $twilio_sid = getenv("TWILIO_SID");
            //     $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
            //     $twilio = new Client($twilio_sid, $token);
            //     $twilio->verify->v2->services($twilio_verify_sid)
            //         ->verifications
            //         ->create($data['phone_number'], "sms");
            // } else {

            // }

            // dd($request->exists('country_id') ? $request->country_id : auth()->user()->country_id);
            // dd(($request->exists('country_id') ? $request->country_id : auth()->user()->country_id).($request->exists('phone') ? $request->phone : auth()->user()->phone_no));
            // dd($request->exists('country_id') ? $request->country_id->dial_code : auth()->user()->country->dial_code);
            // dd(!Auth::user());

            if(!Auth::user()) {
                // dd($request->exists('country_id'));
                $country_id = $request->exists('country_id') ? $request->country_id : auth()->user()->country_id;
                $dial_code = Country::where('id','=',$country_id)->first()->dial_code;
                // dd($dial_code);
                $phone_no = ($dial_code).($request->exists('phone') ? $request->phone : auth()->user()->phone_no);
                // dd($phone_no);

                $token = getenv("TWILIO_AUTH_TOKEN");
                $twilio_sid = getenv("TWILIO_SID");
                $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
                $twilio = new Client($twilio_sid, $token);
                $twilio->verify->v2->services($twilio_verify_sid)
                    ->verifications
                    ->create($phone_no, "sms");
                // dd($phone_no);
                // session()->flash('phone_number', $phone_no);
                // setcookie('phone_number', $phone_no);
                $_COOKIE['phone_number'] = $phone_no;
                // dd($_COOKIE['phone_number']);
            }
            // dd($phone_no);

            $phone = $request->exists('phone') ? $request->phone : auth()->user()->phone_no;
            $email = $request->exists('email') ? $request->email : auth()->user()->email;
            $firstname = $request->exists('firstname') ? $request->firstname : auth()->user()->firstname;
            $lastname = $request->exists('lastname') ? $request->lastname : auth()->user()->lastname;
            $country_id = $request->exists('country_id') ? $request->country_id : auth()->user()->country_id;

            $quote = Quote::create([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'phone_no' => $phone,
                'country_code' => $country_id,
                'pickup_country_id' => $request->senderCountry,
                'pickup_pincode' => $request->senderPin,
                'shipment_type' => $request->shipmentType,
                'destination_country_id' => $request->receiverCountry,
                'destination_pincode' => $request->receiverPin,
                'approx_weight' => $request->weight,
                'user_id' => Auth::user() ? Auth::user()->id : null,
            ]);

            if ($quote) {
                session()->flash('success', 'Quote created successfully');
            } else {
                session()->flash('error', 'Quote creation failed');
            }
            // dd($quote);
        });
        // dd($quote);
        // Session::put('phone_number', $phone_no);
        // dd(session()->get('phone_number'));
        if(Auth::user()) {
            return redirect()->route('quotes.index');
        }
        else {
            // dd($quote);
            // return redirect()->route('verifyPhone')->with(['phone_number' => $phone_no]);
            return view('auth.verifyPhone', compact(['phone_no', 'quote']));
            // return redirect()->route('customer.services.index', $quote);
        }
    }

    protected function verify(Request $request, Quote $quote)
    {
        // dd($request);
        // dd($request['verification_code']);
        $data = $request->validate([
            'verification_code' => ['required', 'numeric'],
            'phone_number' => ['required', 'string'],
        ]);
        // dd("hello");
        /* Get credentials from .env */
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($data['verification_code'], array('to' => $data['phone_number']));

        if ($verification->valid) {
            // dd('Valid OTP');
            // $user = tap(User::where('id', '=', $quote->id))->update(['isVerified' => true]);
            /* Authenticate user */
            // Auth::login($user->first());
            // return redirect()->route('home');
            return redirect()->route('customer.services.index', $quote);
        }
        // dd($request['verification_code']);
        dd('Invalid verification code entered!');
        return back()->with(['phone_number' => $data['phone_number'], 'error' => 'Invalid verification code entered!']);
    }

    public function repeatQuote (Quote $quote) {
        $newQuote = $quote->replicate();
        $newQuote->created_at = Carbon::now();
        $newQuote->save();

        return redirect()->back();
    }

    public function update(Request $request, Quote $quote) {
        $this->authorize('isOwener', $quote);

        DB::transaction(function () use($request, $quote){
            $phone = $request->exists('phone') ? $request->phone : $quote->phone_no;
            $email = $request->exists('email') ? $request->email : $quote->email;
            $firstname = $request->exists('firstname') ? $request->phone : $quote->firstname;
            $lastname = $request->exists('lastname') ? $request->phone : $quote->lastname;
            $country_id = $request->exists('country_id') ? $request->country_id : $quote->country_code;

            $status = $quote->update([
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'phone_no' => $phone,
                'country_code' => $country_id,
                'pickup_country_id' => $request->senderCountry,
                'pickup_pincode' => $request->senderPin,
                'shipment_type' => $request->edit_shipmentType,
                'destination_country_id' => $request->receiverCountry,
                'destination_pincode' => $request->receiverPin,
                'approx_weight' => $request->weight
            ]);

            if ($status) {
                session()->flash('success', 'Quote updated successfully');
            } else {
                session()->flash('error', 'Quote updation failed');
            }
        });
        return redirect()->route('quotes.index');
    }

    public function destroy(Quote $quote)
    {
        $quote->delete();
        return redirect()->back();
    }

    public function getQuoteById(Request $request) {
        $data = Quote::find($request->id);
        return $data;
    }
}
