<?php

namespace App\Http\Requests\Admin\Customer;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'min:3',
            'lastname' => 'min:1',
            'email' => 'unique:users,email,'.$this->user->id,
            'phone' => 'required|unique:users,phone_no,'.$this->user->id,
            'password' => 'required',
        ];
    }
}
