<?php

namespace App\Http\Requests\Admin\Service;

use Illuminate\Foundation\Http\FormRequest;

class UpdateServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'vendor' => 'required',
            'countries' => 'required|array|min:1',
            'duration' => 'required',
            // 'base_kg' => 'required',
            // 'base_price' => 'required',
            // 'additional_kg' => 'required',
            // 'additional_kg_price' => 'required',
        ];
    }
}
