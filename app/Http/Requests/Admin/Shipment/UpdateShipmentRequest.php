<?php

namespace App\Http\Requests\Admin\Shipment;

use Illuminate\Foundation\Http\FormRequest;

class UpdateShipmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer' => 'required|not_in:0',
            'origin' => 'required|not_in:0',
            'destination' => 'required|not_in:0',
            'originPincode' => 'required',
            'destination' => 'required',
            'destinationPincode' => 'required',
            'service' => 'required',
            'shipmentType' => 'required',
            'senderFirstname' => 'required',
            'senderLastname' => 'required',
            'senderEmail' => 'required',
            'senderPhone' => 'required',
            'senderAddress1' => 'required',
            'senderCity' => 'required',
            'senderState' => 'required',
            'senderAddressType' => 'required',
            'receiverFirstname' => 'required',
            'receiverLastname' => 'required',
            'receiverEmail' => 'required',
            'receiverPhone' => 'required',
            'receiverAddress1' => 'required',
            'receiverCity' => 'required',
            'receiverPincode' => 'required',
            'receiverState' => 'required',
            'length' => 'required',
            'height' => 'required',
            'width' => 'required',
            'weight' => 'required',
        ];
    }
}
