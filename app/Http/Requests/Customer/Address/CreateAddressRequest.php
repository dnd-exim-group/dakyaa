<?php

namespace App\Http\Requests\Customer\Address;

use Illuminate\Foundation\Http\FormRequest;

class CreateAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:1',
            'email' => 'email',
            'dial_code' => 'required',
            'phone_no' => 'required',
            'address_line1' => 'required|max:255',
            'city' => 'required',
            'pincode' => 'required',
            'state' => 'required',
            'country' => 'required',
            'address_type' => 'required',
            'billing_address' => 'required',
        ];
    }
}
