<?php

namespace App\Imports;

use App\Models\Country;
use App\Imports\ImportCountries;
use Maatwebsite\Excel\Concerns\ToModel;

class CountriesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd("hello");
        // dd($row);
        if(isset($row[0]) && isset($row[1]) && isset($row[2])) {
            return new Country([
                'name' => $row[0],
                'code' => $row[1],
                'dial_code' => $row[2],
            ]);
        }
    }
}
