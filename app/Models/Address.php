<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    public function user(){
        return $this->belongsToOne(User::class);
    }

    public function sentShipments(){
        return $this->hasMany(Shipment::class, "sender_address_id");
    }

    public function receivedShipments(){
        return $this->hasMany(Shipment::class, "receiver_address_id");
    }

    public function country(){
        return $this->belongsTo(Country::class, "country_id", "id");
    }

    public function countryCode() {
        return $this->belongsTo(Country::class, "country_code", "id");
    }
}
