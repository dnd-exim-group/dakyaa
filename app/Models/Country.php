<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    protected $table = "countries";

    protected $fillable = ['name', 'code', 'dial_code'];

    public function services(){
        return $this->belongsToMany(Service::class)->withPivot("duration", "created_at", "updated_at");
    }

    public function quotes() {
        return $this->hasMany(Quote::class);
    }

    public function addresses(){
        return $this->hasMany(Address::class);
    }

    public function vendors() {
        return $this->hasMany(Vendor::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }

    public function country_code_addresses() {
        return $this->hasMany(Address::class, "country_code");
    }

    public function pickupQuotes() {
        return $this->hasMany(Quote::class, "pickup_country_id");
    }

    public function destinationQuotes(){
        return $this->hasMany(Quote::class, "destination_country_id");
    }
}
