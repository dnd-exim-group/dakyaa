<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryService extends Model
{
    use HasFactory;

    protected $table = "country_service";
    protected $guarded = [];

}
