<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PricingLog extends Model
{
    use HasFactory;
    protected $table = "pricing_log";
    protected $guarded = [];

    public function serviceLog() {
        return $this->belongsTo(ServiceLog::class);
    }
}
