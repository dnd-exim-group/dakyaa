<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_code', 'id');
    }

    public function shipment(){
        return $this->hasOne(Shipment::class);
    }

    public function pickup() {
        return $this->belongsTo(Country::class, 'pickup_country_id', 'id');
    }

    public function destination(){
        return $this->belongsTo(Country::class, 'destination_country_id', 'id');
    }

    public function isBooked() {
        return !($this->shipment == null);
    }
}
