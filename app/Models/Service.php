<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

use function PHPUnit\Framework\isEmpty;

class Service extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function pricings(){
        return $this->hasMany(Pricing::class);
    }

    public function countries(){
        return $this->belongsToMany(Country::class)->withPivot("duration","created_at","updated_at");
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class);
    }

    public function shipment(){
        return $this->hasMany(Shipment::class);
    }

    public function hasCountry($country_id){
        return (count($this->countries()->where("country_id","=",$country_id)->get()) > 0);
    }

    public function getDuration(){
        if(!empty($this->countries[0])){
            return $this->countries[0]->pivot->duration;
        }
        return 0;
    }

    public function isVendorActive(): bool {
        return ($this->vendor->active == 1);
    }

    public function serviceLog() {
        return $this->hasMany(ServiceLog::class);
    }

    public function getPrice($weight) {
        $price = $this->pricings()->where('end_kg', '>=', $weight)->skip(0)->take(1)->get();
        if(count($price)==0){
            if($this->further_rate_same_as_late_rate == 1){
                return $this->pricings->last()->price * $weight;
            } else {
                return 0;
            }
        }
        return $price[0]->price * $weight;
    }
}
