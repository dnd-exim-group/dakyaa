<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceLog extends Model
{
    use HasFactory;
    protected $table = "service_log";
    protected $guarded = [];

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function pricingLog() {
        return $this->hasMany(PricingLog::class);
    }
}
