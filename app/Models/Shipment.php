<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function quote(){
        return $this->belongsTo(Quote::class);
    }

    public function senderAddress(){
        return $this->belongsTo(Address::class);
    }

    public function receiverAddress(){
        return $this->belongsTo(Address::class);
    }

    public function boxes(){
        return $this->hasMany(Box::class);
    }

    public function tracking(){
        return $this->hasOne(Tracking::class);
    }

    public function service(){
        return $this->belongsTo(Service::class);
    }
}
