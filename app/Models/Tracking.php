<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function shipment(){
        return $this->belongsTo(Shipment::class);
    }
    public function statuses(){
        return $this->belongsToMany(Status::class)->withPivot('note','created_at');
    }

    public function lastStatus(){
        return $this->statuses->last();
    }
}
