<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role() {
        return $this->belongsTo(Role::class);
    }

    public function addresses() {
        return $this->hasMany(Address::class);
    }

    public function quotes() {
        return $this->hasMany(Quote::class);
    }

    public function shipments() {
        return $this->hasMany(Shipment::class);
    }

    public function isAdmin() {
        return $this->role->name == "admin";
    }

    public function isRoot() {
        return $this->role->name == "root";
    }

    public function isCustomer() {
        return $this->role->name == "customer";
    }

    public function country() {
        return $this->belongsTo(Country::class);
    }
}
