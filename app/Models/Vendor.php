<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Vendor extends Model
{
    protected $guarded = [];
    use HasFactory;
    public function services(){
        return $this->hasMany(Service::class);
    }

    public function country() {
        return $this->belongsTo(Country::class);
    }

    public function deleteImage() {
        if(isset($this->logo)) {
            $absolutePath = explode('/', $this->logo);
            $count = count($absolutePath);
            $path = $absolutePath[$count - 2] . "/" . $absolutePath[$count - 1];
            Storage::delete($path);
        }
    }
}
