<?php

namespace App\Policies;

use App\Models\Quote;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CheckQuoteOwener
{
    use HandlesAuthorization;

    public function isOwener(User $user, Quote $quote) {
        return $quote->user->id == $user->id;
    }
}
