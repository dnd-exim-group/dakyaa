<?php

namespace App\Policies;

use App\Models\Shipment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerShipment
{
    use HandlesAuthorization;

    public function canViewShipment(User $user, Shipment $shipment) {
        return $shipment->user->id == $user->id;
    }
}
