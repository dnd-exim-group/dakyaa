<?php

return [
    'RESIDENT' => 0,
    'COMMERCIAL' => 1,

    'PERSONAL_SHIPMENT' => 0,
    'COMMERCIAL_SHIPMENT' => 1,
    'OTHERS_SHIPMENT' => 2,

    'SHIPMENT_PENDING' => 0,
    'SHIPMENT_APPROVED' => 1,
    'SHIPMENT_REJECTED' => 2,
]

?>
