<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'firstname' => $this->faker->name(),
            'lastname' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'country_code' => "91",
            'phone_no' => 8976895659,
            'address_line1' => $this->faker->streetAddress(),
            'address_line2' => $this->faker->address(),
            'city' => $this->faker->city(),
            'pincode' => $this->faker->postcode(),
            'state_country' => $this->faker->state(),
            'country_id' => rand(1,10),
            'address_type' => config('constants.RESIDENT'),
            'is_saved' => true,
            'is_billing_address' => false,
        ];
    }
}
