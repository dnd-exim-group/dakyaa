<?php

namespace Database\Factories;

use App\Models\Quote;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuoteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Quote::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $shipment_type = ['PERSONAL_SHIPMENT', 'COMMERCIAL_SHIPMENT','OTHERS_SHIPMENT'];

        return [
            'firstname' => $this->faker->name(),
            'lastname' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'country_code' => "91",
            'phone_no' => 8976895659, //$this->faker->phoneNumber()
            'pickup_country' => $this->faker->country(),
            'pickup_pincode' => $this->faker->postcode(),
            'destination_country' => $this->faker->country(),
            'destination_pincode' => $this->faker->postcode(),
            'approx_weight' => rand(5,100),
        ];
    }
}
