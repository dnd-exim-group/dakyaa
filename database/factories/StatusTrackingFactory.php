<?php

namespace Database\Factories;

use App\Models\StatusTracking;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusTrackingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StatusTracking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "note" => $this->faker->sentences(1, true),
        ];
    }
}
