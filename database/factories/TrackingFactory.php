<?php

namespace Database\Factories;

use App\Models\Tracking;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrackingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tracking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tracking_number' => $this->faker->unique()->numberBetween(1000000000,9999999999),
            'awb_number' => $this->faker->unique()->numberBetween(1000000000,9999999999)
        ];
    }
}
