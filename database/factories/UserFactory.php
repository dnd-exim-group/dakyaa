<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserFactory extends Factory
{

    public function definition()
    {
        return [
            'firstname' => $this->faker->name(),
            'lastname' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => Hash::make("abcd123"), // password
            'remember_token' => Str::random(10),
            'admin_created' => false,
            'country_code' => "91",
            'phone_no' => 8976895659,//$this->faker->phoneNumber()
            'provider' => "google",
            'provider_id' => $this->faker->unique()->numberBetween(100000,999999)
        ];
    }

    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
