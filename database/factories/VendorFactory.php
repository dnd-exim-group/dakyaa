<?php

namespace Database\Factories;

use App\Models\Vendor;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Validation\Rules\Unique;

class VendorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vendor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'logo' => "img",
            'address' => $this->faker->address(),
            'country_id' => 1,
            'phone_no' => rand(1000000000, 9999999999),
            'email' => $this->faker->unique()->safeEmail(),
            'active' => 1,
        ];
    }
}
