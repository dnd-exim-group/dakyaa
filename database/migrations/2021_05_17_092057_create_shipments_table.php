<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('quote_id')->nullable();
            $table->unsignedBigInteger('sender_address_id');
            $table->unsignedBigInteger('receiver_address_id');
            $table->integer('shipment_type');
            $table->boolean('payment_status');
            $table->tinyInteger('status');
            $table->tinyInteger('active')->default(1);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->foreign('quote_id')
                ->references('id')
                ->on('quotes');

            $table->foreign('sender_address_id')
                ->references('id')
                ->on('addresses');

            $table->foreign('receiver_address_id')
                ->references('id')
                ->on('addresses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
