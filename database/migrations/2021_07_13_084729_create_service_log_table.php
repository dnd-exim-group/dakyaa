<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("service_id");
            $table->boolean('duty_paid');
            $table->float('base_kg')->nullable();
            $table->integer('price')->nullable();
            $table->tinyInteger("further_rate_same_as_late_rate")->nullable();
            $table->timestamps();

            $table->foreign('service_id')
                ->references('id')
                ->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_log');
    }
}
