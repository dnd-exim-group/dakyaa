<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("service_log_id");
            $table->float('end_kg');
            $table->float('per_kg');
            $table->integer('price');
            $table->timestamps();

            $table->foreign('service_log_id')
                ->references('id')
                ->on('service_log');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing_log');
    }
}
