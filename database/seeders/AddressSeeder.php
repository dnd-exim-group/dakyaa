<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Address;
use App\Models\Country;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i<100; $i++) {
            Country::factory()->create();
        }

        for($i=1; $i<=5; $i++) {
            Address::factory()->count(2)->create(['user_id'=>$i]);
        }
    }
}
