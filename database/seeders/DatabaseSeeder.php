<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Role::create([
        //     "name" => "admin",
        // ]);
        // User::factory(10)->create(['role_id' => 1]);
        $this->call([
            UserSeeder::class,
            // VendorSeeder::class,
            // AddressSeeder::class,
            // ServiceSeeder::class,
            // ShipmentSeeder::class,
        ]);
    }
}
