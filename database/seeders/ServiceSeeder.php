<?php

namespace Database\Seeders;

use App\Models\Pricing;
use App\Models\Service;
use App\Models\Vendor;
use App\Models\Country;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i=1; $i<=6; $i++) {
            $vendorID = Vendor::all()->random()->id;

            $service = Service::create([
                'vendor_id' => $vendorID,
                'name'=> $faker->word(),
                'duty_paid'=>0,
                'base_kg'=>0.5,
                'price'=>300,
                'additional_kg'=>0.5,
                'additional_kg_price'=>260,
                'active'=>1,
            ]);

            $service->pricings()
                ->saveMany(Pricing::factory()->count(1)->make([
                    'service_id' => $i,
                ]));

            $service->countries()->attach("country_id",[
                    "duration" => 4,
                    "country_id" => Country::all()->random()->id,
                ]);
        }
        // $vendor = Vendor::all()->random(1);

        // $service = $vendor->services()->create([
        //     "name" => $this->faker->word(),
        //     "duty_paid" => 0,
        //     "base_kg" => 0.5,
        //     "price" => 500,
        //     "active" => 1,
        // ]);

        // $service->pricings()->create([
        //     "end_kg" => 6,
        //     "per_kg" => 0.5,
        //     "price" => 300,
        // ]);

        // $service->countries()->attach("country_id",[
        //     "duration" => 4,
        //     "country_id" => 1,
        // ]);

    }
}
