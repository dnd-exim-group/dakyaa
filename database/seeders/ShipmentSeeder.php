<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;
use App\Models\Shipment;
use App\Models\Box;
use App\Models\Item;
use App\Models\Quote;
use App\Models\StatusTracking;
use App\Models\Tracking;

class ShipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::create([
            'name'=>'Booking in Progress'
        ]);

        Status::create([
            'name'=>'In Transit'
        ]);

        Status::create([
            'name'=>'Out For Pickup'
        ]);

        Status::create([
            'name'=>'Delivered'
        ]);

        Quote::factory(1)->create();

        $shipment = Shipment::create([
            'user_id' => 1,
            'quote_id' => 1,
            'sender_address_id' => 1,
            'receiver_address_id' => 2,
            'service_id' => 1,
            'status' => config('constants.SHIPMENT_APPROVED'),
            'active' => 1,
            'payment_status' => false,
            'shipment_type' => 0,
        ]);

        $shipment->boxes()
            ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>1]))
            ->each(function($box){
                $i=0;
                $box->items()
                    ->saveMany(Item::factory()->count(6)->make([
                        'box_id'=>1
                ]));
            });

        $shipment->tracking()
            ->saveMany(Tracking::factory()->count(1)->make(['shipment_id'=>1]))
            ->each(function($tracking){
                $tracking->statuses()
                    ->attach('status_id',[
                        'status_id' => rand(1,4),
                        'note' => "Some note for customer",
                    ]);
            });


        $shipment = Shipment::create([
            'user_id' => 2,
            'quote_id' => 1,
            'sender_address_id' => 3,
            'receiver_address_id' => 4,
            'service_id' => 1,
            'active' => 1,
            'status' => config('constants.SHIPMENT_APPROVED'),
            'payment_status' => false,
            'shipment_type' => 0,
        ]);

        $shipment->boxes()
            ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>2]))
            ->each(function($box){
                $i=0;
                $box->items()
                    ->saveMany(Item::factory()->count(6)->make([
                        'box_id'=>2
                ]));
            });

        $shipment->tracking()
            ->saveMany(Tracking::factory()->count(1)->make(['shipment_id'=>2]))
            ->each(function($tracking){
                $tracking->statuses()
                    ->attach('status_id',[
                        'status_id' => rand(1,4),
                        'note' => "Some note for customer",
                    ]);
            });

        // Shipment::factory()->count(1)->create([
        //     'user_id' => 2,
        //     'quote_id' => 2,
        //     'sender_address_id' => 3,
        //     'receiver_address_id' => 4,
        //     'service_id' => 1,
        //     'status' => config('constants.SHIPMENT_APPROVED')
        // ])
        // ->each(function($shipment){
        //     $shipment->boxes()
        //         ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>2]))
        //         ->each(function($box){
        //             $i=0;
        //             $box->items()
        //                 ->saveMany(Item::factory()->count(3)->make([
        //                     'box_id'=>2
        //                 ]));
        //         });
        //     // $shipment->tracking()
        //     //     ->saveMany(Tracking::factory()->count(1)->make(['shipment_id'=>2]))
        //     //     ->each(function($tracking){
        //     //         $tracking->statuses()
        //     //             ->saveMany(StatusTracking::factory()->count(1)->make([
        //     //                 'tracking_id' => 2,
        //     //                 'status_id' => rand(1,4),
        //     //             ]));
        //     //     });
        // });


        $shipment = Shipment::create([
            'user_id' => 3,
            'quote_id' => 1,
            'sender_address_id' => 5,
            'receiver_address_id' => 6,
            'service_id' => 1,
            'active' => 1,
            'status' => config('constants.SHIPMENT_APPROVED'),
            'payment_status' => false,
            'shipment_type' => 0,
        ]);

        $shipment->boxes()
            ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>3]))
            ->each(function($box){
                $i=0;
                $box->items()
                    ->saveMany(Item::factory()->count(3)->make([
                        'box_id'=>3
                ]));
            });

        $shipment->tracking()
            ->saveMany(Tracking::factory()->count(1)->make(['shipment_id'=>3]))
            ->each(function($tracking){
                $tracking->statuses()
                    ->attach('status_id',[
                        'status_id' => rand(1,4),
                        'note' => "Some note for customer",
                    ]);
            });


        // Shipment::factory()->count(1)->create([
        //     'user_id' => 3,
        //     'quote_id' => 3,
        //     'sender_address_id' => 5,
        //     'receiver_address_id' => 6,
        //     'service_id' => 1,
        //     'status' => config('constants.SHIPMENT_REJECTED')
        // ])
        // ->each(function($shipment){
        //     $shipment->boxes()
        //         ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>3]))
        //         ->each(function($box){
        //             $i=0;
        //             $box->items()
        //                 ->saveMany(Item::factory()->count(2)->make([
        //                     'box_id'=>3
        //                 ]));
        //         });
        //     // $shipment->tracking()
        //     //     ->saveMany(Tracking::factory()->count(1)->make(['shipment_id'=>3]))
        //     //     ->each(function($tracking){
        //     //         $tracking->statuses()
        //     //             ->saveMany(StatusTracking::factory()->count(1)->make([
        //     //                 'tracking_id' => 3,
        //     //                 'status_id' => rand(1,4),
        //     //             ]));
        //     //     });
        // });

        $shipment = Shipment::create([
            'user_id' => 4,
            'quote_id' => 1,
            'sender_address_id' => 7,
            'receiver_address_id' => 8,
            'service_id' => 1,
            'active' => 1,
            'status' => config('constants.SHIPMENT_PENDING'),
            'payment_status' => false,
            'shipment_type' => 0,
        ]);

        $shipment->boxes()
            ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>4]))
            ->each(function($box){
                $i=0;
                $box->items()
                    ->saveMany(Item::factory()->count(4)->make([
                        'box_id'=>4
                ]));
            });

        // Shipment::factory()->count(1)->create([
        //     'user_id' => 4,
        //     'quote_id' => 4,
        //     'sender_address_id' => 7,
        //     'receiver_address_id' => 8,
        //     'service_id' => 1,
        //     'status' => config('constants.SHIPMENT_PENDING')
        // ])
        // ->each(function($shipment){
        //     $shipment->boxes()
        //         ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>4]))
        //         ->each(function($box){
        //             $i=0;
        //             $box->items()
        //                 ->saveMany(Item::factory()->count(4)->make([
        //                     'box_id'=>4
        //                 ]));
        //         });
        // });

        $shipment = Shipment::create([
            'user_id' => 5,
            'quote_id' => 1,
            'sender_address_id' => 9,
            'receiver_address_id' => 10,
            'service_id' => 1,
            'active' => 1,
            'status' => config('constants.SHIPMENT_PENDING'),
            'payment_status' => false,
            'shipment_type' => 0,
        ]);

        $shipment->boxes()
            ->saveMany(Box::factory()->count(1)->make(['shipment_id'=>5]))
            ->each(function($box){
                $i=0;
                $box->items()
                    ->saveMany(Item::factory()->count(2)->make([
                        'box_id'=>5
                ]));
            });
    }
}
