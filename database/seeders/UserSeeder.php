<?php

namespace Database\Seeders;

use App\Imports\CountriesImport;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Role;
use App\Models\User;
use App\Models\Quote;
use App\Models\Shipment;
use App\Models\Status;
use App\Models\Country;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role::factory()->count(2)->create()
        //     ->each(function($role){
        //         $role->user()
        //             ->saveMany(User::factory()->count(5)->make())
        //             ->each(function($user){
        //                 $user->quotes()
        //                     ->saveMany(Quote::factory()->count(5)->make())
        //                     ->each(function($quote){
        //                         $quote->shipment()
        //                             ->saveMany(Shipment::factory()->count(5)->make());
        //                     });
        //             });
        //     });

        Excel::import(new CountriesImport, storage_path('Excel/Countries.xls'));

        // DB::table('countries')->insert([
        //     'name' => 'India',
        //     'code' => 'IND',
        //     'dial_code' => '+91',
        //     'created_at' => Carbon::now(),
        //     'updated_at' => Carbon::now(),
        // ]);

        DB::table('roles')->insert([
            'name' => 'root',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('roles')->insert([
            'name' => 'admin',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('roles')->insert([
            'name' => 'customer',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('users')->insert([
            'firstname' => 'root',
            'lastname' => 'root',
            'email' => 'root@root.com',
            'email_verified_at' => now(),
            'password' => Hash::make("root"), // password
            'remember_token' => Str::random(10),
            'admin_created' => true,
            'country_id' => 102,
            'phone_no' => 00,//$this->faker->phoneNumber()
            'provider' => null,
            'provider_id' => null,
            'role_id' => 1
        ]);

        DB::table('users')->insert([
            'firstname' => 'Nirav',
            'lastname' => 'Khatadia',
            'email' => 'info@dndeximltd.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make("Dnd@33"), // password
            'remember_token' => Str::random(10),
            'admin_created' => true,
            'country_id' => 102,
            'phone_no' => 7710098742,//$this->faker->phoneNumber()
            'provider' => null,
            'provider_id' => null,
            'role_id' => 2
        ]);

        DB::table('users')->insert([
            'firstname' => 'Deepali',
            'lastname' => 'Khatadia',
            'email' => 'deepali@dndeximltd.co.uk',
            'email_verified_at' => now(),
            'password' => Hash::make("Dnd@33"), // password
            'remember_token' => Str::random(10),
            'admin_created' => true,
            'country_id' => 102,
            'phone_no' => 7710080742,//$this->faker->phoneNumber()
            'provider' => null,
            'provider_id' => null,
            'role_id' => 2
        ]);

        DB::table('users')->insert([
            'firstname' => 'Raj',
            'lastname' => 'Panchal',
            'email' => 'rajpanchal.3786@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make("Dnd@33"), // password
            'remember_token' => Str::random(10),
            'admin_created' => true,
            'country_id' => 102,
            'phone_no' => 7977199670,//$this->faker->phoneNumber()
            'provider' => null,
            'provider_id' => null,
            'role_id' => 2
        ]);

        Status::create([
            'name'=>'Booking in Progress'
        ]);

        Status::create([
            'name'=>'In Transit'
        ]);

        Status::create([
            'name'=>'Out For Pickup'
        ]);

        Status::create([
            'name'=>'Delivered'
        ]);

        // User::factory()->count(5)->create(['role_id'=>rand(1,2)])
        //     ->each(function($user){
        //         $user->quotes()
        //             ->saveMany(Quote::factory()->count(1)->make());
        //             // ->each(function($quote){
        //             //     $i=0;
        //             //     $quote->shipment()
        //             //         ->saveMany(Shipment::factory()->count(5)->make([
        //             //             "user_id"=>rand(1,5),
        //             //             "quote_id"=>$i,
        //             //         ]));
        //             // });
        //     });

        // factory(User::class, 5)->create()
        //     ->each(function($user){
        //         $user->quotes()
        //             ->saveMany(factory(Quote::class, 5)->make())
        //             ->each(function($quote){
        //                 $quote->shipments()->saveMany(factory(Shipment::class, 5)->make());
        //             });
        // });
    }
}
