$(function(){
    $(".customer").validate({
        rules: {
            'firstname': {
                required: true
            },
            'lastname': {
                required: true
            },
            'email': {
                required: true
            },
            'phone': {
                required: true
            },
            password: {
                required: true
            },
            confirmpassword: {
                required: true,
                equalTo: "#password"
            }

        },
        // messages: {},
        // errorElement : 'div',
        // errorLabelContainer: '.invalid-feedback',
        submitHandler: function (form) {
            form.submit();
        }
    })
});
