function addRow(tableID) {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    console.log(rowCount);

    $("#tableBody").append(`
        <tr>
            <td>
                <input type="text" class="form-control" id="startKg{$rowCount}" name="startKg2" value="0.5" disabled>
            </td>
            <td>
                <input type="text" class="form-control" id="endKg2" name="endKg2" onkeypress="return onlyNumberKey(event)">
            </td>
            <td>
                <input type="text" class="form-control" id="price2" name="price2" onkeypress="return onlyNumberKey(event)">
            </td>
            <td>
                <button class="btn btn-danger" id="button2" name="button2" onclick="removeRow('button2')">
                    <i class="fe fe-trash-2"></i>
                </button>
            </td>
        </tr>
    `);
    // var table = document.getElementById(tableID);
    // var rowCount = table.rows.length;
    // var row = table.insertRow(rowCount);
    // // console.log(rowCount);
    // // validateBeforeAddingRow(tableID);
    // if(true) {
    //     //Column 1

    //     var cell1 = row.insertCell(0);
    //     var element1 = document.createElement("input");
    //     var elementName1 = "startKg" + (rowCount);
    //     element1.name = elementName1;
    //     element1.type = "number";
    //     if(1 == rowCount) {
    //         element1.value = 0.5;
    //     } else {
    //         // console.log(document.getElementsByName('endKg' + (rowCount))[0]);
    //         element1.value = document.getElementsByName('endKg' + (rowCount - 1))[0].value;
    //     }
    //     element1.classList.add("form-control");
    //     element1.required = true;
    //     element1.disabled = true;
    //     cell1.appendChild(element1);

    //     //Column 2
    //     var cell2 = row.insertCell(1);
    //     var element2 = document.createElement("input");
    //     var elementName2 = "endKg" + (rowCount);
    //     element2.name = elementName2;
    //     element2.type = "number";
    //     if(1 == rowCount) {
    //         element2.min = "0.5";
    //     } else {
    //         // console.log(document.getElementsByName('endKg' + (rowCount))[0]);
    //         element2.min = document.getElementsByName('endKg' + (rowCount - 1))[0].value;
    //     }

    //     element2.step = "0.5";
    //     element2.classList.add("form-control");
    //     element2.required = true;
    //     cell2.appendChild(element2);


    //     //Column 3
    //     var cell3 = row.insertCell(2);
    //     var element3 = document.createElement("input");
    //     var elementName3 = "price" + (rowCount);
    //     element3.name = elementName3;
    //     element3.classList.add("form-control", "input-money");
    //     element3.type = "number";
    //     element3.min = "1";
    //     element3.required = true;
    //     cell3.appendChild(element3);

    //     //Column 4
    //     var cell4 = row.insertCell(3);
    //     var element4 = document.createElement("button");
    //     // element4.type = "button";
    //     var btnName = "button" + (rowCount);
    //     element4.name = btnName;
    //     element4.classList.add("btn", "btn-danger");
    //     // element4.textContent  = 'Delete'; // or element4.value = "button";
    //     element4.innerHTML = '<i class="fe fe-trash-2"></i>';
    //     element4.onclick = function() {
    //         removeRow(btnName);
    //     }
    //     element4.required = true;
    //     cell4.appendChild(element4);
    // }
}

function removeRow(btnName) {
    try {
        var table = document.getElementById('pricingTable');
        var rowCount = table.rows.length;
        for (var i = 1; i < rowCount; i++) {
            var row = table.rows[i];
            var rowObj = row.cells[3].childNodes[0];
            // console.log(row.cells[3]);
            // console.log(btnName);
            if (rowObj.name == btnName) {
                table.deleteRow(i);
                rowCount--;
            }
        }
    } catch (e) {
        alert(e);
    }
}
