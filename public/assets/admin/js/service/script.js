$('.countries').select2(
{
    multiple: true,
    theme: 'bootstrap4',
    placeholder: {
        id: '', // the value of the option
        text: 'None Selected'
    },
    allowClear: true,
});
$('.input-money').mask("0,00,00,000",
{
    reverse: true
});

function onlyNumberKey(evt) {
    // Only ASCII character in that range allowed
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
        return false;
    return true;
}

var rowCount;

function addRow(tableID) {
    var table = document.getElementById(tableID);
    rowCount = table.rows.length;
    console.log(rowCount);

    var endKgEle = document.getElementById('endKg_'+(rowCount-1));
    console.log(endKgEle);
    
    var endKg;

    if(endKgEle) {
        endKg = endKgEle.value;
        // console.log(endKgEle.value);
    } else {
        endKg = 0.5;
    }

    if(endKg) {
        $("#tableBody").append(`
            <tr>
                <td>
                    <input type="text" class="form-control" id="startKg_${rowCount}" name="startKg[]" value="${endKg}" disabled>
                </td>
                <td>
                    <input type="text" class="form-control" id="endKg_${rowCount}" name="endKg[]" min="${endKg}" onkeypress="return onlyNumberKey(event)">
                </td>
                <td>
                    <input type="text" class="form-control" id="price_${rowCount}" name="price[]" onkeypress="return onlyNumberKey(event)">
                </td>
                <td>
                    
                </td>
            </tr>
        `);
    }
    
    if(rowCount>1 && endKg) {
        clearAllRemoveBtn();
        $( "#tableBody tr:last-child td:last-child" ).append(`
            <button class="btn btn-danger" id="button_${rowCount}" onclick="removeRow('button_${rowCount}')">
                <i class="fe fe-trash-2"></i>
            </button>
        `)
    }
}

function removeRow(btnName) {
    try {
        document.getElementById(btnName).parentElement.parentElement.remove();

        var table = document.getElementById("pricingTable");
        // var rowCount = table.rows.length;
        // console.log(rowCount);

        var rowCount = $('#tableBody').children().length;
        console.log(rowCount);

        console.log($( "#tableBody tr td:last-child" ));
        
        if(rowCount > 1) {
            clearAllRemoveBtn();
            $( "#tableBody tr:last-child td:last-child" ).append(`
                <button class="btn btn-danger" id="button_${rowCount}" onclick="removeRow('button_${rowCount}')">
                    <i class="fe fe-trash-2"></i>
                </button>
            `)
        }

    } catch (e) {
        alert(e + btnName);
    }
}

function clearAllRemoveBtn() {
    $( "#tableBody tr td:last-child button" ).remove();
}