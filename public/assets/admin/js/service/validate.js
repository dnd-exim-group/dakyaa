$(function(){
    $(".service").validate({
        rules: {
            'name': {
                required: true
            },
            'vendor': {
                required: true
            },
            'countries': {
                required: true
            },
            'duration': {
                required: true
            },
            // 'base_kg': {
            //     required: true
            // },
            // 'base_price': {
            //     required: true
            // },
            // 'additional_kg': {
            //     required: true
            // },
            // 'additional_kg_price': {
            //     required: true
            // },
            // 'endKg[]': {
            //     required: true
            // },
            // 'price[]': {
            //     required: true
            // },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
