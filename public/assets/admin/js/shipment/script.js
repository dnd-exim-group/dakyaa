function fillServices(country_id, route, csrf_token){
    $.ajax({
        url: route,
        method: "POST",
        data:{
            _token: csrf_token,
            'country': country_id,
        },
        dataType: 'json',
        success: function(success){
            $("#courier_services").empty();
            success.forEach(element => {
                $("#courier_services").append(`<option value="${element.id}">${element.name}</option>`);
            });
        }
    });
}

$("#boxes").on('focusout', '.calculate', function(){
    console.log($(this).attr('id').split('_')[1]);

    var box = $(this).attr('id').split('_')[1];
    var length = $.trim($(`#length_${box}`).val());
    var height = $.trim($(`#height_${box}`).val());
    var width = $.trim($(`#width_${box}`).val());
    var mul = (length * width * height)/5000;
    if(mul != 0){
        $(`#volumeWeight_${box}`).val(mul);
    }
});

$("#origin").focusout(function(){
    $("#senderCountry").val($("#origin").children("option").filter(":selected").text());
});
$("#destination").focusout(function(){
    $("#receiverCountry").val($("#destination").children("option").filter(":selected").text());
});

$("#originPincode").keyup(function(){
    $("#senderPincode").val($("#originPincode").val());
});
$("#destinationPincode").keyup(function(){
    $("#receiverPincode").val($("#destinationPincode").val());
});
$("#customerName").keyup(function(){
    $("#senderName").val($("#customerName").val());
});

function addRow(tableID, boxNo) {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;

    $("#"+tableID).append(`
        <tr>
            <td><input type="text" class="form-control" name="itemName_${boxNo}[]"></td>
            <td><input class="form-control" type="number" min="1" name="itemQuantity_${boxNo}[]"></td>
            <td><input class="form-control" type="number" min="1" name="value_${boxNo}[]"></td>
            <td>
                <button id="button_${boxNo}_${rowCount}" name="button_${boxNo}_${rowCount}" class="btn btn-danger" onclick="removeRow('button_${boxNo}_${rowCount}')">
                    <i class="fe fe-trash-2"></i>
                </button>
            </td>
        </tr>
    `);
}

function removeRow(btnName) {
    try {
        document.getElementById(btnName).parentElement.parentElement.remove();
    } catch (e) {
        alert(e + " " + btnName);
    }
}

function addBox() {
    var boxes = document.getElementById("boxes");
    // console.log($('#boxes').children());
    // console.log($('#boxes').children().length);
    var boxCount = $('#boxes').children().length + 1;
    // console.log(boxes);
    $("#boxes").append(
        `<div id="box_${boxCount}" class="col-md-12">
            <h4 class="mr-3 d-inline">Box ${boxCount}</h4>`+
            `${
                (boxCount > 1) ? `<button class="btn btn-sm btn-danger d-inline mb-2" id="box_btn_${boxCount}" onclick="removeBox('box_btn_${boxCount}')"><i class="fe fe-trash-2"></i></button>` : ''
            }`
           +
            `<div class="row mt-3 mb-3">
                <div class="col-md-6 col-sm-12">
                    <div>
                        <label>Dimensions (Length x Width x Height)</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Length</span>
                            </div>
                            <input name="length[]" type="number" class="calculate form-control" id="length_${boxCount}" min="0" required>
                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Height</span>
                            </div>
                            <input name="height[]" type="number" class="calculate form-control" id="height_${boxCount}" min="0" required>
                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Width</span>
                            </div>
                            <input name="width[]" type="number" class="calculate form-control" id="width_${boxCount}" min="0" required>
                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="mb-2">
                        <label for="volumeWeight_1">Volume Weight</label>
                        <input type="number" class="form-control" id="volumeWeight_${boxCount}" readonly>
                    </div>
                    <div>
                        <label for="actualWeight_1">Actual Weight</label>
                        <input name="weight[]" type="number" class="form-control" id="weight_${boxCount}" min="0.1" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <!-- table -->
                    <table id="itemTable_${boxCount}" class="table table-bordered text-center">
                        <thead class="thead-dark">
                            <tr>
                                <th>Item</th>
                                <th>Quantity</th>
                                <th>Value</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="itemTableBody_${boxCount}">
                            <tr>
                                <td><input type="text" class="form-control" name="itemName_${boxCount}[]" required></td>
                                <td><input class="form-control" type="number" min="1" name="itemQuantity_${boxCount}[]" required></td>
                                <td><input class="form-control" type="number" min="1" name="value_${boxCount}[]" required></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="button" class="btn btn-primary mb-4 float-right" value="Add New Item" onclick="addRow('itemTableBody_${boxCount}', '${boxCount}')" />
                </div>
            </div>
        </div>`
    );
}

function removeBox(btnID) {
    try {
        document.getElementById(btnID).parentElement.remove();
    } catch (e) {
        alert(e);
    }
}
