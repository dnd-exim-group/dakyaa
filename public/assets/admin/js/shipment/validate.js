$(function(){
    $(".shipment").validate({
        rules: {
            customer: {
                required: true
            },
            origin: {
                required: true
            },
            originPincode: {
                required: true
            },
            destination: {
                required: true
            },
            destinationPincode: {
                required: true
            },
            service: {
                required: true
            },
            shipmentType: {
                required: true
            },
            senderFirstname: {
                required: true
            },
            senderLastname: {
                required: true
            },
            senderEmail: {
                required: true
            },
            senderPhone: {
                required: true
            },
            senderAddress1: {
                required: true
            },
            senderCity: {
                required: true
            },
            senderState: {
                required: true
            },
            senderAddressType: {
                required: true
            },
            receiverFirstname: {
                required: true
            },
            receiverLastname: {
                required: true
            },
            receiverEmail: {
                required: true
            },
            receiverPhone: {
                required: true
            },
            receiverAddress1: {
                required: true
            },
            receiverCity: {
                required: true
            },
            receiverState: {
                required: true
            },
            receiverAddressType: {
                required: true
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
