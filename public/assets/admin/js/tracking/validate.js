$(function(){
    $(".tracking").validate({
        rules: {
            tracking_number: {
                required: true
            },
            trackingStatus: {
                required: true
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
