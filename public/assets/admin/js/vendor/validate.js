$(function(){
    $(".vendor").validate({
        rules: {
            'name': {
                required: true
            },
            'email': {
                required: true
            },
            'phone': {
                required: true
            },
            'address': {
                required: true
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
