$(function(){
    $("#address-form, #edit-address-form").validate({
        rules: {
            'firstname': {
                required: true
            },
            'lastname': {
                required: true
            },
            'email': {
                required: true,
                email: true
            },
            'phone_no': {
                required: true,
                digits: true
            },
            'address_line1': {
                required: true
            },
            'city': {
                required: true,
            },
            'pincode': {
                required: true,
            },
            'state': {
                required: true,
            },
            'country': {
                required: true,
            },
            'address_type': {
                required: true,
            },
            'billing_address': {
                required: true,
            },
        },
        // messages: {},
        // errorElement : 'div',
        // errorLabelContainer: '.invalid-feedback',
        submitHandler: function (form) {
            form.submit();
        }
    })
});
