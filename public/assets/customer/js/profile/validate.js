$(function(){
    $("#profile-details").validate({
        rules: {
            'firstname': {
                required: true
            },
            'lastname': {
                required: true
            },
        },
        // messages: {},
        // errorElement : 'div',
        // errorLabelContainer: '.invalid-feedback',
        submitHandler: function (form) {
            form.submit();
        }
    })

    $("#update-password").validate({
        rules: {
            'password': {
                required: true,
                minlength: 8,
            },
            confirmpassword: {
                required: true,
                minlength: 8,
                equalTo: "#password",
            },
        },
        submitHandler: function (form) {
            form.submit();
        }
    })
});
