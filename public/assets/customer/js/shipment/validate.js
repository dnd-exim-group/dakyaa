$(function(){
    $(".customer").validate({
        rules: {
            'length[]': {
                required: true
            },
            'height[]': {
                required: true
            },
            'width[]': {
                required: true
            },
            'weight[]': {
                required: true
            },
        },
        // messages: {},
        // errorElement : 'div',
        // errorLabelContainer: '.invalid-feedback',
        submitHandler: function (form) {
            form.submit();
        }
    })
});
