jQuery.extend(jQuery.validator.messages, {
    required: `<p class="error">This field is required.</p>`,
    remote: `<p class="error">Please fix this field</p>`,
    email: `<p class="error">Please enter a valid email address.</p>`,
    url: `<p class="error">Please enter a valid URL.</p>`,
    date: `<p class="error">Please enter a valid date.</p>`,
    dateISO: `<p class="error">Please enter a valid date (ISO).</p>`,
    number: `<p class="error">Please enter a valid number.</p>`,
    digits: `<p class="error">Please enter only digits.</p>`,
    creditcard: `<p class="error">Please enter a valid credit card number.</p>`,
    equalTo: `<p class="error">Please enter the same value again.</p>`,
    accept: `<p class="error">Please enter a value with a valid extension.</p>`,
    maxlength: jQuery.validator.format(`<p class="error">Please enter no more than {0} characters.</p>`),
    minlength: jQuery.validator.format(`<p class="error">Please enter at least {0} characters.</p>`),
    rangelength: jQuery.validator.format(`<p class="error">Please enter a value between {0} and {1} characters long.</p>`),
    range: jQuery.validator.format(`<p class="error">Please enter a value between {0} and {1}.</p>`),
    max: jQuery.validator.format(`<p class="error">Please enter a value less than or equal to {0}.</p>`),
    min: jQuery.validator.format(`<p class="error">Please enter a value greater than or equal to {0}.</p>`)
});