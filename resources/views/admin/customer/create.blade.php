@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Customers</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Customer</strong>
                </div>
                <div class="card-body">
                    <form id="add-customer" class="customer" action="{{route('admin.customer.store')}}" method="POST">
                        @csrf
                        @method('POST')
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">First Name</label>
                                <input name="firstname" type="text" class="form-control @error('firstname') 'is-invalid' @enderror"  value="{{old('firstname')}}" id="firstname">
                                @error('firstname')
                                    <small class="text-danger">{{$errors->first('firstname')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastname">Last name</label>
                                <input name="lastname" type="text" class="form-control" id="lastname" value="{{old('lastname')}}">
                                @error('lastname')
                                    <small class="text-danger">{{$errors->first('lastname')}}</small>
                                @enderror
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email">Email address</label>
                                <input name="email" value="{{old('email')}}" type="email" class="form-control" id="email" required>
                                @error('email')
                                    <small class="text-danger">{{$errors->first('email')}}</small>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="phone">Phone Number</label>
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <select class="form-control select2" id="simple-select2" name="dial_code">
                                            <?php
                                                foreach ($countries as $country) {
                                            ?>
                                                <option value="{{$country->id}}" {{ $country->name == 'India' ? 'selected' : '' }}> {{$country->dial_code . ' ' . $country->code}} </option>
                                            <?php
                                                }
                                            ?>
                                            @error('dial_code')
                                                <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                            @enderror
                                        </select>
                                    </div>
                                    <div class="col-md-10">
                                        <input name="phone" class="form-control input-phoneus" id="phone" maxlength="14" value="{{old('phone')}}">
                                        @error('phone')
                                            <small class="text-danger">{{$errors->first('phone')}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row mb-3">
                            <div class="col-md-6 mb-3">
                                <label for="password">Password</label>
                                <input name="password" type="password" class="form-control" id="password" value="{{old('password')}}">
                                @error('password')
                                    <small class="text-danger">{{$errors->first('password')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="confirmpassword">Confirm Password</label>
                                <input name="confirmpassword" type="password" class="form-control" id="confirmpassword" value="{{old('confirmpassword')}}">
                                @error('confirmpassword')
                                    <small class="text-danger">{{$errors->first('confirmpassword')}}</small>
                                @enderror
                            </div>
                        </div>
                        <!-- <button class="btn btn-primary" type="submit">Submit</button> -->
                        <input type="submit" class="btn btn-primary" name="add_customer" value="Submit">
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/customer/validate.js') }}"></script>

@endsection
