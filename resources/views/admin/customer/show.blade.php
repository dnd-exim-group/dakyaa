@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Customers</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Customer</strong>
                </div>
                <div class="card-body">
                    <form class="needs-validation" action="{{route('admin.customer.update',$user->id)}}" method="POST" novalidate>
                        @csrf
                        @method('PUT')
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">First Name</label>
                                <input name="firstname" type="text" class="form-control" id="firstname" value="{{$user->firstname}}" readonly>
                                <div class="valid-feedback"> Looks good! </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastname">Last name</label>
                                <input name="lastname" type="text" class="form-control" id="lastname" value="{{$user->lastname}}" readonly>
                                <div class="valid-feedback"> Looks good! </div>
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email">Email address</label>
                                <input name="email" type="email" class="form-control" id="email" value="{{$user->email}}" readonly>
                                <div class="invalid-feedback"> Please use a valid email </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="phone">Phone Number</label>
                                <div class="form-row">
                                    <div class="col-md-2">
                                        <label class="form-control">{{$user->country->dial_code}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input name="phone" class="form-control input-phoneus" id="phone" maxlength="14" value="{{old('phone', $user->phone_no)}}" readonly>
                                        @error('phone')
                                            <small class="text-danger">{{$errors->first('phone')}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        <a class="btn btn-primary" href="{{route('admin.customers')}}">Back</a>
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/global/js/jquery.validate.min.js') }}"></script>
<script>
    (function()
    {
        'use strict';
        window.addEventListener('load', function()
        {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form)
            {
                form.addEventListener('submit', function(event)
                {
                    if (form.checkValidity() === false)
                    {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
