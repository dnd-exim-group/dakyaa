@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h5 page-title">Welcome, {{Auth::user()->firstname}}</h1>
            {{-- <button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#getAQuote"> Get A Quote </button> --}}

            {{-- <form action="{{ route("shipments.masterReportExport") }}" method="GET" id="active-shipment-excel-form">
                @csrf
                @method("GET")
                <div class="input-group input-daterange">
                    <div class="form-group">
                        <button type="submit" name="filter" id="filter" class="btn btn-info">
                            <i class="fe fe-download fe-16 mr-1"></i>
                            Download Active Shipment Report
                        </button>
                    </div>
                </div>
            </form> --}}
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection
