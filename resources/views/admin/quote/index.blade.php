@extends('layouts.admin')

@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title">Quotes</h1>
            <div class="row my-4">
                <!-- Small table -->
                <div class="col-md-12">
                    {{-- {{ dd($quotes) }} --}}
                    <div class="card shadow">
                        <div class="card-body">
                            <!-- table -->
                            <table class="table datatables" id="quoteDataTable">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email ID</th>
                                        <th>Phone No</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Approx Weight</th>
                                        <th>Shipment Type</th>
                                        <th>Booking Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach ($quotes as $quote) {
                                            // dd($quote->pickup);
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td>{{ $quote->firstname }}</td>
                                        <td>{{ $quote->lastname }}</td>
                                        <td>{{ $quote->email }}</td>
                                        <td>{{ $quote->country->dial_code.' '.$quote->phone_no }}</td>
                                        <td>{{ $quote->pickup->name.' - '.$quote->pickup_pincode }}</td>
                                        <td>{{ $quote->destination->name.' - '.$quote->destination_pincode }}</td>
                                        <td>{{ $quote->approx_weight }}</td>
                                        <td>
                                            <?php
                                                if ($quote->shipment_type == config("constants.PERSONAL_SHIPMENT")) {
                                            ?>
                                                Personal/Gifts
                                            <?php
                                                }
                                                elseif ($quote->shipment_type == config("constants.COMMERCIAL_SHIPMENT")) {
                                            ?>
                                                Commercial
                                            <?php
                                                }
                                                elseif ($quote->shipment_type == config("constants.OTHERS_SHIPMENT")) {
                                            ?>
                                                Others
                                            <?php
                                                }
                                            ?>
                                        </td>
                                        <td>{{ $quote->shipment ? 'Yes' : 'No' }}</td>
                                    </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- simple table -->
            </div> <!-- end section -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#quoteDataTable').DataTable(
    {
        autoWidth: true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ]
    });
</script>

@endsection
