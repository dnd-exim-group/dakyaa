@extends('layouts.admin')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/global/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/select2-bootstrap4.css') }}">
@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Services</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Service</strong>
                </div>
                <div class="card-body">
                    <form id="add-service" class="service" action="{{route('admin.service.store')}}" method="POST">
                        @csrf
                        @method('POST')
                        <p class="mb-2"><strong>Service Details</strong></p>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="name">Name</label>
                                <input name="name" type="text" class="form-control" id="name" value="{{old('name')}}">
                                @error('name')
                                    <small class="text-danger">{{$errors->first('name')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="vendor">Select Vendor</label>
                                <select name="vendor" class="form-control select2" id="vendor">
                                    <?php
                                        foreach ($vendors as $vendor) {
                                    ?>
                                        <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label for="duty_paid">Duty Paid</label>
                                <div class="custom-control custom-switch">
                                    <input name="duty_paid" type="checkbox" class="custom-control-input" id="duty_paid">
                                    <label class="custom-control-label" for="duty_paid">Paid</label>
                                    @error('duty_paid')
                                        <small class="text-danger">{{$errors->first('duty_paid')}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-8 mb-3">
                                <label for="countries">Select Country OR List Of Countries</label>
                                <select name="countries[]" class="form-control countries" id="countries">
                                    <?php
                                        foreach ($countries as $country) {
                                    ?>
                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="duration">Shipping Duration</label>
                                <input name="duration" type="number" class="form-control" id="duration" min="1" value="{{old('duration')}}">
                                @error('duration')
                                    <small class="text-danger">{{$errors->first('duration')}}</small>
                                @enderror
                            </div>
                        </div> <!-- /.form-row -->
                        <hr class="mb-4">
                        <p class="mb-2"><strong>Pricing</strong></p>
                        {{-- <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="base_kg">Base Kg</label>
                                <input name="base_kg" type="number" id="base_kg" class="form-control" min="0.5" step="0.5" value="0.5" disabled>
                                @error('base_kg')
                                    <small class="text-danger">{{$errors->first('base_kg')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="base_price">Price</label>
                                <input name="base_price" class="form-control" id="base_price" type="text" name="money" value="{{old('base_price')}}" onkeypress="return onlyNumberKey(event)">
                                @error('base_price')
                                    <small class="text-danger">{{$errors->first('base_price')}}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="additional_kg">Additional Kg</label>
                                <input name="additional_kg" type="number" id="additional_kg" class="form-control" min="0.5" step="0.5" value="0.5" disabled>
                                @error('additional_kg')
                                    <small class="text-danger">{{$errors->first('additional_kg')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label for="additional_kg_price">Price</label>
                                <input name="additional_kg_price" class="form-control" id="additional_kg_price" type="text" name="money" value="{{old('additional_kg_price')}}" onkeypress="return onlyNumberKey(event)">
                                @error('additional_kg_price')
                                    <small class="text-danger">{{$errors->first('additional_kg_price')}}</small>
                                @enderror
                            </div>
                        </div> --}}
                        <div class="form-row mb-3">
                            <div class="col">
                                <!-- table -->
                                <table id="pricingTable" class="table table-bordered text-center">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Start Kg</th>
                                            <th>End Kg</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableBody">
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control" id="startKg_1" name="startKg[]" value="1" disabled>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="endKg_1" name="endKg[]" onkeypress="return onlyNumberKey(event)" required>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="price_1" name="price[]" onkeypress="return onlyNumberKey(event)" required>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="m-3">
                            <label for="further_rate_same_as_late_rate">If this service has further Kg rate same as the last Kg rate Please turn the toggle ON.</label>
                            <div class="custom-control custom-switch">
                                <input name="further_rate_same_as_late_rate" type="checkbox" class="custom-control-input" id="further_rate_same_as_late_rate">
                                <label class="custom-control-label" for="further_rate_same_as_late_rate"></label>
                                @error('further_rate_same_as_late_rate')
                                    <small class="text-danger">{{$errors->first('further_rate_same_as_late_rate')}}</small>
                                @enderror
                            </div>
                        </div>
                        <!-- <div>
                            <input type="button" id="add-row-btn" class="btn btn-danger float-right d-block mr-3" value="Reset" onclick="addRow('pricingTable')" />
                        </div> -->
                        <div>
                            <input type="button" id="add-more-kg-btn" class="btn btn-primary float-right d-block mr-3" value="Add More Kg" onclick="addRow('pricingTable')" />
                        </div>

                        <input type="submit" class="btn btn-primary" name="add_service" value="Submit">
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/global/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/service/validate.js') }}"></script>
<script src="{{ asset('assets/admin/js/service/script.js') }}"></script>

<script>

</script>

@endsection
