@extends('layouts.admin')

@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">

@endsection

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title">Services</h1>
            <div class="row my-4">
                <!-- Small table -->
                <div class="col-md-12">
                    <div class="card shadow">
                        <div class="card-body">
                            <!-- table -->
                            <table class="table datatables" id="customersDataTable">
                                <thead>
                                    <tr>
                                        <th>Service Name</th>
                                        <th>Vendor</th>
                                        <th>Countries</th>
                                        <th>Duty Paid</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i=1;
                                        foreach ($services as $service) {
                                    ?>
                                        <tr>
                                            <td>{{$service->name}}</td>
                                            <td>{{$service->vendor->name}}</td>
                                            <td>
                                                <?php
                                                    foreach ($service->countries as $country) {
                                                ?>
                                                    {{$country->name}},
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                            <td>{{$service->duty_paid==0 ? 'No Duty' : "Duty Paid"}}</td>
                                            <td>
                                                <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="text-muted sr-only">Action</span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{route('admin.service.edit',$service)}}"><i class="fe fe-edit mr-1 text-warning"></i> Edit</a>
                                                    <form action="{{route('admin.service.inactive',$service)}}" method="POST">
                                                        @csrf
                                                        @method("PUT")
                                                        <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Inactive</button>
                                                    </form>
                                                    <a class="dropdown-item" href="{{route('admin.service.show',$service)}}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- simple table -->
            </div> <!-- end section -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
@endsection

@section('scripts')


<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#customersDataTable').DataTable(
    {
        autoWidth: true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ]
    });
</script>

@endsection
