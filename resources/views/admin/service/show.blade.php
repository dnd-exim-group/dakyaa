@extends('layouts.admin')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/global/css/select2.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/global/css/select2-bootstrap4.css') }}">
@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Services</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Service</strong>
                </div>
                <div class="card-body">
                    <form class="needs-validation" novalidate>
                        <p class="mb-2"><strong>Service Details</strong></p>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom3">Name</label>
                                <input name="name" type="text" class="form-control" id="validationCustom3" value="{{$service->name}}" readonly>
                                <div class="valid-feedback"> Looks good! </div>
                                <div class="invalid-feedback"> Please enter a service name </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="simple-select2">Vendor</label>
                                <input name="name" type="text" class="form-control" id="validationCustom3" value="{{$service->vendor->name}}" readonly>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="approxWeight">Duty Paid</label>
                                <input name="duty_paid" type="text" value="{{$service->duty_paid == 0 ? "No" : "Yes"}}" class="form-control" id="customSwitch1" readonly>
                            </div>
                            <div class="col-md-8 mb-3">
                                <label for="multi-select2">Select Country OR List Of Countries</label>
                                <input name="duty_paid" type="text" class="form-control" id="customSwitch1" value="<?php
                                    foreach ($service->countries as $country) {
                                        echo $country->name.", ";
                                    }
                                ?>" readonly>

                                <div class="valid-feedback"> Looks good! </div>
                                <div class="invalid-feedback"> Please select atleast one country </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom3">Shipping Duration (in Days)</label>
                                <input name="duration" type="number" class="form-control" id="validationCustom3" min="1" value="{{$service->getDuration()}}" readonly>
                                <div class="valid-feedback"> Looks good! </div>
                                <div class="invalid-feedback"> Please enter a Duration </div>
                            </div>
                        </div> <!-- /.form-row -->
                        <hr class="mb-4">
                        <p class="mb-2"><strong>Pricing</strong></p>
                        {{-- <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="baseKg">Base Kg</label>
                                <input value="{{$service->base_kg}}" name="base_kg" type="number" id="baseKg" class="form-control" min="0.5" step="0.5" readonly>
                            </div>
                            <div class="col-md-6">
                                <label for="basePrice">Price</label>
                                <input value="{{$service->price}}" name="base_price" class="form-control input-money" id="basePrice" type="text" name="money" readonly>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="additional_kg">Additional Kg</label>
                                <input value="{{$service->additional_kg}}" name="additional_kg" type="number" id="additional_kg" class="form-control" min="0.5" step="0.5" readonly>
                            </div>
                            <div class="col-md-6">
                                <label for="additional_kg_price">Price</label>
                                <input value="{{$service->additional_kg_price}}" name="additional_kg_price" class="form-control" id="additional_kg_price" type="text" name="money" readonly>
                            </div>
                        </div> --}}
                        <div class="form-row mb-3">
                            <div class="col">
                                <!-- table -->
                                <table id="pricingTable" class="table table-bordered text-center">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Start Kg</th>
                                            <th>End Kg</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tableBody">
                                        <?php
                                            $prices = $service->pricings;
                                            for ($i=0 ; $i < count($prices) ; $i++) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control" id="startKg_1" name="startKg[]" value="{{$i==0 ? "1" : $prices[$i-1]->end_kg}}" readonly>
                                                </td>
                                                <td>
                                                    <input value="{{$prices[$i]->end_kg}}" type="text" class="form-control" id="endKg_1" name="endKg[]" onkeypress="return onlyNumberKey(event)" readonly>
                                                </td>
                                                <td>
                                                    <input value="{{$prices[$i]->price}}" type="text" class="form-control" id="price_1" name="price[]" onkeypress="return onlyNumberKey(event)" readonly>
                                                </td>
                                            </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="mb-4">
                            <div class="col-md-4">
                                <input name="further_rate_same_as_late_rate" type="text" value="{{$service->further_rate_same_as_late_rate == 0 ? "This service does not accept more than last specified KG" : "This service has further Kg rate same as the last Kg rate"}}" class="form-control" id="further_rate_same_as_late_rate" readonly>
                            </div>
                        </div>

                        <a href="{{route('admin.services')}}" class="btn btn-primary">Go Back</a>
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/global/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/global/js/select2.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.mask.min.js') }}"></script>
<script>
    (function()
    {
        'use strict';
        window.addEventListener('load', function()
        {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form)
            {
                form.addEventListener('submit', function(event)
                {
                    if (form.checkValidity() === false)
                    {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
    $('.countries').select2(
    {
        multiple: true,
        theme: 'bootstrap4',
        placeholder: {
            id: '', // the value of the option
            text: 'None Selected'
        },
        allowClear: true,
    });
    $('.input-money').mask("0,00,00,000",
    {
        reverse: true
    });

    function onlyNumberKey(evt) {
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }

    function validateBeforeAddingRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        // var row = table.insertRow(rowCount);
        // console.log(rowCount);
        // if(1 == rowCount) {
        //     var element = document.getElementsByName('endKg'+(rowCount+1))[0];
        //     // console.log(rowCount);
        //     console.log(element);
        //     if(document.getElementsByName('endKg' + (rowCount+2))[0]) {
        //         console.log(rowCount+2);
        //         element.max = document.getElementsByName('endKg' + (rowCount+2))[0].value;
        //     }
        // }

        if(0 == rowCount || 1 == rowCount) {
            return true;
        }
        else if(1 < rowCount) {
            var endKg = document.getElementById('endKg_'+(rowCount-1));
            console.log(endKg);
            if(endKg.value) {
                return true;
            }
        }

        return false;
    }


    function addRow(tableID) {
        // var table = document.getElementById(tableID);
        // var rowCount = table.rows.length;
        // console.log(rowCount);
        // $("#tableBody").append(`
        //     <tr>
        //         <td>
        //             <input type="text" class="form-control" id="startKg_${rowCount}" name="startKg_${rowCount}" value="0.5" disabled>
        //         </td>
        //         <td>
        //             <input type="text" class="form-control" id="endKg_${rowCount}" name="endKg_${rowCount}" onkeypress="return onlyNumberKey(event)">
        //         </td>
        //         <td>
        //             <input type="text" class="form-control" id="price_${rowCount}" name="price_${rowCount}" onkeypress="return onlyNumberKey(event)">
        //         </td>
        //         <td>
        //             <button class="btn btn-danger" id="button_${rowCount}" name="button_${rowCount}" onclick="removeRow('button_${rowCount}')">
        //                 <i class="fe fe-trash-2"></i>
        //             </button>
        //         </td>
        //     </tr>
        // `);

        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;

        console.log(rowCount);

        if(validateBeforeAddingRow(tableID)) {
            var row = table.insertRow(rowCount);
            //Column 1
            var cell1 = row.insertCell(0);
            var element1 = document.createElement("input");
            var elementName1 = "startKg_" + (rowCount);
            element1.name = "startKg[]";
            element1.id = elementName1;
            element1.type = "number";
            if(1 == rowCount) {
                element1.value = 0.5;
            } else {
                // console.log(document.getElementsByName('endKg' + (rowCount))[0]);
                element1.value = document.getElementById('endKg_' + (rowCount - 1)).value;
            }
            element1.classList.add("form-control");
            element1.required = true;
            element1.disabled = true;
            cell1.appendChild(element1);

            //Column 2
            var cell2 = row.insertCell(1);
            var element2 = document.createElement("input");
            var elementName2 = "endKg_" + (rowCount);
            element2.name = "endKg[]";
            element2.id = elementName2;
            element2.type = "number";
            if(1 == rowCount) {
                element2.min = "0.5";
            } else {
                // console.log(document.getElementsByName('endKg' + (rowCount))[0]);
                element2.min = document.getElementById('endKg_' + (rowCount - 1)).value;
                // if(document.getElementById('endKg_' + (rowCount+1))) {
                //     element2.max = document.getElementById('endKg_' + (rowCount+1))[0].value
                // }
            }
            element2.step = "0.5";
            element2.classList.add("form-control");
            element2.required = true;
            cell2.appendChild(element2);

            //Column 3
            var cell3 = row.insertCell(2);
            var element3 = document.createElement("input");
            var elementName3 = "price_" + (rowCount);
            element3.name = "price[]";
            element3.id = elementName3;
            element3.classList.add("form-control", "input-money");
            element3.type = "number";
            element3.min = "1";
            element3.required = true;
            cell3.appendChild(element3);


            //Column 4
            var cell4 = row.insertCell(3);
            var element4 = document.createElement("button");
            // element4.type = "button";
            var btnName = "button_" + (rowCount);
            element4.name = btnName;
            element4.id = btnName;
            element4.classList.add("btn", "btn-danger");
            // element4.textContent  = 'Delete'; // or element4.value = "button";
            element4.innerHTML = '<i class="fe fe-trash-2"></i>';
            element4.onclick = function() {
                removeRow(btnName);
            }
            element4.required = true;
            cell4.appendChild(element4);
        }
    }

    function removeRow(btnName) {
        try {
            document.getElementById(btnName).parentElement.parentElement.remove();
        } catch (e) {
            alert(e);
        }
    }
</script>

@endsection
