@extends('layouts.admin')
@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
{{-- <!-- <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.steps.css') }}"> --> --}}

@endsection
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <form action="{{route('admin.shipment.store')}}" method="POST" id="shipment-form" class="shipment">
                @csrf
                @method("POST")
                <div class="row my-4">
                    <div class="col">
                        <h1 class="h2 page-title">Shipment</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Add Shipment</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <h3 class="mb-3">Customer Details</h3>
                                    <section class="row">
                                        <div class="col-md-4">

                                            <div class="mb-3">
                                                <label for="customer">Select Customer</label>
                                                <select name="customer" class="form-control select2" id="customer">
                                                    <option value="">Select Customer</option>
                                                <?php
                                                    foreach ($users as $user) {
                                                        if ($user->role_id == 3) {
                                                ?>
                                                        <option value="{{$user->id}}">{{$user->firstname . " " . $user->lastname}}</option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                                    </select>
                                                @error('customer')
                                                    <small class="text-danger">{{$errors->first('customer')}}</small>
                                                @enderror
                                            </div>

                                            <div class="mb-3">
                                                <label for="origin">Origin</label>
                                                <select name="origin" class="form-control select2" id="origin">
                                                    <option value="">Select Orign</option>
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{ $country->name == 'India' ? 'selected' : ''; }} {{ $country->name != 'India' ? 'hidden' : ''; }}>{{$country->name}}</option>
                                                        <option value="{{$country->id}}" {{ $country->name != 'United Kingdom' ? 'hidden' : ''; }}>{{$country->name}}</option>
                                                <?php
                                                    }
                                                ?>
                                                    </select>
                                                @error('origin')
                                                    <small class="text-danger">{{$errors->first('origin')}}</small>
                                                @enderror
                                            </div>

                                            <div class="mb-3">
                                                <label for="originPincode">Origin Pincode</label>
                                                <input name="originPincode" value="{{old('originPincode')}}" type="text" class="form-control" id="originPincode">
                                                @error('originPincode')
                                                    <small class="text-danger">{{$errors->first('originPincode')}}</small>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="mb-3">
                                                <label for="destination">Destination</label>
                                                <select name="destination" class="form-control select2" id="destination" >
                                                    <option value="">Select Destination</option>
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{ $country->name == 'India' ? 'hidden' : ''; }}>{{$country->name}}</option>
                                                <?php
                                                    }
                                                ?>
                                                    </select>
                                                @error('destination')
                                                    <small class="text-danger">{{$errors->first('destination')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="destinationPincode">Destination Pincode</label>
                                                <input name="destinationPincode" value="{{old('destinationPincode')}}" type="text" class="form-control" id="destinationPincode">
                                                @error('destinationPincode')
                                                    <small class="text-danger">{{$errors->first('destinationPincode')}}</small>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-4">

                                            <div class="mb-3">
                                                <label for="service">Services</label>
                                                <select name="service" class="form-control select2" id="courier_services">

                                                </select>
                                                @error('service')
                                                    <small class="text-danger">{{$errors->first('service')}}</small>
                                                @enderror
                                            </div>

                                            <div class="mb-3">
                                                <label>Shipment Type</label>
                                                <div class="form-row">
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-2">
                                                            <input type="radio" class="custom-control-input" id="shipmentType1" name="shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}" checked>
                                                            <label class="custom-control-label" for="shipmentType1">Personal/Gift</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-2">
                                                            <input type="radio" class="custom-control-input" id="shipmentType2" name="shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}">
                                                            <label class="custom-control-label" for="shipmentType2">Commercial</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" id="shipmentType3" name="shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}">
                                                            <label class="custom-control-label" for="shipmentType3">Others</label>
                                                        </div>
                                                    </div>
                                                    @error('shipmentType')
                                                        <small class="text-danger">{{$errors->first('shipmentType')}}</small>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="mb-3">
                                                <label for="paymentStatus">Payment Status</label>
                                                <div class="custom-control custom-switch">
                                                    <input name="paymentStatus" value="{{old('paymentStatus')}}" type="checkbox" class="custom-control-input" id="paymentStatus">
                                                    <label class="custom-control-label" for="paymentStatus">Paid</label>
                                                </div>
                                                @error('paymentStatus')
                                                    <small class="text-danger">{{$errors->first('paymentStatus')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Sender Details</h2>
                                        <div class="mb-3">
                                            <label for="senderFirstname">First Name</label>
                                            <input name="senderFirstname" value="{{old('senderFirstname')}}" type="text" class="form-control" id="senderFirstname">
                                            @error('senderFirstname')
                                                <small class="text-danger">{{$errors->first('senderFirstname')}}</small>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="senderLastname">Last Name</label>
                                            <input name="senderLastname" value="{{old('senderLastname')}}" type="text" class="form-control" id="senderLastname">
                                            @error('senderLastname')
                                                <small class="text-danger">{{$errors->first('senderLastname')}}</small>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="senderEmail">Email ID</label>
                                            <input name="senderEmail" value="{{old('senderEmail')}}" type="email" class="form-control" id="senderEmail">
                                            @error('senderEmail')
                                                <small class="text-danger">{{$errors->first('senderEmail')}}</small>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="senderPhone">Phone Number</label>
                                            <div class="form-row">
                                                <div class="col-3">
                                                    <select class="form-control select2" id="simple-select2" name="senderCountryCode">
                                                        <?php
                                                            foreach ($countries as $country) {
                                                        ?>
                                                            <option value="{{$country->id}}" {{ $country->name == 'India' ? 'selected' : ''; }}> {{$country->dial_code.' '.$country->code}} </option>
                                                        <?php
                                                            }
                                                        ?>
                                                        @error('senderCountryCode')
                                                            <small class="text-danger">{{$errors->first('senderCountryCode')}}</small>
                                                        @enderror
                                                    </select>
                                                </div>
                                                <div class="col-9">
                                                    <input type="text" name="senderPhone" value="{{old('senderPhone')}}" class="form-control" id="senderPhone">
                                                    @error('senderPhone')
                                                        <small class="text-danger">{{$errors->first('senderPhone')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddress1">Address Line 1</label>
                                            <input name="senderAddress1" value="{{old('senderAddress1')}}" type="text" class="form-control" id="senderAddress1">
                                            @error('senderAddress1')
                                                <small class="text-danger">{{$errors->first('senderAddress1')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddress2">Address Line 2 <small> (This field is optional)</small></label>
                                            <input name="senderAddress2" value="{{old('senderAddress2')}}" type="text" class="form-control" id="senderAddress2">
                                            @error('senderAddress2')
                                                <small class="text-danger">{{$errors->first('senderAddress2')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCity">City</label>
                                            <input name="senderCity" value="{{old('senderCity')}}" type="text" class="form-control" id="senderCity">
                                            @error('senderCity')
                                                <small class="text-danger">{{$errors->first('senderCity')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderPincode">Pincode</label>
                                            <input type="text" class="form-control" id="senderPincode" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderState">State</label>
                                            <input name="senderState" value="{{old('senderState')}}" type="text" class="form-control" id="senderState">
                                            @error('senderState')
                                                <small class="text-danger">{{$errors->first('senderState')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCountry">Country</label>
                                            <input type="text" class="form-control" id="senderCountry" name="senderCountry" value="India" readonly>
                                        </div>
                                        <div>
                                            <label for="senderAddressType">Address Type</label>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-2">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.RESIDENT")}}" id="senderAddressTypeResidential" name="senderAddressType" checked>
                                                        <label class="custom-control-label" for="senderAddressTypeResidential">Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="senderAddressTypeCommercial" name="senderAddressType">
                                                        <label class="custom-control-label" for="senderAddressTypeCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                                @error('senderAddressType')
                                                    <small class="text-danger">{{$errors->first('senderAddressType')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Receiver Details</h2>
                                        <div class="mb-3">
                                            <label for="receiverFirstname">First Name</label>
                                            <input name="receiverFirstname" value="{{old('receiverFirstname')}}" type="text" class="form-control" id="receiverFirstname">
                                            @error('receiverFirstname')
                                                <small class="text-danger">{{$errors->first('receiverFirstname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverLastname">Last Name</label>
                                            <input name="receiverLastname" value="{{old('receiverLastname')}}" type="text" class="form-control" id="receiverLastname">
                                            @error('receiverLastname')
                                                <small class="text-danger">{{$errors->first('receiverLastname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverEmail">Email ID</label>
                                            <input name="receiverEmail" value="{{old('receiverEmail')}}" type="email" class="form-control" id="receiverEmail">
                                            @error('receiverEmail')
                                                <small class="text-danger">{{$errors->first('receiverEmail')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPhone">Phone Number</label>
                                            <div class="form-row">
                                                <div class="col-3">
                                                    <select class="form-control select2" id="simple-select2" name="receiverCountryCode">
                                                        <?php
                                                            foreach ($countries as $country) {
                                                        ?>
                                                            <option value="{{$country->id}}" {{ $country->id == 102 ? 'selected' : ''; }}> {{$country->dial_code.' '.$country->code}} </option>
                                                        <?php
                                                            }
                                                        ?>
                                                        @error('senderCountryCode')
                                                            <small class="text-danger">{{$errors->first('senderCountryCode')}}</small>
                                                        @enderror
                                                    </select>
                                                </div>
                                                <div class="col-9">
                                                    <input type="text" name="receiverPhone" value="{{old('receiverPhone')}}" class="form-control" id="receiverPhone">
                                                    @error('receiverPhone')
                                                        <small class="text-danger">{{$errors->first('receiverPhone')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress1">Address Line 1</label>
                                            <input name="receiverAddress1" value="{{old('receiverAddress1')}}" type="text" class="form-control" id="receiverAddress1">
                                            @error('receiverAddress1')
                                                <small class="text-danger">{{$errors->first('receiverAddress1')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress2">Address Line 2</label>
                                            <input name="receiverAddress2" value="{{old('receiverAddress2')}}" type="text" class="form-control" id="receiverAddress2">
                                            @error('receiverAddress2')
                                                <small class="text-danger">{{$errors->first('receiverAddress2')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCity">City</label>
                                            <input name="receiverCity" value="{{old('receiverCity')}}" type="text" class="form-control" id="receiverCity">
                                            @error('receiverCity')
                                                <small class="text-danger">{{$errors->first('receiverCity')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPincode">Pincode</label>
                                            <input type="text" class="form-control" id="receiverPincode" name="receiverPincode" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverState">State</label>
                                            <input name="receiverState" value="{{old('receiverState')}}" type="text" class="form-control" id="receiverState">
                                            @error('receiverState')
                                                <small class="text-danger">{{$errors->first('receiverState')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCountry">Country</label>
                                            <input name="receiverCountry" type="text" class="form-control" id="receiverCountry" readonly>
                                        </div>
                                        <div>
                                            <label for="receiverAddressType">Address Type</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-2">
                                                        <input type="radio" class="custom-control-input" id="receiverResidential" value="{{config("constants.RESIDENT")}}" name="receiverAddressType" checked>
                                                        <label class="custom-control-label" for="receiverResidential">Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="receiverCommercial" name="receiverAddressType">
                                                        <label class="custom-control-label" for="receiverCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                                @error('receiverAddressType')
                                                    <small class="text-danger">{{$errors->first('receiverAddressType')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <h2 class="h3 mb-4">Packing Details</h2>
                                <div id="boxes" class="row">

                                </div>
                                <div>
                                    <input type="button" id="" class="btn btn-danger float-right d-block mr-3" value="Add Box" onclick="addBox()" />
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

            </form>

        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="{{ asset('assets/admin/js/jquery.steps.min.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/shipment/script.js') }}"></script>
<script src="{{ asset('assets/admin/js/shipment/validate.js') }}"></script>
<script>
    $('#destination').change(function() {
        // alert("hello");
        fillServices(this.value, '{{ route("ajax.serviceByCountry") }}', '{{ csrf_token() }}');
    });

    addBox();
</script>
@endsection
