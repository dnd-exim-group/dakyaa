@extends('layouts.admin')
@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.steps.css') }}"> -->
@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <form action="{{route('admin.shipment.update',$shipment)}}" method="POST" id="shipment-form" novalidate>
                @csrf
                @method("PUT")
                <div class="row my-4">
                    <div class="col">
                        <h1 class="h2 page-title">Shipment</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Add Shipment</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <h3 class="mb-3">Customer Details</h3>
                                    <section class="row">
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="customer">Select Customer</label>
                                                <select name="customer" class="form-control select2" id="customer">
                                                    <option value="--Select-Customer">--Select Customer--</option>
                                                <?php
                                                    foreach ($users as $user) {
                                                ?>
                                                        <option value="{{$user->id}}" {{($shipment->user_id == $user->id) ? 'selected':''}}>{{$user->firstname . " " . $user->lastname}}</option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('customer')
                                                    <small class="text-danger">{{$errors->first('customer')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="origin">Origin</label>
                                                <select name="origin" class="form-control select2" id="origin">
                                                    <option value="">Select Origin</option>
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{($shipment->senderAddress->country->id==$country->id) ? 'selected':''}}>{{$country->name}}</option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('origin')
                                                    <small class="text-danger">{{$errors->first('origin')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="originPincode">Origin Pincode</label>
                                                <input name="originPincode" type="text" class="form-control" id="originPincode" value="{{old('originPincode', $shipment->senderAddress->pincode)}}">
                                                @error('origin')
                                                    <small class="text-danger">{{$errors->first('origin')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="destination">Destination</label>
                                                <select name="destination" class="form-control select2" id="destination" onChange="fillServices(this.value)">
                                                    <option value="">Select Destination</option>
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{($shipment->receiverAddress->country->id==$country->id) ? 'selected':''}}>{{$country->name}}</option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('destination')
                                                    <small class="text-danger">{{$errors->first('destination')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="destinationPincode">Destination Pincode</label>
                                                <input name="destinationPincode" type="text" value="{{old('destinationPincode', $shipment->receiverAddress->pincode)}}" class="form-control" id="destinationPincode">
                                                @error('destinationPincode')
                                                    <small class="text-danger">{{$errors->first('destinationPincode')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="services">Services</label>
                                                <select name="service" class="form-control select2" id="courier_services">
                                                    <?php
                                                        foreach ($shipment->receiverAddress->country->services as $service) {
                                                    ?>
                                                        <option value="{{$service->id}}" {{$shipment->service->id == $service->id ? 'selected':''}}>{{$service->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                                @error('service')
                                                    <small class="text-danger">{{$errors->first('service')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label>Shipment Type</label>
                                                <div class="form-row">
                                                <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" id="shipmentType1" name="shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.PERSONAL_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType1">Personal/Gift</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" id="shipmentType2" name="shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.COMMERCIAL_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType2">Commercial</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" id="shipmentType3" name="shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.OTHERS_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType3">Others</label>
                                                        </div>
                                                    </div>
                                                    @error('shipmentType')
                                                        <small class="text-danger">{{$errors->first('shipmentType')}}</small>
                                                    @enderror
                                                </div>

                                            </div>
                                            <div class="mb-3">
                                                <label for="paymentStatus">Payment Status</label>
                                                <div class="custom-control custom-switch">
                                                    <input name="paymentStatus" type="checkbox" class="custom-control-input" id="customSwitch1" {{$shipment->payment_status==1? 'checked':''}}>
                                                    <label class="custom-control-label" for="customSwitch1">Paid</label>
                                                    @error('paymentStatus')
                                                        <small class="text-danger">{{$errors->first('paymentStatus')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Sender Details</h2>
                                        <div class="mb-3">
                                            <label for="senderFirstname">First Name</label>
                                            <input value="{{old('senderFirstname', $shipment->senderAddress->firstname)}}" name="senderFirstname" type="text" class="form-control" id="senderFirstname">
                                            @error('senderFirstname')
                                                <small class="text-danger">{{$errors->first('senderFirstname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderLastname">Last Name</label>
                                            <input value="{{old('senderLastname', $shipment->senderAddress->lastname)}}" name="senderLastname" type="text" class="form-control" id="senderLastname">
                                            @error('senderLastname')
                                                <small class="text-danger">{{$errors->first('senderLastname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderEmail">Email ID</label>
                                            <input value="{{old('senderEmail', $shipment->senderAddress->email)}}" name="senderEmail" type="email" class="form-control" id="senderEmail">
                                            @error('senderEmail')
                                                <small class="text-danger">{{$errors->first('senderEmail')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderPhoneNo">Phone Number</label>
                                            <div class="form-row">
                                                <div class="col-3">
                                                    <select class="form-control select2" id="simple-select2" name="senderCountryCode">
                                                        <?php
                                                            foreach ($countries as $country) {
                                                        ?>
                                                            <option value="{{$country->id}}" {{$country->id == $shipment->senderAddress->countryCode->id ? 'selected' : ''}}> {{$country->code.' '.$country->dial_code}} </option>
                                                        <?php
                                                            }
                                                        ?>
                                                        @error('senderCountryCode')
                                                            <small class="text-danger">{{$errors->first('senderCountryCode')}}</small>
                                                        @enderror
                                                    </select>
                                                </div>
                                                <div class="col-9">
                                                    <input type="text" name="senderPhone" class="form-control" id="senderPhone" value="{{old('senderPhone', $shipment->senderAddress->phone_no)}}">
                                                    @error('senderPhone')
                                                        <small class="text-danger">{{$errors->first('senderPhone')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddress1">Address Line 1</label>
                                            <input value="{{old('senderAddress1', $shipment->senderAddress->address_line1)}}" name="senderAddress1" type="text" class="form-control" id="senderAddress1">
                                            @error('senderAddress1')
                                                <small class="text-danger">{{$errors->first('senderAddress1')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddress2">Address Line 2</label>
                                            <input value="{{old('senderAddress2', $shipment->senderAddress->address_line2)}}" name="senderAddress2" type="text" class="form-control" id="senderAddress2">
                                            @error('senderAddress2')
                                                <small class="text-danger">{{$errors->first('senderAddress2')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCity">City</label>
                                            <input value="{{old('senderCity', $shipment->senderAddress->city)}}" name="senderCity" type="text" class="form-control" id="senderCity">
                                            @error('senderCity')
                                                <small class="text-danger">{{$errors->first('senderCity')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderPincode">Pincode</label>
                                            <input value="{{old('senderPincode', $shipment->senderAddress->pincode)}}" type="text" class="form-control" id="senderPincode" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderState">State</label>
                                            <input value="{{old('senderState', $shipment->senderAddress->state_country)}}" name="senderState" type="text" class="form-control" id="senderState">
                                            @error('senderState')
                                                <small class="text-danger">{{$errors->first('senderState')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCountry">Country</label>
                                            <input value="{{old('senderCountry', $shipment->senderAddress->country->name)}}" type="text" class="form-control" id="senderCountry" readonly>
                                        </div>
                                        <div>
                                            <label>Address Type</label>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-2">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.RESIDENT")}}" id="senderAddressType" name="senderAddressType" {{$shipment->senderAddress->address_type == config("constants.RESIDENT") ? 'checked':''}}>
                                                        <label class="custom-control-label" for="senderAddressType">Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="senderAddressTypeCommercial" name="senderAddressType" {{$shipment->senderAddress->address_type == config("constants.COMMERCIAL") ? 'checked':''}}>
                                                        <label class="custom-control-label" for="senderAddressTypeCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                                @error('senderAddressType')
                                                    <small class="text-danger">{{$errors->first('senderAddressType')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Receiver Details</h2>
                                        <div class="mb-3">
                                            <label for="receiverFirstname">First Name</label>
                                            <input value="{{old('receiverFirstname',$shipment->receiverAddress->firstname)}}" name="receiverFirstname" type="text" class="form-control" id="receiverFirstname">
                                            @error('receiverFirstname')
                                                <small class="text-danger">{{$errors->first('receiverFirstname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverLastname">Last Name</label>
                                            <input value="{{old('receiverLastname',$shipment->receiverAddress->lastname)}}" name="receiverLastname" type="text" class="form-control" id="receiverLastname">
                                            @error('receiverLastname')
                                                <small class="text-danger">{{$errors->first('receiverLastname')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverEmail">Email ID</label>
                                            <input value="{{old('receiverEmail', $shipment->receiverAddress->email)}}" name="receiverEmail" type="email" class="form-control" id="receiverEmail">
                                            @error('receiverEmail')
                                                <small class="text-danger">{{$errors->first('receiverEmail')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPhone">Phone Number</label>
                                            <div class="form-row">
                                                <div class="col-3">
                                                    <select class="form-control select2" id="simple-select2" name="receiverCountryCode">
                                                        <?php
                                                            foreach ($countries as $country) {
                                                        ?>
                                                            <option value="{{$country->id}}" {{$country->id == $shipment->receiverAddress->countryCode->id ? 'selected' : ''}}> {{$country->code.' '.$country->dial_code}} </option>
                                                        <?php
                                                            }
                                                        ?>
                                                        @error('receiverCountryCode')
                                                            <small class="text-danger">{{$errors->first('receiverCountryCode')}}</small>
                                                        @enderror
                                                    </select>
                                                </div>
                                                <div class="col-9">
                                                    <input type="text" name="receiverPhone" class="form-control" id="receiverPhone" value="{{old('receiverPhone',$shipment->receiverAddress->phone_no)}}">
                                                    @error('receiverPhone')
                                                        <small class="text-danger">{{$errors->first('receiverPhone')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress1">Address Line 1</label>
                                            <input value="{{old('receiverAddress1', $shipment->receiverAddress->address_line1)}}" name="receiverAddress1" type="text" class="form-control" id="receiverAddress1">
                                            @error('receiverAddress1')
                                                <small class="text-danger">{{$errors->first('receiverAddress1')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress2">Address Line 2</label>
                                            <input value="{{old('receiverAddress2', $shipment->receiverAddress->address_line2)}}" name="receiverAddress2" type="text" class="form-control" id="receiverAddress2">
                                            @error('receiverAddress2')
                                                <small class="text-danger">{{$errors->first('receiverAddress2')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCity">City</label>
                                            <input value="{{old('receiverCity', $shipment->receiverAddress->city)}}" name="receiverCity" type="text" class="form-control" id="receiverCity">
                                            @error('receiverCity')
                                                <small class="text-danger">{{$errors->first('receiverCity')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPincode">Pincode</label>
                                            <input value="{{old('receiverPincode', $shipment->receiverAddress->pincode)}}" name="receiverPincode" type="text" class="form-control" id="receiverPincode" readonly>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverState">State</label>
                                            <input value="{{old('receiverState', $shipment->receiverAddress->state_country)}}" name="receiverState" type="text" class="form-control" id="receiverState">
                                            @error('receiverCity')
                                                <small class="text-danger">{{$errors->first('receiverCity')}}</small>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCountry">Country</label>
                                            <input value="{{old('receiverCountry', $shipment->receiverAddress->country->name)}}" name="receiverCountry" type="text" class="form-control" id="receiverCountry" readonly>
                                        </div>
                                        <div>
                                            <label>Address Type</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" id="receiverResidential" value="{{config("constants.RESIDENT")}}" name="receiverAddressType" {{$shipment->receiverAddress->address_type == config("constants.RESIDENT") ? 'checked':''}}>
                                                        <label class="custom-control-label" for="receiverResidential">Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="receiverCommercial" name="receiverAddressType" {{$shipment->receiverAddress->address_type == config("constants.COMMERCIAL") ? 'checked':''}}>
                                                        <label class="custom-control-label" for="receiverCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                                @error('receiverAddressType')
                                                    <small class="text-danger">{{$errors->first('receiverAddressType')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <h2 class="h3 mb-4">Packing Details</h2>
                                <div id="boxes" class="row">
                                <?php
                                    $i = 1;
                                    foreach ($shipment->boxes as $box) {
                                ?>
                                    <div id="box_1" class="col-md-12">
                                        <input type="hidden"  name="box_no[]" value="{{$i}}">
                                        <h4 class="mr-3 d-inline">Box {{$i}}</h4>
                                        @if($i != 1)
                                            <button class="btn btn-sm btn-danger d-inline mb-2" id="box_btn_{{$i}}" onclick="removeBox('box_btn_{{$i}}')"><i class="fe fe-trash-2"></i></button>
                                        @endif
                                        <div class="row mt-3 mb-3">
                                            <div class="col-md-6 col-sm-12">
                                                <div>
                                                    <label>Dimensions (Length x Width x Height)</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Length</span>
                                                        </div>
                                                        <input name="length[]" value="{{$box->length}}" type="number" class="calculate form-control" id="length_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Height</span>
                                                        </div>
                                                        <input name="height[]" value="{{$box->height}}" type="number" class="calculate form-control" id="height_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Width</span>
                                                        </div>
                                                        <input name="width[]" value="{{$box->width}}" type="number" class="calculate form-control" id="width_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="mb-2">
                                                    <label for="volumeWeight_{{$i}}">Volume Weight</label>
                                                    <input name="" value="{{($box->length * $box->width * $box->height) / 5000}}" type="number" class="form-control" id="volumeWeight_{{$i}}" readonly>
                                                </div>
                                                <div class="">
                                                    <label for="weight_{{$i}}">Actual Weight</label>
                                                    <input name="weight[]" value="{{$box->weight}}" type="number" class="form-control" id="weight_{{$i}}" min="0.1" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <!-- table -->
                                                <table id="itemTable_{{$i}}" class="table table-bordered text-center">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th>Item</th>
                                                            <th>Quantity</th>
                                                            <th>Value</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="itemTableBody_{{$i}}">
                                                        <?php
                                                            $j = 1;
                                                            foreach ($box->items as $item) {
                                                        ?>
                                                            <tr>
                                                                <td><input type="text" class="form-control" name="itemName_{{$i}}[]" value="{{$item->name}}" required></td>
                                                                <td><input class="form-control" type="number" min="1" name="itemQuantity_{{$i}}[]" value="{{$item->count}}" required></td>
                                                                <td><input class="form-control" type="number" min="1" name="value_{{$i}}[]" value="{{$item->value}}" required></td>
                                                                <td>
                                                                    <button id="button_{{$i}}_{{$j}}" name="button_{{$i}}_{{$j}}" class="btn btn-danger" onclick="removeRow('button_{{$i}}_{{$j}}')">
                                                                        <i class="fe fe-trash-2"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                            $j++;
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <input type="button" class="btn btn-primary mb-4 float-right" value="Add New Item" onclick="addRow('itemTableBody_{{$i}}', {{$i}})" />
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                        $i++;
                                    }
                                ?>
                                </div>
                                <div>
                                    <input type="button" id="" class="btn btn-danger float-right d-block mr-3" value="Add Box" onclick="addBox()" />
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

            </form>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="{{ asset('assets/admin/js/jquery.steps.min.js') }}"></script> -->
<script src="{{ asset('assets/admin/js/shipment/validate.js') }}"></script>
<script src="{{ asset('assets/admin/js/shipment/script.js') }}"></script>

<script>
    $('#destination').change(function() {
        fillServices(this.value, '{{ route("ajax.serviceByCountry") }}', '{{ csrf_token() }}');
    });
</script>
@endsection
