@extends('layouts.admin')
@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/global/css/jquery.timepicker.css') }}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title">Shipments</h1>
            {{-- <div class="row mt-4">
                <div class="col">
                    <form action="{{ route("shipments.export") }}" method="GET" id="shipment-excel-form" class="float-right">
                        @csrf
                        @method("GET")
                        <div class="input-group input-daterange">
                            <div class="form-group">
                                <label for="reportrange" style="display: block">Shipments Excel</label>
                                <div id="reportrange" class="border px-2 py-2 bg-light d-inline-flex" style="width: 450px">
                                    <i class="fe fe-calendar fe-16 mx-2"></i>
                                    <span id="date_range">July 1, 2021 - July 31, 2021</span>
                                </div>
                                <button type="submit" name="filter" id="filter" class="btn btn-info ml-3">
                                    <i class="fe fe-download fe-16 mr-1"></i>
                                    Download
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            <div class="row mt-2">
                <ul class="col-12 nav nav-tabs mx-3 mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="all-tab" data-toggle="tab" href="#allShipments" role="tab" aria-controls="home" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pending-tab" data-toggle="tab" href="#pending" role="tab" aria-controls="pending" aria-selected="false">Pending</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="approved-tab" data-toggle="tab" href="#approved" role="tab" aria-controls="approved" aria-selected="false">Approved</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="rejected-tab" data-toggle="tab" href="#rejected" role="tab" aria-controls="rejected" aria-selected="false">Rejected</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">

                    <div class="tab-pane fade show active" id="allShipments" role="tabpanel" aria-labelledby="all-tab">
                        <div class="row">
                            <div class="col">
                                <form action="{{ route("shipments.export") }}" method="GET" id="shipment-excel-form" class="mx-3">
                                    @csrf
                                    @method("GET")
                                    <div class="input-group input-daterange">
                                        <div class="form-group">
                                            <label for="reportrange" style="display: block">All Shipments</label>
                                            <div id="reportrange" class="border px-2 py-2 bg-light d-inline-flex" style="width: 450px">
                                                <i class="fe fe-calendar fe-16 mx-2"></i>
                                                <span id="date_range">July 1, 2021 - July 31, 2021</span>
                                            </div>
                                            <button type="submit" name="filter" id="filter" class="btn btn-info ml-3">
                                                <i class="fe fe-download fe-16 mr-1"></i>
                                                Download
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="shipmentDataTable">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Sender Name</th>
                                                <th>Receiver Name</th>
                                                <th>Vendor</th>
                                                <th>Service</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Booking Date</th>
                                                <th>Tracking</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php
                                    foreach ($shipments as $shipment) {
                                ?>
                                        <tr>
                                            <td>{{$shipment->user->firstname}} {{$shipment->user->lastname}}</td>
                                            <td>{{$shipment->senderAddress->firstname}} {{$shipment->senderAddress->lastname}}</td>
                                            <td>{{$shipment->receiverAddress->firstname}} {{$shipment->receiverAddress->lastname}}</td>
                                            <td>{{$shipment->service->vendor->name}}</td>
                                            <td>{{$shipment->service->name}}</td>
                                            <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                            <td>{{$shipment->receiverAddress->country->name}} - {{$shipment->receiverAddress->pincode}}</td>
                                            <td>{{$shipment->created_at}}</td>
                                            <td>
                                                <?php
                                                    if( $shipment->tracking()->count() == 0){
                                                ?>
                                                        <a href="{{route('admin.tracking.create',$shipment)}}" class="btn btn-primary btn-sm" type="button">
                                                            Add AWB
                                                        </a>
                                                <?php
                                                    } else {
                                                ?>
                                                        <a href="{{route('admin.tracking.show',$shipment->tracking)}}" class="btn btn-primary btn-sm" type="button">
                                                            Track
                                                        </a>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="text-muted sr-only">Action</span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <form method="POST" action="{{route('admin.shipment.pending',$shipment)}}">
                                                        @csrf
                                                        @method('POST')
                                                        <button class="dropdown-item" type="submit"><i class="fe fe-alert-triangle mr-1 text-warning"></i> Pending</button>
                                                    </form>
                                                    <form method="POST" action="{{route('admin.shipment.approved',$shipment)}}">
                                                        @csrf
                                                        @method('POST')
                                                        <button class="dropdown-item" type="submit"><i class="fe fe-check-circle mr-1 text-success"></i> Approve</button>
                                                    </form>
                                                    <form method="POST" action="{{route('admin.shipment.rejected',$shipment)}}">
                                                        @csrf
                                                        @method('POST')
                                                        <button class="dropdown-item" type="submit"><i class="fe fe-x-circle mr-1 text-danger"></i> Reject</button>
                                                    </form>
                                                </div>
                                                <strong>
                                                    <?php
                                                        if($shipment->status == config("constants.SHIPMENT_PENDING")){
                                                            echo "<span class='text-warning'>Pending</span>";
                                                        }else if($shipment->status == config("constants.SHIPMENT_APPROVED")){
                                                            echo "<span class='text-success'>Approved</span>";
                                                        }else if($shipment->status == config("constants.SHIPMENT_REJECTED")){
                                                            echo "<span class='text-danger'>Rejected</span>";
                                                        }
                                                    ?>
                                                </strong>
                                            </td>
                                            <td>
                                                <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="text-muted sr-only">Action</span>
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{route('admin.shipment.edit',$shipment)}}"><i class="fe fe-edit mr-1 text-warning"></i> Edit</a>
                                                    <a class="dropdown-item" href="{{route('admin.shipment.show',$shipment)}}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                    <form method="POST" action="{{route('admin.shipment.inactive',$shipment)}}">
                                                        @csrf
                                                        @method('PUT')
                                                        <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Trash</button>
                                                    </form>
                                                </div>
                                            </td>

                                        </tr>
                                <?php
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="pending" role="tabpanel" aria-labelledby="pending-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="pendingShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Sender Name</th>
                                                <th>Receiver Name</th>
                                                <th>Vendor</th>
                                                <th>Service</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Booking Date</th>
                                                <th>Tracking</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php
                                    foreach ($shipments as $shipment) {
                                        if($shipment->status == config("constants.SHIPMENT_PENDING")){
                                ?>
                                                <tr>
                                                    <td>{{$shipment->user->firstname}}</td>
                                                    <td>{{$shipment->senderAddress->firstname}}</td>
                                                    <td>{{$shipment->receiverAddress->firstname}}</td>
                                                    <td>{{$shipment->service->vendor->name}}</td>
                                                    <td>Economy</td>
                                                    <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                    <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                    <td>{{$shipment->created_at}}</td>
                                                    <td>
                                                        <?php
                                                            if( $shipment->tracking()->count() == 0){
                                                        ?>
                                                                <a href="{{route('admin.tracking.create',$shipment)}}" class="btn btn-primary btn-sm" type="button">
                                                                    Add AWB
                                                                </a>
                                                        <?php
                                                            } else {
                                                        ?>
                                                                <a href="{{route('admin.tracking.show',$shipment->tracking)}}" class="btn btn-primary btn-sm" type="button">
                                                                    Track
                                                                </a>
                                                        <?php
                                                            }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <form method="POST" action="{{route('admin.shipment.approved',$shipment)}}">
                                                                @csrf
                                                                @method('POST')
                                                                <button class="dropdown-item" type="submit"><i class="fe fe-check-circle mr-1 text-success"></i> Approve</button>
                                                            </form>
                                                            <form method="POST" action="{{route('admin.shipment.rejected',$shipment)}}">
                                                                @csrf
                                                                @method('POST')
                                                                <button class="dropdown-item" type="submit"><i class="fe fe-x-circle mr-1 text-danger"></i> Reject</button>
                                                            </form>
                                                        </div>
                                                        <strong><span class="text-warning">Pending</span></strong>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{route('admin.shipment.edit',$shipment)}}"><i class="fe fe-edit mr-1 text-warning"></i> Edit</a>
                                                            <a class="dropdown-item" href="{{route('admin.shipment.show',$shipment)}}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                            <form method="POST" action="{{route('admin.shipment.inactive',$shipment)}}">
                                                                @csrf
                                                                @method('PUT')
                                                                <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Trash</button>
                                                            </form>
                                                        </div>
                                                    </td>
                                                </tr>
                                <?php
                                        }
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="approved" role="tabpanel" aria-labelledby="approved-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="approvedShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Sender Name</th>
                                                <th>Receiver Name</th>
                                                <th>Vendor</th>
                                                <th>Service</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Booking Date</th>
                                                <th>Tracking</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php
                                    foreach ($shipments as $shipment) {
                                        if($shipment->status == config("constants.SHIPMENT_APPROVED")){
                                ?>
                                            <tr>
                                                <td>{{$shipment->user->firstname}}</td>
                                                <td>{{$shipment->senderAddress->firstname}}</td>
                                                <td>{{$shipment->receiverAddress->firstname}}</td>
                                                <td>{{$shipment->service->vendor->name}}</td>
                                                <td>Economy</td>
                                                <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                <td>{{$shipment->created_at}}</td>
                                                <td>
                                                    <?php
                                                        if( $shipment->tracking()->count() == 0){
                                                    ?>
                                                            <a href="{{route('admin.tracking.create',$shipment)}}" class="btn btn-primary btn-sm" type="button">
                                                                Add AWB
                                                            </a>
                                                    <?php
                                                        } else {
                                                    ?>
                                                            <a href="{{route('admin.tracking.show',$shipment->tracking)}}" class="btn btn-primary btn-sm" type="button">
                                                                Track
                                                            </a>
                                                    <?php
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="text-muted sr-only">Action</span>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <form method="POST" action="{{route('admin.shipment.pending',$shipment)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-alert-triangle mr-1 text-warning"></i> Pending</button>
                                                        </form>
                                                        <form method="POST" action="{{route('admin.shipment.rejected',$shipment)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-x-circle mr-1 text-danger"></i> Reject</button>
                                                        </form>
                                                    </div>
                                                    <strong><span class="text-success">Approved</span></strong>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="text-muted sr-only">Action</span>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="{{route('admin.shipment.edit',$shipment)}}"><i class="fe fe-edit mr-1 text-warning"></i> Edit</a>
                                                        <a class="dropdown-item" href="{{route('admin.shipment.show',$shipment)}}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        <form method="POST" action="{{route('admin.shipment.inactive',$shipment)}}">
                                                            @csrf
                                                            @method('PUT')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Trash</button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                <?php
                                        }
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="rejected" role="tabpanel" aria-labelledby="rejected-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="rejectedShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Sender Name</th>
                                                <th>Receiver Name</th>
                                                <th>Vendor</th>
                                                <th>Service</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Booking Date</th>
                                                <th>Tracking</th>
                                                <th>Status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                <?php
                                    foreach ($shipments as $shipment) {
                                        if($shipment->status == config("constants.SHIPMENT_REJECTED")){
                                ?>
                                            <tr>
                                                <td>{{$shipment->user->firstname}}</td>
                                                <td>{{$shipment->senderAddress->firstname}}</td>
                                                <td>{{$shipment->receiverAddress->firstname}}</td>
                                                <td>{{$shipment->service->vendor->name}}</td>
                                                <td>Economy</td>
                                                <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                                <td>{{$shipment->created_at}}</td>
                                                <td>
                                                    <?php
                                                        if( $shipment->tracking()->count() == 0){
                                                    ?>
                                                            <a href="{{route('admin.tracking.create',$shipment)}}" class="btn btn-primary btn-sm" type="button">
                                                                Add AWB
                                                            </a>
                                                    <?php
                                                        } else {
                                                    ?>
                                                            <a href="{{route('admin.tracking.show',$shipment->tracking)}}" class="btn btn-primary btn-sm" type="button">
                                                                Track
                                                            </a>
                                                    <?php
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="text-muted sr-only">Action</span>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <form method="POST" action="{{route('admin.shipment.pending',$shipment)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-alert-triangle mr-1 text-warning"></i> Pending</button>
                                                        </form>

                                                        <form method="POST" action="{{route('admin.shipment.approved',$shipment)}}">
                                                            @csrf
                                                            @method('POST')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-check-circle mr-1 text-success"></i> Approve</button>
                                                        </form>
                                                    </div>
                                                    <strong><span class="text-danger">Rejected</span></strong>
                                                </td>
                                                <td>
                                                    <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="text-muted sr-only">Action</span>
                                                    </button>
                                                    <div class="dropdown-menu dropdown-menu-right">
                                                        <a class="dropdown-item" href="{{route('admin.shipment.edit',$shipment)}}"><i class="fe fe-edit mr-1 text-warning"></i> Edit</a>
                                                        <a class="dropdown-item" href="{{route('admin.shipment.show',$shipment)}}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        <form method="POST" action="{{route('admin.shipment.inactive',$shipment)}}">
                                                            @csrf
                                                            @method('PUT')
                                                            <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Trash</button>
                                                        </form>
                                                    </div>
                                                </td>
                                            </tr>
                                <?php
                                        }
                                    }
                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                </div>

            </div> <!-- end section -->

        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/global/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/global/js/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/global/js/jquery.timepicker.js') }}"></script>

<script>
    $('#shipmentDataTable, #pendingShipments, #approvedShipments, #rejectedShipments').DataTable(
    {
        autoWidth: true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ]
    });

    // if ($('.datetimes').length)
    // {
    //     $('.datetimes').daterangepicker(
    //     {
    //         timePicker: true,
    //         startDate: moment().startOf('hour'),
    //         endDate: moment().startOf('hour').add(32, 'hour'),
    //         locale:
    //         {
    //             format: 'M/DD hh:mm A'
    //         }
    //     });
    // }

    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end)
    {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    $('#reportrange').daterangepicker(
    {
        startDate: start,
        endDate: end,
        ranges:
        {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
    cb(start, end);



    $("#shipment-excel-form").submit(function(){
        var dates = $('#date_range').text().split('-');

        var oldFromDate = new Date(dates[0]);
        var convertedFromDate = convertToDateTime(oldFromDate);
        var oldToDate = new Date(dates[1]);
        var convertedToDate = convertToDateTime(oldToDate);

        $("#shipment-excel-form").append(
            `<input type="text" name="from_date" id="from_date" value="${convertedFromDate} 00:00:00" hidden/>
            <input type="text" name="to_date" id="to_date" value="${convertedToDate} 23:59:59" hidden/>
        `);
        return true;
    });

    function convertToDateTime(str) {
        var mnths = {
            Jan: "01",
            Feb: "02",
            Mar: "03",
            Apr: "04",
            May: "05",
            Jun: "06",
            Jul: "07",
            Aug: "08",
            Sep: "09",
            Oct: "10",
            Nov: "11",
            Dec: "12"
        },
        str = str+'';
        date = str.split(" ");
        return [date[3], mnths[date[1]], date[2]].join("-");
    }

</script>

@endsection
