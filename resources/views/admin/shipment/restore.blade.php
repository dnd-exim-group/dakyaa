@extends('layouts.admin')

@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title">Restore Shipments</h1>
            <div class="row my-4">
                <div class="col-md-12">
                    <div class="card shadow">
                        <div class="card-body">
                            <!-- table -->
                            <table class="table datatables" id="shipmentDataTable">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Sender Name</th>
                                        <th>Receiver Name</th>
                                        <th>Vendor</th>
                                        <th>Service</th>
                                        <th>Origin</th>
                                        <th>Destination</th>
                                        <th>Booking Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    foreach ($shipments as $shipment) {
                                ?>
                                    <tr>
                                        <td>{{$shipment->user->firstname}} {{$shipment->user->lastname}}</td>
                                        <td>{{$shipment->senderAddress->firstname}} {{$shipment->senderAddress->lastname}}</td>
                                        <td>{{$shipment->receiverAddress->firstname}} {{$shipment->receiverAddress->lastname}}</td>
                                        <td>{{$shipment->service->vendor->name}}</td>
                                        <td>{{$shipment->service->name}}</td>
                                        <td>{{$shipment->senderAddress->country->name}} - {{$shipment->senderAddress->pincode}}</td>
                                        <td>{{$shipment->receiverAddress->country->name}} - {{$shipment->receiverAddress->pincode}}</td>
                                        <td>{{$shipment->created_at}}</td>
                                        <td>
                                            <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="text-muted sr-only">Action</span>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <form action="{{route('admin.shipment.restore', $shipment)}}" method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <button class="dropdown-item" type="submit"><i class="fe fe-refresh-cw mr-1 text-primary"></i> Restore</button>
                                                </form>
                                                <form action="{{route('admin.shipment.delete', $shipment)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="dropdown-item" type="submit"><i class="fe fe-trash-2 mr-1 text-danger"></i> Permanently Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> <!-- simple table -->
            </div> <!-- end section -->

        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#shipmentDataTable').DataTable(
    {
        autoWidth: true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ]
    });
</script>

@endsection
