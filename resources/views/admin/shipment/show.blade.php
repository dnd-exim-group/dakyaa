@extends('layouts.admin')
@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('assets/admin/css/jquery.steps.css') }}"> -->

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
                <div class="row my-4">
                    <div class="col">
                        <h1 class="h2 page-title">Shipment</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Add Shipment</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <h3 class="mb-3">Customer Details</h3>
                                    <section class="row">
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="customer">Select Customer</label>
                                                <select name="customer" class="form-control select2" id="customer" disabled>
                                            <?php
                                                foreach ($users as $user) {
                                            ?>
                                                    <option value="{{$user->id}}" {{($shipment->user_id == $user->id) ? 'selected':''}}>{{$user->firstname . " " . $user->lastname}}</option>
                                            <?php
                                                }
                                            ?>
                                                </select>
                                                <div class="invalid-feedback"> Please select a valid country. </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="services">Origin</label>
                                                <select name="origin" class="form-control select2" id="origin" disabled>
                                                    <option value="">Select Origin</option>
                                            <?php
                                                foreach ($countries as $country) {
                                            ?>
                                                    <option value="{{$country->id}}" {{($shipment->senderAddress->country->id==$country->id) ? 'selected':''}}>{{$country->name}}</option>
                                            <?php
                                                }
                                            ?>
                                                </select>
                                                <div class="invalid-feedback"> Please select a valid country. </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="originPincode">Origin Pincode</label>
                                                <input name="originPincode" type="text" class="form-control" id="originPincode" value="{{$shipment->senderAddress->pincode}}" readonly>
                                                <div class="valid-feedback"> Looks good! </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="services">Destination</label>
                                                <select name="destination" class="form-control select2" id="destination" onChange="fillServices(this.value)" disabled>
                                                    <option value="">Select Destination</option>
                                            <?php
                                                foreach ($countries as $country) {
                                            ?>
                                                    <option value="{{$country->id}}" {{($shipment->receiverAddress->country->id==$country->id) ? 'selected':''}}>{{$country->name}}</option>
                                            <?php
                                                }
                                            ?>
                                                </select>
                                                <div class="invalid-feedback"> Please select a valid country. </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="pincodeDes">Destination Pincode</label>
                                                <input name="destinationPincode" type="text" value="{{$shipment->receiverAddress->pincode}}" class="form-control" id="destinationPincode" readonly>
                                                <div class="valid-feedback"> Looks good! </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="approxWeight">Approx Weight</label>
                                                <input name="weight" type="number" class="form-control" id="approxWeight" min="0.5" step="0.5" value="{{$shipment->boxes[0]->weight}}" readonly>
                                                <div class="valid-feedback"> Looks good! </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="services">Services</label>
                                                <select name="service" class="form-control select2" id="courier_services" disabled>
                                                    <?php
                                                        foreach ($shipment->receiverAddress->country->services as $service) {
                                                    ?>
                                                        <option value="{{$service->id}}" {{$shipment->service->id == $service->id ? 'selected':''}}>{{$service->name}}</option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                                <div class="invalid-feedback"> Please select a valid service. </div>
                                            </div>
                                            <div class="mb-3">
                                                <label>Shipment Type</label>
                                                <div class="form-row">
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" id="shipmentType1" name="shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.PERSONAL_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType1">Personal/Gift</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" id="shipmentType2" name="shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.COMMERCIAL_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType2">Commercial</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" id="shipmentType3" name="shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}" {{$shipment->shipment_type == config('constants.OTHERS_SHIPMENT') ? 'checked' : ''}}>
                                                            <label class="custom-control-label" for="shipmentType3">Others</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="approxWeight">Payment Status</label>
                                                <div class="custom-control custom-switch">
                                                    <input name="paymentStatus" type="checkbox" class="custom-control-input" id="customSwitch1" {{$shipment->payment_status==1? 'checked':''}} disabled>
                                                    <label class="custom-control-label" for="customSwitch1">Paid</label>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Sender Details</h2>
                                        <div class="mb-3">
                                            <label for="senderFirstname">First Name</label>
                                            <input value="{{$shipment->senderAddress->firstname}}" name="senderFirstname" type="text" class="form-control" id="senderFirstname" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderLastname">Last Name</label>
                                            <input value="{{$shipment->senderAddress->lastname}}" name="senderLastname" type="text" class="form-control" id="senderLastname" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderEmail">Email ID</label>
                                            <input value="{{$shipment->senderAddress->email}}" name="senderEmail" type="email" class="form-control" id="senderEmail" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderPhoneNo">Phone Number</label>
                                            <input value="{{$shipment->senderAddress->phone_no}}" name="senderPhone" type="text" class="form-control" id="senderPhoneNo" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddressLine1">Address Line 1</label>
                                            <input value="{{$shipment->senderAddress->address_line1}}" name="senderAddress1" type="text" class="form-control" id="senderAddressLine1" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderAddressLine2">Address Line 2</label>
                                            <input value="{{$shipment->senderAddress->address_line2}}" name="senderAddress2" type="text" class="form-control" id="senderAddressLine2" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCity">City</label>
                                            <input value="{{$shipment->senderAddress->city}}" name="senderCity" type="text" class="form-control" id="senderCity" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderPincode">Pincode</label>
                                            <input value="{{$shipment->senderAddress->pincode}}" type="text" class="form-control" id="senderPincode" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderState">State</label>
                                            <input value="{{$shipment->senderAddress->state_country}}" name="senderState" type="text" class="form-control" id="senderState" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="senderCountry">Country</label>
                                            <input value="{{$shipment->senderAddress->country->name}}" type="text" class="form-control" id="senderCountry" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div>
                                            <label>Address Type</label>
                                            <div class="form-row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.RESIDENT")}}" id="senderAddressType" name="senderAddressType" {{$shipment->senderAddress->address_type == config("constants.RESIDENT") ? 'checked':''}} disabled>
                                                        <label class="custom-control-label" for="senderAddressType" disabled>Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="senderAddressTypeCommercial" name="senderAddressType" {{$shipment->senderAddress->address_type == config("constants.COMMERCIAL") ? 'checked':''}} disabled>
                                                        <label class="custom-control-label" for="senderAddressTypeCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Receiver Details</h2>
                                        <div class="mb-3">
                                            <label for="receiverFirstname">First Name</label>
                                            <input value="{{$shipment->receiverAddress->firstname}}" name="receiverFirstname" type="text" class="form-control" id="receiverFirstname" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverLastname">Last Name</label>
                                            <input value="{{$shipment->receiverAddress->lastname}}" name="receiverLastname" type="text" class="form-control" id="receiverLastname" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverEmail">Email ID</label>
                                            <input value="{{$shipment->receiverAddress->email}}" name="receiverEmail" type="email" class="form-control" id="receiverEmail" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPhone">Phone Number</label>
                                            <input value="{{$shipment->receiverAddress->phone_no}}" name="receiverPhone" type="text" class="form-control" id="receiverPhone" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress1">Address Line 1</label>
                                            <input value="{{$shipment->receiverAddress->address_line1}}" name="receiverAddress1" type="text" class="form-control" id="receiverAddress1" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverAddress2">Address Line 2</label>
                                            <input value="{{$shipment->receiverAddress->address_line2}}" name="receiverAddress2" type="text" class="form-control" id="receiverAddress2" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCity">City</label>
                                            <input value="{{$shipment->receiverAddress->city}}" name="receiverCity" type="text" class="form-control" id="receiverCity" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverPincode">Pincode</label>
                                            <input value="{{$shipment->receiverAddress->pincode}}" name="receiverPincode" type="text" class="form-control" id="receiverPincode" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverState">State</label>
                                            <input value="{{$shipment->receiverAddress->state_country}}" name="receiverState" type="text" class="form-control" id="receiverState" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div class="mb-3">
                                            <label for="receiverCountry">Country</label>
                                            <input value="{{$shipment->receiverAddress->country->name}}" name="receiverCountry" type="text" class="form-control" id="receiverCountry" readonly>
                                            <div class="valid-feedback"> Looks good! </div>
                                        </div>
                                        <div>
                                            <label>Address Type</label>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="receiverResidential" value="{{config("constants.RESIDENT")}}" name="receiverAddressType" {{$shipment->receiverAddress->address_type == config("constants.RESIDENT") ? 'checked':''}} disabled>
                                                        <label class="custom-control-label" for="receiverResidential">Residential</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="receiverCommercial" name="receiverAddressType" {{$shipment->receiverAddress->address_type == config("constants.COMMERCIAL") ? 'checked':''}} disabled>
                                                        <label class="custom-control-label" for="receiverCommercial">Commercial</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <h2 class="h3 mb-4">Packing Details</h2>
                                <div id="boxes" class="row">
                                <?php
                                    $i = 1;
                                    foreach ($shipment->boxes as $box) {
                                ?>
                                    <div id="box_1" class="col-md-12">
                                        <input type="hidden"  name="box_no[]" value="{{$i}}">
                                        <h4 class="mr-3 d-inline">Box {{$i}}</h4>
                                        <div class="row mt-3 mb-3">
                                            <div class="col">
                                                <label>Dimensions (Length x Width x Height)</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Length</span>
                                                    </div>
                                                    <input name="length[]" value="{{$box->length}}" type="number" class="calculate form-control" id="length_{{$i}}" min="0" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Height</span>
                                                    </div>
                                                    <input name="height[]" value="{{$box->height}}" type="number" class="calculate form-control" id="height_{{$i}}" min="0" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Width</span>
                                                    </div>
                                                    <input name="width[]" value="{{$box->width}}" type="number" class="calculate form-control" id="width_{{$i}}" min="0" readonly>
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">cm</span>
                                                    </div>
                                                </div>

                                                <div class="valid-feedback"> Looks good! </div>
                                            </div>
                                            <div class="col">
                                                <div class="mb-2">
                                                    <label for="weight_{{$i}}">Actual Weight</label>
                                                    <input name="weight[]" value="{{$box->weight}}" type="number" class="form-control" id="weight_{{$i}}" min="0.5" step="0.5" readonly>
                                                    <div class="valid-feedback"> Looks good! </div>
                                                </div>

                                                <div class="">
                                                    <label for="volumeWeight_{{$i}}">Volume Weight</label>
                                                    <input name="" value="{{($box->length * $box->width * $box->height) / 5000}}" type="number" class="form-control" id="volumeWeight_{{$i}}" readonly>
                                                    <div class="valid-feedback"> Looks good! </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <!-- table -->
                                                <table id="itemTable_{{$i}}" class="table table-bordered text-center">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th>Item</th>
                                                            <th>Quantity</th>
                                                            <th>Value</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="itemTableBody_{{$i}}">
                                                        <?php
                                                            $j = 1;
                                                            foreach ($box->items as $item) {
                                                        ?>
                                                            <tr>
                                                                <td><input type="text" class="form-control" name="itemName_{{$i}}[]" value="{{$item->name}}" readonly></td>
                                                                <td><input class="form-control" type="number" min="1" name="itemQuantity_{{$i}}[]" value="{{$item->count}}" readonly></td>
                                                                <td><input class="form-control" type="number" min="1" name="value_{{$i}}[]" value="{{$item->value}}" readonly></td>
                                                            </tr>
                                                        <?php
                                                            $j++;
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                        $i++;
                                    }
                                ?>
                                </div>
                                <a class="btn btn-primary" href="{{route('admin.shipments')}}">back</a>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>



        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="{{ asset('assets/admin/js/jquery.steps.min.js') }}"></script> -->
<script>
    function fillServices(country_id){
        let route = "{{ route('ajax.serviceByCountry') }}";

        $.ajax({
            url: route,
            method: "POST",
            data:{
                _token: "{{ csrf_token() }}",
                'country': country_id,
            },
            dataType: 'json',
            success: function(success){
                $("#courier_services").empty();
                success.forEach(element => {
                    $("#courier_services").append(`<option value="${element.id}">${element.name}</option>`);
                });
            }
        });
    }

    $(".calculate").focusout(function(){
        var length = $.trim($("#length").val());
        var height = $.trim($("#height").val());
        var width = $.trim($("#width").val());

        var mul = (length * width * height)/5000;
        if(mul != 0){
            $("#volumeWeight").val(mul);
        }
    });

    $("#origin").focusout(function(){
        $("#senderCountry").val($("#origin").children("option").filter(":selected").text());
    });
    $("#destination").focusout(function(){
        $("#receiverCountry").val($("#destination").children("option").filter(":selected").text());
    });

    $("#originPincode").keyup(function(){
        $("#senderPincode").val($("#originPincode").val());
    });
    $("#destinationPincode").keyup(function(){
        $("#receiverPincode").val($("#destinationPincode").val());
    });
    $("#customerName").keyup(function(){
        $("#senderName").val($("#customerName").val());
    });


    function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        //Column 1
        var cell1 = row.insertCell(0);
        var element1 = document.createElement("input");
        element1.type = "text";
        element1.classList.add("form-control");
        element1.setAttribute("name","itemName[]");
        cell1.appendChild(element1);

        //Column 2
        var cell2 = row.insertCell(1);
        var element2 = document.createElement("input");
        element2.classList.add("form-control");
        element2.type = "number";
        element2.min = "1";
        element2.setAttribute("name","itemQuantity[]");
        cell2.appendChild(element2);

        //Column 3
        var cell3 = row.insertCell(2);
        var element3 = document.createElement("input");
        element3.classList.add("form-control");
        element3.type = "number";
        element3.min = "1";
        element3.setAttribute("name","value[]");
        cell3.appendChild(element3);

        //Column 4
        var cell4 = row.insertCell(3);
        var element4 = document.createElement("button");
        // element4.type = "button";
        var btnName = "button" + (rowCount + 1);
        element4.name = btnName;
        element4.classList.add("btn", "btn-danger");
        // element4.textContent  = 'Delete'; // or element4.value = "button";
        element4.innerHTML = '<i class="fe fe-trash-2"></i>';
        element4.onclick = function() {
            removeRow(btnName);
        }
        cell4.appendChild(element4);
    }

    function removeRow(btnName) {
        try {
            var table = document.getElementById('itemTable');
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                var row = table.rows[i];
                var rowObj = row.cells[3].childNodes[0];
                console.log(row.cells[3]);
                console.log(btnName);
                if (rowObj.name == btnName) {
                    table.deleteRow(i);
                    rowCount--;
                }
            }
        } catch (e) {
            alert(e);
        }
    }



</script>

@endsection
