@extends('layouts.admin')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Tracking</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Tracking</strong>
                </div>
                <div class="card-body">
                    <?php
                        // Function to generate a random string of specified length
                        function generateRandomString($length) {
                            $characters = '0123456789';
                            $randomString = '';
                            for ($i = 0; $i < $length; $i++) {
                                $randomString .= $characters[rand(0, strlen($characters) - 1)];
                            }
                            return $randomString;
                        }

                        // Function to generate unique random digits
                        function generateUniqueRandomDigits($length, $existingDigits) {
                            $randomDigits = generateRandomString($length);
                            while (in_array($randomDigits, $existingDigits)) {
                                $randomDigits = generateRandomString($length);
                            }
                            return $randomDigits;
                        }

                        // Example phone number (last 3 digits)
                        // $lastThreeDigits = substr($shipment->user->phone_no, -3);
                        $randomDigits = generateUniqueRandomDigits(7, $trackingNumbers);

                        // Generate 4 random digits
                        // $startRandomDigits = generateUniqueRandomDigits(3, $trackingNumbers);
                        // $endRandomDigits = generateUniqueRandomDigits(3, $trackingNumbers);

                        // Construct the tracking number
                        $trackingNumber = 'DND' . $randomDigits;

                        // Output the tracking number
                        // echo "Tracking number: $trackingNumber";
                    ?>
                    <form id="add-tracking" class="tracking" action="{{route('admin.tracking.store',$shipment->id)}}" method="POST">
                        @csrf
                        @method("POST")
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">Customer Name</label>
                                <input type="text" class="form-control" id="firstname" value="{{$shipment->user->firstname . " " . $shipment->user->lastname}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="tracking_number">Tracking Number</label>
                                <input name="tracking_number" type="text" class="form-control" id="tracking_number" value="{{ $trackingNumber, old('tracking_number')}}" readonly>
                            </div>
                            {{-- <div class="col-md-6 mb-3">
                                <label for="live_tracking_url">Live Tracking URL</label>
                                <input name="live_tracking_url" type="text" class="form-control" id="live_tracking_url" value="{{old('live_tracking_url')}}">
                                <small class="form-text text-muted">This field is optional.</small>
                                @error('live_tracking_url')
                                    <small class="text-danger">{{$errors->first('live_tracking_url')}}</small>
                                @enderror
                            </div> --}}
                        </div>
                        <div class="form-row">
                            {{-- <div class="col-md-6 mb-3">
                                <label for="tracking_number">Tracking Number</label>
                                <input name="tracking_number" type="text" class="form-control" id="tracking_number" value="{{ $trackingNumber, old('tracking_number')}}" onkeypress="return onlyNumberKey(event)" disabled>
                                @error('tracking_number')
                                    <small class="text-danger">{{$errors->first('tracking_number')}}</small>
                                @enderror
                            </div> --}}
                            <div class="col-md-6 mb-3">
                                <label for="live_tracking_url">Live Tracking URL</label>
                                <input name="live_tracking_url" type="text" class="form-control" id="live_tracking_url" value="{{old('live_tracking_url')}}">
                                <small class="form-text text-muted">This field is optional.</small>

                                @error('live_tracking_url')
                                    <small class="text-danger">{{$errors->first('live_tracking_url')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="awb_number">AWB Number</label>
                                <input name="awb_number" type="text" class="form-control" id="awb_number" value="{{old('awb_number')}}" onkeypress="return onlyNumberKey(event)">
                                @error('awb_number')
                                    <small class="text-danger">{{$errors->first('awb_number')}}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row mb-3">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="trackingStatus">Shipment Status</label>
                                    <select class="form-control select2" id="trackingStatus" name="trackingStatus" required>
                                        <?php
                                            foreach ($statuses as $status) {
                                        ?>
                                            <option value="{{$status->id}}">{{$status->name}}</option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div>
                                    <label for="note">Note</label>
                                    <textarea name="note" id="note" rows="3" class="form-control"></textarea>
                                    <small id="emailHelp1" class="form-text text-muted">This field is optional.</small>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label>Use Predefined Statuses</label>
                                <div>
                                    <ol>
                                        <li>
                                            Booking In Progress
                                            <ul>
                                                <li>Booking Generated</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Out For Pickiup
                                            <ul>
                                                <li>Pick-up In Progress</li>
                                            </ul>
                                        </li>
                                        <li>
                                            In Transit
                                            <ul>
                                                <li>In Transit To Warehouse</li>
                                                <li>Parcel Received At Warehouse</li>
                                                <li>Preparing For Dispatch</li>
                                                <li>In Transit To Final Destination</li>
                                                <li>Arrived At Destination</li>
                                                <li>Under Customs Process</li>
                                                <li>Released And Arrived At Warehouse</li>
                                                <li>Out For Delivery</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Delivered
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/tracking/script.js') }}"></script>
<script src="{{ asset('assets/admin/js/tracking/validate.js') }}"></script>

@endsection
