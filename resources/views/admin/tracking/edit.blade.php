@extends('layouts.admin')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Tracking</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Edit Tracking</strong>
                </div>
                <div class="card-body">
                    <form id="add-tracking" class="tracking" action="{{route('admin.tracking.update',$tracking)}}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">Customer Name</label>
                                <input type="text" class="form-control" id="firstname" value="{{$tracking->shipment->user->firstname . " " . $tracking->shipment->user->lastname}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="tracking_number">Tracking Number</label>
                                <input name="tracking_number" type="text" class="form-control" id="tracking_number" value="{{$tracking->tracking_number}}" onkeypress="return onlyNumberKey(event)" readonly>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="live_tracking_url">Live Tracking URL</label>
                                <input name="live_tracking_url" type="text" class="form-control" id="live_tracking_url" value="{{$tracking->live_tracking_url}}">
                                @error('live_tracking_url')
                                    <small class="text-danger">{{$errors->first('live_tracking_url')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="awb_number">AWB Number</label>
                                <input name="awb_number" type="text" class="form-control" id="awb_number" value="{{$tracking->awb_number}}" onkeypress="return onlyNumberKey(event)">
                                @error('awb_number')
                                    <small class="text-danger">{{$errors->first('awb_number')}}</small>
                                @enderror
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/tracking/script.js') }}"></script>
<script src="{{ asset('assets/admin/js/tracking/validate.js') }}"></script>

@endsection
