@extends('layouts.admin')
@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title">Trackings</h1>
            <div class="row my-4">
                <ul class="col-12 nav nav-tabs mx-3 mb-3" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="all-tab" data-toggle="tab" href="#allShipments" role="tab" aria-controls="home" aria-selected="true">All</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="booking-in-process-tab" data-toggle="tab" href="#booking-in-process" role="tab" aria-controls="booking-in-process" aria-selected="false">Booking In Process</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="out-for-pickup-tab" data-toggle="tab" href="#out-for-pickup" role="tab" aria-controls="out-for-pickup" aria-selected="false">Out For Pickup</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="in-transit-tab" data-toggle="tab" href="#in-transit" role="tab" aria-controls="in-transit" aria-selected="false">In-Transit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="delivered-tab" data-toggle="tab" href="#delivered" role="tab" aria-controls="delivered" aria-selected="false">Delivered</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="allShipments" role="tabpanel" aria-labelledby="all-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="shipmentDataTable">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Tracking Number</th>
                                                <th>AWB Number</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Status</th>
                                                <th>Notes</th>
                                                <th>Live Track</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($trackings as $tracking) {
                                                    // $status = $tracking->statuses->last();
                                                    // dd($status->pivot->note);
                                            ?>
                                                <tr>
                                                    <td>
                                                        <a href="{{ route('admin.tracking.editStatus',$tracking->id) }}">
                                                        {{$tracking->shipment->user->firstname . "  " . $tracking->shipment->user->lastname}}
                                                        </a>
                                                    </td>
                                                    <td>{{$tracking->tracking_number}}</td>
                                                    <td>{{$tracking->awb_number}}</td>
                                                    <td>{{$tracking->shipment->senderAddress->country->name." -  ". $tracking->shipment->senderAddress->pincode}}</td>
                                                    <td>{{$tracking->shipment->receiverAddress->country->name ." -  ". $tracking->shipment->receiverAddress->pincode}}</td>
                                                    <td>{{$tracking->lastStatus()->name}}</td>
                                                    <td>{{$tracking->statuses->last()->pivot->note}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.editStatus',$tracking->id) }}"><i class="fe fe-share mr-1 text-info"></i> Update Status </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.edit',$tracking->id) }}"><i class="fe fe-edit mr-1 text-warning"></i> Edit </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.show',$tracking) }}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="booking-in-process" role="tabpanel" aria-labelledby="booking-in-process-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="pendingShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Tracking Number</th>
                                                <th>AWB Number</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Status</th>
                                                <th>Live Track</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($trackings as $tracking) {
                                                    if($tracking->lastStatus()->id == 1){
                                            ?>
                                                <tr>
                                                    <td>{{$tracking->shipment->user->firstname . "  " . $tracking->shipment->user->lastname}}</td>
                                                    <td>{{$tracking->tracking_number}}</td>
                                                    <td>{{$tracking->awb_number}}</td>
                                                    <td>{{$tracking->shipment->senderAddress->country->name." -  ". $tracking->shipment->senderAddress->pincode}}</td>
                                                    <td>{{$tracking->shipment->receiverAddress->country->name ." -  ". $tracking->shipment->receiverAddress->pincode}}</td>
                                                    <td>{{$tracking->lastStatus()->name}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.editStatus',$tracking->id) }}"><i class="fe fe-share mr-1 text-info"></i> Update Status </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.edit',$tracking->id) }}"><i class="fe fe-edit mr-1 text-warning"></i> Edit </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.show',$tracking) }}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="out-for-pickup" role="tabpanel" aria-labelledby="out-for-pickup-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="approvedShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Tracking Number</th>
                                                <th>AWB Number</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Status</th>
                                                <th>Live Track</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($trackings as $tracking) {
                                                    if($tracking->lastStatus()->id == 3){
                                            ?>
                                                <tr>
                                                    <td>{{$tracking->shipment->user->firstname . "  " . $tracking->shipment->user->lastname}}</td>
                                                    <td>{{$tracking->tracking_number}}</td>
                                                    <td>{{$tracking->awb_number}}</td>
                                                    <td>{{$tracking->shipment->senderAddress->country->name." -  ". $tracking->shipment->senderAddress->pincode}}</td>
                                                    <td>{{$tracking->shipment->receiverAddress->country->name ." -  ". $tracking->shipment->receiverAddress->pincode}}</td>
                                                    <td>{{$tracking->lastStatus()->name}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.editStatus',$tracking->id) }}"><i class="fe fe-share mr-1 text-info"></i> Update Status </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.edit',$tracking->id) }}"><i class="fe fe-edit mr-1 text-warning"></i> Edit </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.show',$tracking) }}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="in-transit" role="tabpanel" aria-labelledby="in-transit-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="rejectedShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Tracking Number</th>
                                                <th>AWB Number</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Status</th>
                                                <th>Live Track</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($trackings as $tracking) {
                                                    if($tracking->lastStatus()->id == 2){
                                            ?>
                                                <tr>
                                                    <td>{{$tracking->shipment->user->firstname . "  " . $tracking->shipment->user->lastname}}</td>
                                                    <td>{{$tracking->tracking_number}}</td>
                                                    <td>{{$tracking->awb_number}}</td>
                                                    <td>{{$tracking->shipment->senderAddress->country->name." -  ". $tracking->shipment->senderAddress->pincode}}</td>
                                                    <td>{{$tracking->shipment->receiverAddress->country->name ." -  ". $tracking->shipment->receiverAddress->pincode}}</td>
                                                    <td>{{$tracking->lastStatus()->name}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.editStatus',$tracking->id) }}"><i class="fe fe-share mr-1 text-info"></i> Update Status </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.edit',$tracking->id) }}"><i class="fe fe-edit mr-1 text-warning"></i> Edit </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.show',$tracking) }}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>

                    <div class="tab-pane fade" id="delivered" role="tabpanel" aria-labelledby="delivered-tab">
                        <div class="col-md-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <!-- table -->
                                    <table class="table datatables" id="rejectedShipments">
                                        <thead>
                                            <tr>
                                                <th>Customer Name</th>
                                                <th>Tracking Number</th>
                                                <th>AWB Number</th>
                                                <th>Origin</th>
                                                <th>Destination</th>
                                                <th>Status</th>
                                                <th>Live Track</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                foreach ($trackings as $tracking) {
                                                    if($tracking->lastStatus()->id == 4){
                                            ?>
                                                <tr>
                                                    <td>{{$tracking->shipment->user->firstname . "  " . $tracking->shipment->user->lastname}}</td>
                                                    <td>{{$tracking->tracking_number}}</td>
                                                    <td>{{$tracking->awb_number}}</td>
                                                    <td>{{$tracking->shipment->senderAddress->country->name." -  ". $tracking->shipment->senderAddress->pincode}}</td>
                                                    <td>{{$tracking->shipment->receiverAddress->country->name ." -  ". $tracking->shipment->receiverAddress->pincode}}</td>
                                                    <td>{{$tracking->lastStatus()->name}}</td>
                                                    <td>
                                                        <a class="btn btn-sm btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-sm dropdown-toggle more-horizontal" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span class="text-muted sr-only">Action</span>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.editStatus',$tracking->id) }}"><i class="fe fe-share mr-1 text-info"></i> Update Status </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.edit',$tracking->id) }}"><i class="fe fe-edit mr-1 text-warning"></i> Edit </a>
                                                            <a class="dropdown-item" href="{{ route('admin.tracking.show',$tracking) }}"><i class="fe fe-eye mr-1 text-primary"></i> View</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- simple table -->
                    </div>
                </div>
            </div> <!-- end section -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    $('#shipmentDataTable, #pendingShipments, #approvedShipments, #rejectedShipments').DataTable(
    {
        autoWidth: true,
        "lengthMenu": [
            [10, 20, 50, -1],
            [10, 20, 50, "All"]
        ],
        order: [[5, 'desc']],
    });
</script>

@endsection
