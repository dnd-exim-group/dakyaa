@extends('layouts.admin')
@section('styles')

<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row my-4">
                <div class="col">
                    <h1 class="h2 page-title">Tracking - {{$tracking->shipment->user->firstname}}'s</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card shadow mb-4">
                        <div class="card-body pb-1">
                            <h2 class="h3 mb-3">Customer Details</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Customer Name</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->user->firstname}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Vendor</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->service->vendor->name}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Service</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->service->name}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Shipment Type</label>
                                        <strong>
                                            <?php
                                                if(config('constants.PERSONAL_SHIPMENT') == $tracking->shipment->shipment_type){
                                            ?>
                                                <p class="large">Personal/Shipment</p>
                                            <?php
                                                }
                                            ?>
                                            <?php
                                                if(config('constants.COMMERCIAL_SHIPMENT') == $tracking->shipment->shipment_type){
                                            ?>
                                                <p class="large">Commercial Shipment</p>
                                            <?php
                                                }
                                            ?>
                                            <?php
                                                if(config('constants.OTHERS_SHIPMENT') == $tracking->shipment->shipment_type){
                                            ?>
                                                <p class="large">Others</p>
                                            <?php
                                                }
                                            ?>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Origin</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->senderAddress->country->name ." ". $tracking->shipment->senderAddress->pincode}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Destination</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->receiverAddress->country->name ." ". $tracking->shipment->receiverAddress->pincode}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">AWB Number</label>
                                        <strong>
                                            <p class="large">{{$tracking->awb_number}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Tracking Number</label>
                                        <strong>
                                            <p class="large">{{$tracking->tracking_number}}</p>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Booking Date</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->created_at}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Payment Status</label>
                                        <strong>
                                            <p class="large">{{$tracking->shipment->payment_status == 0 ? 'Pending' : 'Success'}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Shipment Status</label>
                                        <strong>
                                            <p class="large">{{$tracking->lastStatus()->name}}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <a class="btn btn-primary" target="_blank" href="{{$tracking->live_tracking_url}}">Track Live</a>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- ./card-text -->
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="card timeline">
                        <div class="card-header">
                            <h4 class="d-inline-block m-0"><strong class="card-title">Tracking History</strong></h4>
                        </div>
                        <div class="card-body">
                            <?php
                                foreach ($tracking->statuses as $status) {
                            ?>
                                <div class="pb-4 timeline-item item-success">
                                    <div class="pl-5">
                                        <div class="mb-2">
                                            <strong>{{$status->pivot->created_at}}</strong>
                                        </div>
                                        <p class="mb-2 large"><strong>{{$status->name}} <span class="badge badge-light">{{$status->pivot->created_at->diffForHumans()}}</span></strong></p>
                                        <div class="text-muted">
                                            {{$status->pivot->note}}
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div> <!-- / .card-body -->
                    </div> <!-- / .card -->
                </div> <!-- / .col-md-6 -->
            </div>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script>
    // $('#packingList').DataTable(
    // {
    //     autoWidth: true,
    //     "lengthMenu": [
    //         [10, 20, 50, -1],
    //         [10, 20, 50, "All"]
    //     ]
    // });
</script>

@endsection
