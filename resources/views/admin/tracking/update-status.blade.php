@extends('layouts.admin')
@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Tracking</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Edit Tracking</strong>
                </div>
                <div class="card-body">
                    <form id="edit-tracking" class="tracking" action="{{route('admin.tracking.updateStatus',$tracking)}}" method="POST">
                        @csrf
                        @method("POST")
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname">Customer Name</label>
                                <input type="text" class="form-control" id="firstname" value="{{$tracking->shipment->user->firstname ." ". $tracking->shipment->user->lastname}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="tracking_number">Tracking Number</label>
                                <input name="tracking_number" type="text" class="form-control" id="tracking_number" value="{{old('tracking_number', $tracking->tracking_number)}}" onkeypress="return onlyNumberKey(event)" readonly>
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="live_tracking_url">Live Tracking URL</label>
                                <input name="live_tracking_url" type="text" class="form-control" id="live_tracking_url" value="{{old('live_tracking_url', $tracking->live_tracking_url)}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="awb_number">AWB Number</label>
                                <input name="awb_number" type="text" class="form-control" id="awb_number" value="{{old('awb_number', $tracking->awb_number)}}" onkeypress="return onlyNumberKey(event)" readonly>
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="origin">Origin</label>
                                <input type="text" class="form-control" id="origin" value="{{$tracking->shipment->senderAddress->country->name ." ". $tracking->shipment->senderAddress->pincode}}" readonly>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="origin">Destination</label>
                                <input type="text" class="form-control" id="origin" value="{{$tracking->shipment->receiverAddress->country->name ." ". $tracking->shipment->receiverAddress->pincode}}" readonly>
                            </div>
                        </div> <!-- /.form-row -->
                        <div class="form-row mb-3">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="trackingStatus">Shipment Status</label>
                                    <select class="form-control select2" id="trackingStatus" name="trackingStatus">
                                        <?php
                                            foreach ($statuses as $status) {
                                        ?>
                                            <option value="{{$status->id}}">{{$status->name}}  {{($status->id == $tracking->lastStatus()->id )? "(current)":""}}</option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div>
                                    <label for="note">Note</label>
                                    <textarea name="note" id="note" rows="3" class="form-control">{{null}}</textarea>
                                    <small id="emailHelp1" class="form-text text-muted">This field is optional.</small>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="pre-defined-statuses" class="bold">Use Predefined Statuses</label>
                                <div id="pre-defined-statuses">
                                    <ol>
                                        <li>
                                            Booking In Progress
                                            <ul>
                                                <li>Booking Generated</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Out For Pickiup
                                            <ul>
                                                <li>Pick-up In Progress</li>
                                            </ul>
                                        </li>
                                        <li>
                                            In Transit
                                            <ul>
                                                <li>In Transit To Warehouse</li>
                                                <li>Parcel Received At Warehouse</li>
                                                <li>Preparing For Dispatch</li>
                                                <li>In Transit To Final Destination</li>
                                                <li>Arrived At Destination</li>
                                                <li>Under Customs Process</li>
                                                <li>Released And Arrived At Warehouse</li>
                                                <li>Out For Delivery</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Delivered
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">

                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/tracking/script.js') }}"></script>
<script src="{{ asset('assets/admin/js/tracking/validate.js') }}"></script>

@endsection
