@extends('layouts.admin')

@section('styles')

<!-- <script src="{{ asset('assets/global/css/dropzone.css') }}"></script> -->
<!-- <script src="{{ asset('assets/global/css/dropzone.min.css') }}"></script> -->

@endsection

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Vendors</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Vendor</strong>
                </div>
                <div class="card-body">
                    <form id="vendor-form" class="vendor" method="POST" action="{{route('admin.vendor.store')}}" enctype='multipart/form-data'>
                        @csrf
                        @method('POST')
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="name">Name</label>
                                    <input name="name" value="{{old('name')}}" type="text" class="form-control" id="name">
                                    @error('name')
                                        <small class="text-danger">{{$errors->first('name')}}</small>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="email">Email address</label>
                                    <input name="email" type="email" value="{{old('email')}}" class="form-control" id="email">
                                    @error('email')
                                        <small class="text-danger">{{$errors->first('email')}}</small>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label for="phone">Phone Number</label>
                                    <div class="form-row">
                                        <div class="col-md-2">
                                            <select name="country_code" class="form-control select2" id="simple-select2">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{ $country->name == 'India' ? 'selected' : '' }}>{{ $country->dial_code.' '.$country->code}}</option>
                                                <?php
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-10">
                                            <input type="text" value="{{old('phone')}}" name="phone" class="form-control" id="phone">
                                            @error('phone')
                                                <small class="text-danger">{{$errors->first('phone')}}</small>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="address">Address</label>
                                    <textarea name="address" class="form-control" id="address" rows="3">{{old('address')}}</textarea>
                                    @error('address')
                                        <small class="text-danger">{{$errors->first('address')}}</small>
                                    @enderror
                                </div>
                                <!-- <form></form>
                                <label for="tinydash-dropzone">Upload Logo</label>
                                <form action="/file-upload" class="dropzone bg-light rounded-lg" id="tinydash-dropzone">
                                    <div class="dz-message needsclick">
                                        <div class="circle circle-lg bg-primary">
                                            <i class="fe fe-upload fe-24 text-white"></i>
                                        </div>
                                        <h5 class="text-muted mt-4">Drop files here or click to upload</h5>
                                    </div>
                                </form> -->
                                <label for="logoUpload">Upload Logo</label>
                                <div>
                                    <input accept="image" id="logoUpload" name="logoUpload" type="file">
                                </div>
                            </div>



                            <div class="col-md-6">
                                <button class="btn btn-primary mt-3" type="submit">Submit</button>
                            </div>
                        </div> <!-- /.form-row -->
                    </form>

                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/admin/js/vendor/validate.js') }}"></script>
<!-- <script src="{{ asset('assets/global/js/dropzone.min.js') }}"></script> -->

@endsection
