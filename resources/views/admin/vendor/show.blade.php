@extends('layouts.admin')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title mb-3">Vendors</h1>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Add Vendor</strong>
                </div>
                <div class="card-body">
                    <form id="vendor-form" class="needs-validation" novalidate>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="validationCustom3">Name</label>
                                    <input name="name" type="text" class="form-control" id="validationCustom3" value="{{$vendor->name}}" readonly>
                                    <div class="invalid-feedback"> Please enter name </div>
                                    <div class="valid-feedback"> Looks good! </div>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputEmail2">Email address</label>
                                    <input name="email" type="email" class="form-control" id="exampleInputEmail2" aria-describedby="emailHelp1" value="{{$vendor->email}}" readonly>
                                    <div class="invalid-feedback"> Please use a valid email </div>
                                    <small id="emailHelp1" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                </div>
                                <div class="mb-3">
                                    <label for="custom-phone">Phone Number</label>
                                    <input name="phone" class="form-control input-phoneus" id="custom-phone" maxlength="14" value="{{$vendor->phone_no}}" readonly>
                                    <div class="invalid-feedback"> Please enter a correct phone </div>
                                </div>
                                <div class="mb-3">
                                    <label for="validationTextarea1">Address</label>
                                    <textarea name="address" class="form-control" id="validationTextarea1" rows="3" readonly> "{{$vendor->address}}"</textarea>
                                    <div class="invalid-feedback"> Please enter address </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <form></form>
                                <label for="tinydash-dropzone">Upload Logo</label>
                                <form action="/file-upload" class="dropzone bg-light rounded-lg" id="tinydash-dropzone">
                                    <div class="dz-message needsclick">
                                        <div class="circle circle-lg bg-primary">
                                            <i class="fe fe-upload fe-24 text-white"></i>
                                        </div>
                                        <h5 class="text-muted mt-4">Drop files here or click to upload</h5>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-6">
                                <a class="btn btn-primary mt-3" href="{{route('admin.vendors')}}">Back</a>
                            </div>
                        </div> <!-- /.form-row -->
                    </form>

                </div> <!-- /.card-body -->
            </div> <!-- /.card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/global/js/jquery.validate.min.js') }}"></script>
<script>
    (function()
    {
        'use strict';
        window.addEventListener('load', function()
        {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form)
            {
                form.addEventListener('submit', function(event)
                {
                    if (form.checkValidity() === false)
                    {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
@endsection
