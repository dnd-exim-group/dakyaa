@extends('layouts.customer')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <h2 class="h3 mb-4 page-title">Profile</h2>
            <div class="my-4">
                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false">Addresses</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Notifications</a>
                    </li> --}}
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">

                            <div class="row mt-5 align-items-center">
                                <div class="col-md-3 text-center mb-4">
                                    <div class="avatar avatar-xl">
                                        <img src="{{ asset('assets/images/avatars/face-1.jpg') }}" alt="..." class="avatar-img rounded-circle">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row align-items-center">
                                        <div class="col-md-7">
                                            <h4 class="mb-1">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h4>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col">
                                            <p class="small mb-0 text-muted">{{Auth::user()->email}}</p>
                                            @if(Auth::user()->country)
                                                <p class="small mb-0 text-muted">{{Auth::user()->country['dial_code']}} {{Auth::user()->phone_no}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">

                            {{-- <div class="d-inline-block float-right">
                                <a href="" id="edit-profile" class="btn btn-warning text-white" onclick="enableFields()"><i class="fe fe-edit"></i> Edit</a>
                            </div> --}}

                            <h4 class="mb-3">Details</h4>
                        <form id="profile-details" method="POST" action="{{ route('customer.profile.update', Auth::user()->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" value="{{Auth::user()->firstname}}">
                                    @error('firstname')
                                        <small class="text-danger">{{$errors->first('firstname')}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="lastname">Lastname</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" value="{{Auth::user()->lastname}}">
                                    @error('lastname')
                                        <small class="text-danger">{{$errors->first('lastname')}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" id="email" value="{{Auth::user()->email}}" disabled>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="senderPhone">Phone Number</label>
                                    <div class="form-row">
                                        @if(Auth::user()->country)
                                            <div class="col-md-2 col-sm-4 mb-3">
                                                <select class="form-control select2" id="simple-select2" name="dial_code">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                    <option value="{{$country->id}}" {{$country->id == Auth::user()->country->id ? 'selected' : ''}}> {{$country->dial_code.' '.$country->code}} </option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('dial_code')
                                                    <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                                @enderror
                                            </div>
                                            <div class="col-md-10 col-sm-6">
                                                <input type="text" name="phone_no" class="form-control" id="phone_no" value="{{ Auth::user()->phone_no }}">
                                                @error('phone_no')
                                                    <small class="text-danger">{{$errors->first('phone_no')}}</small>
                                                @enderror
                                            </div>
                                        @else
                                            <div class="col-md-2 col-sm-4 mb-3">
                                                <select class="form-control select2" id="simple-select2" name="dial_code">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                    <option value="{{$country->id}}" {{$country->name == 'India' ? 'selected' : ''}}> {{$country->dial_code.' '.$country->code}} </option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('dial_code')
                                                    <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                                @enderror
                                            </div>
                                            <div class="col-md-10 col-sm-6">
                                                <input type="text" name="phone_no" class="form-control" id="senderPhone" value="{{ old('phone_no', $address->phone_no) }}">
                                                @error('phone_no')
                                                    <small class="text-danger">{{$errors->first('phone_no')}}</small>
                                                @enderror
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update Details</button>
                            </div>
                        </form>
                            <hr class="my-4">

                        <form id="update-password" method="POST" action="{{ route('customer.profile.updatePassword', Auth::user()->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">New Password</label>
                                        <input type="password" name="password" class="form-control" id="password">
                                        @error('password')
                                            <small class="text-danger">{{$errors->first('password')}}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="confirmpassword">
                                        @error('confirmpassword')
                                            <small class="text-danger">{{$errors->first('confirmpassword')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-2">Password requirements</p>
                                    <p class="small text-muted mb-2"> To create a new password, you have to meet all of the following requirements: </p>
                                    <ul class="small text-muted pl-4 mb-0">
                                        <li> Minimum 8 character </li>
                                        <li>At least one special character</li>
                                        <li>At least one number</li>
                                        <li>Can’t be the same as a previous password </li>
                                    </ul>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update Password</button>
                        </form>
                    </div>

                    <div class="tab-pane fade show active" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">
                        <a href="{{ route('customer.profile') }}" class="btn btn-primary mb-3"><i class="fe fe-arrow-left-circle"></i> Back</a>
                        <form id="edit-address-form" method="POST" action="{{ route('customer.address.update', $address) }}">
                            @csrf
                            @method('PUT')

                            <div class="row justify-content-center">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="firstname">First Name</label>
                                                <input name="firstname" type="text" class="form-control" id="firstname" value="{{old('firstname', $address->firstname)}}">
                                                @error('firstname')
                                                    <small class="text-danger">{{$errors->first('firstname')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="lastname">Last Name</label>
                                                <input name="lastname" type="text" class="form-control" id="lastname" value="{{old('lastname', $address->lastname)}}">
                                                @error('lastname')
                                                    <small class="text-danger">{{$errors->first('lastname')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="email">Email ID</label>
                                        <input name="email" type="email" class="form-control" id="senderEmail" value="{{old('email', $address->email)}}">
                                        @error('email')
                                            <small class="text-danger">{{$errors->first('email')}}</small>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="phone_no">Phone Number</label>
                                        <div class="form-row">
                                            <div class="col-md-3 col-sm-4" style="width: 24%;">
                                                <select class="form-control select2" id="simple-select2" name="dial_code">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{ $country->id == $address->country_code ? 'selected' : ''; }}>{{$country->dial_code.' '.$country->code}}</option>
                                                <?php
                                                    }
                                                ?>
                                                </select>
                                                @error('dial_code')
                                                    <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                                @enderror
                                            </div>
                                            <div class="col-md-9 col-sm-8" style="width: 76%;">
                                                <input type="text" name="phone_no" class="form-control" id="phone_no" value="{{old('phone_no', $address->phone_no)}}">
                                                @error('phone_no')
                                                    <small class="text-danger">{{$errors->first('phone_no')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="address_line1">Address Line 1</label>
                                        <input name="address_line1" type="text" class="form-control" id="address_line1" value="{{old('address_line1', $address->address_line1)}}">
                                        @error('address_line1')
                                            <small class="text-danger">{{$errors->first('address_line1')}}</small>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="address_line2">Address Line 2 <span class="text-muted small">(Optional)</span></label>
                                        <input name="address_line2" type="text" class="form-control" id="address_line2" value="{{old('address_line2', $address->address_line2)}}">
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="city">City</label>
                                                <input name="city" type="text" class="form-control" id="city" value="{{old('city', $address->city)}}">
                                                @error('city')
                                                    <small class="text-danger">{{$errors->first('city')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="pincode">Pincode</label>
                                                <input type="text" class="form-control" id="pincode" name="pincode" value="{{old('pincode', $address->pincode)}}">
                                                @error('pincode')
                                                    <small class="text-danger">{{$errors->first('pincode')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="state">State</label>
                                                <input name="state" type="text" class="form-control" id="state" value="{{old('state', $address->state_country)}}">
                                                @error('state')
                                                    <small class="text-danger">{{$errors->first('state')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="mb-3">
                                                <label for="country">Country</label>
                                                <select name="country" class="form-control select2" id="simple-select2">
                                                    <option value="">Select Orign</option>
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                        <option value="{{$country->id}}" {{ $country->id == $address->country_id ? 'selected' : ''; }}>{{$country->name}}</option>
                                                <?php
                                                    }
                                                ?>
                                                    </select>
                                                @error('country')
                                                    <small class="text-danger">{{$errors->first('country')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="address_type">Address Type</label>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="addressTypeResidential" name="address_type" value="{{config("constants.RESIDENT")}}" {{ $address->address_type == config("constants.RESIDENT") ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="addressTypeResidential">Residential</label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="custom-control custom-radio mb-3">
                                                    <input type="radio" class="custom-control-input" id="addressTypeCommercial" name="address_type" value="{{config("constants.COMMERCIAL")}}" {{ $address->address_type == config("constants.COMMERCIAL") ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="addressTypeCommercial">Commercial</label>
                                                </div>
                                            </div>
                                            @error('address_type')
                                                <small class="text-danger">{{$errors->first('address_type')}}</small>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="billing_address">Billing Address</label>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" class="custom-control-input" id="billingAddressYes" name="billing_address" {{ $address->is_billing_address == 1 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="billingAddressYes">Yes</label>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="custom-control custom-radio mb-3">
                                                    <input type="radio" class="custom-control-input" id="billingAddressNo" name="billing_address" {{ $address->is_billing_address == 0 ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="billingAddressNo">No</label>
                                                </div>
                                            </div>
                                            @error('billing_address')
                                                <small class="text-danger">{{$errors->first('billing_address')}}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
                        <form>

                        </form>
                    </div>
                </div>
            </div> <!-- /.card-body -->
        </div> <!-- /.col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/customer/js/address/validate.js') }}"></script>

@endsection
