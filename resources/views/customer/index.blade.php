@extends('layouts.customer')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h5 page-title">Customer Dashboard</h1>
            <div class="text-center align-middle">
                <a type="button" href="{{ route('quotes.index') }}" class="btn mb-2 btn-primary"> Get A Quote </a>
            </div>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

