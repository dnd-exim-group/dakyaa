@extends('layouts.customer')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">
            <h2 class="h3 mb-4 page-title">Profile</h2>
            <div class="my-4">
                <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="addresses-tab" data-toggle="tab" href="#addresses" role="tab" aria-controls="addresses" aria-selected="false">Addresses</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" id="notifications-tab" data-toggle="tab" href="#notifications" role="tab" aria-controls="notifications" aria-selected="false">Notifications</a>
                    </li> --}}
                </ul>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">

                            <div class="row mt-5 align-items-center">
                                <div class="col-md-3 text-center mb-4">
                                    <div class="avatar avatar-xl">
                                        <img src="{{ asset('assets/images/avatars/face-1.jpg') }}" alt="..." class="avatar-img rounded-circle">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="row align-items-center">
                                        <div class="col-md-7">
                                            <h4 class="mb-1">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h4>
                                        </div>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col">
                                            <p class="small mb-0 text-muted">{{Auth::user()->email}}</p>
                                            @if(Auth::user()->country)
                                                <p class="small mb-0 text-muted">{{Auth::user()->country['dial_code']}} {{Auth::user()->phone_no}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr class="my-4">

                            {{-- <div class="d-inline-block float-right">
                                <a href="" id="edit-profile" class="btn btn-warning text-white" onclick="enableFields()"><i class="fe fe-edit"></i> Edit</a>
                            </div> --}}

                            <h4 class="mb-3">Details</h4>

                        <form id="profile-details" method="POST" action="{{ route('customer.profile.update', Auth::user()->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" name="firstname" id="firstname" class="form-control" value="{{Auth::user()->firstname}}">
                                    @error('firstname')
                                        <small class="text-danger">{{$errors->first('firstname')}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="lastname">Lastname</label>
                                    <input type="text" name="lastname" id="lastname" class="form-control" value="{{Auth::user()->lastname}}">
                                    @error('lastname')
                                        <small class="text-danger">{{$errors->first('lastname')}}</small>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control" id="email" value="{{Auth::user()->email}}" disabled>
                                    @error('email')
                                        <small class="text-danger">{{$errors->first('email')}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="senderPhone">Phone Number</label>
                                    <div class="form-row">
                                        @if(Auth::user()->country)
                                            <div class="col-md-2 col-sm-4 mb-3">
                                                <select class="form-control select2" id="simple-select2" name="dial_code">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                    <option value="{{$country->id}}" {{$country->id == Auth::user()->country->id ? 'selected' : ''}}> {{$country->dial_code.' '.$country->code}} </option>
                                                <?php
                                                    }
                                                ?>
                                                    @error('dial_code')
                                                        <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                                    @enderror
                                                </select>
                                            </div>
                                            <div class="col-md-10 col-sm-6">
                                                <input type="text" name="phone_no" class="form-control" id="senderPhone" value="{{Auth::user()->phone_no}}">
                                                @error('phone_no')
                                                    <small class="text-danger">{{$errors->first('phone_no')}}</small>
                                                @enderror
                                            </div>
                                        @else
                                            <div class="col-md-2 col-sm-4 mb-3">
                                                <select class="form-control select2" id="simple-select2" name="dial_code">
                                                <?php
                                                    foreach ($countries as $country) {
                                                ?>
                                                    <option value="{{$country->id}}" {{$country->name == 'India' ? 'selected' : ''}}> {{$country->dial_code.' '.$country->code}} </option>
                                                <?php
                                                    }
                                                ?>
                                                    @error('dial_code')
                                                        <small class="text-danger">{{$errors->first('dial_code')}}</small>
                                                    @enderror
                                                </select>
                                            </div>
                                            <div class="col-md-10 col-sm-6">
                                                <input type="text" name="phone_no" class="form-control" id="senderPhone" value="">
                                                @error('phone_no')
                                                    <small class="text-danger">{{$errors->first('phone_no')}}</small>
                                                @enderror
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update Details</button>
                            </div>
                        </form>
                            <hr class="my-4">

                        <form id="update-password" method="POST" action="{{ route('customer.profile.updatePassword', Auth::user()->id) }}">
                            @csrf
                            @method('PUT')

                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="password">New Password</label>
                                        <input type="password" name="password" class="form-control" id="password">
                                        @error('password')
                                            <small class="text-danger">{{$errors->first('password')}}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword">Confirm Password</label>
                                        <input type="password" class="form-control" id="confirmpassword" name="confirmpassword">
                                        @error('confirmpassword')
                                            <small class="text-danger">{{$errors->first('confirmpassword')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-2">Password requirements</p>
                                    <p class="small text-muted mb-2"> To create a new password, you have to meet all of the following requirements: </p>
                                    <ul class="small text-muted pl-4 mb-0">
                                        <li> Minimum 8 character </li>
                                        <li>At least one special character</li>
                                        <li>At least one number</li>
                                        <li>Can’t be the same as a previous password </li>
                                    </ul>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Update Password</button>
                        </form>
                    </div>

                    <div class="tab-pane fade" id="addresses" role="tabpanel" aria-labelledby="addresses-tab">

                            <div class="row">
                                <div class="col">
                                    <div class="d-inline-block float-right mb-4">
                                        <a href="{{ route('customer.address.create') }}" class="btn btn-primary"><i class="fe fe-plus"></i> Add New Address</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                    foreach ($addresses as $address) {
                                ?>
                                <div class="col-md-4">
                                    <div class="card shadow mb-4">
                                        <div class="card-body">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <h5 class="mb-3">{{ $address->firstname.' '.$address->lastname }}</h5>
                                                    <p class="mb-1">{{ $address->countryCode->dial_code.' '.$address->phone_no }}</p>
                                                    <p class="mb-1">{{ $address->email }}</p>
                                                    <p class="mb-1">{{ $address->address_line1 }}</p>
                                                    <p class="mb-1">{{ $address->address_line2 }}</p>
                                                    <p class="mb-1">{{ $address->city.' - '. $address->pincode }}</p>
                                                    <p class="mb-1">{{ $address->state_country.', '.$address->country->name }}</p>
                                                    <p class="mb-0 float-right">{{ $address->address_type==config("constants.RESIDENT") ? "Residential" : "Commercial" }}</p>
                                                </div>
                                            </div>
                                        </div> <!-- / .card-body -->
                                        <div class="card-footer py-3">
                                            <div class="row align-items-center float-right">
                                                <div class="col" style="display: inline-flex">
                                                    <a href="{{ route('customer.address.edit', $address) }}" type="button" class="btn btn-sm btn-warning text-white mr-2"><i class="fe fe-edit"></i> Edit</a>
                                                    <form action="{{ route('customer.address.delete', $address) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-sm btn-danger"><i class="fe fe-trash"></i> Delete</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    }
                                ?>
                            </div>

                    </div>

                    <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
                        <form>
                            <h3>Coming Soon</h3>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- /.col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script src="{{ asset('assets/customer/js/profile/validate.js') }}"></script>

@endsection
