@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title d-inline">My Quotes</h1>
            <div class="d-inline-block float-right">
                <button type="button" class="btn mb-2 btn-primary" data-toggle="modal" data-target="#getAQuote"> Generate Quote </button>
            </div>
            <div class="row my-4">
                <?php
                    foreach ($quotes as $quote) {
                ?>
                    <div class="col-md-4">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <h5 class="mb-3 card-title d-inline">{{$quote->pickup->name .' - '. $quote->destination->name}}</h5>
                                <div class="d-inline float-right" id="deleteQuoteData">
                                    @cannot('deleteQuote', $quote)
                                        <div class="text-danger mr-3 d-inline deleteQuoteBtn" style="cursor: pointer" data-toggle="modal" data-target="#deleteQuoteModal"><i class="fe fe-trash-2"></i></div>
                                        <input type="hidden" class="deleteQuoteID" value="{{$quote->id}}">

                                        <a class="text-warning editQuoteBtn"  data-toggle="modal" data-target="#editAQuote"><i class="fe fe-edit" style="cursor: pointer"></i></a>
                                    @endcannot
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <p class="mb-1">Origin: <strong>{{$quote->pickup->name .' - '. $quote->pickup_pincode}}</strong></p>
                                        <p class="mb-1">Destination: <strong>{{$quote->destination->name .' - '. $quote->destination_pincode}}</strong></p>
                                        <p class="mb-1">Approx Weight: <strong>{{$quote->approx_weight}}</strong></p>
                                        <p class="mb-0">Created On: <strong>{{date('d-m-Y', strtotime($quote->created_at))}}</strong></p>
                                        <p class="mb-0">Booked: <strong>{{$quote->isBooked() ? 'Yes' : 'No'}}</strong></p>
                                    </div>
                                </div>
                            </div> <!-- / .card-body -->
                            <div class="card-footer py-3">
                                <div class="row align-items-center float-right">
                                    <div class="col" style="display: inline-flex">
                                        <form action="{{route('customer.quote.repeatQuote', $quote)}}" class="mr-2" method="POST">
                                            @csrf
                                            @method('POST')
                                            <button type="submit" class="btn btn-sm btn-info"><i class="fe fe-repeat"></i> Repeat</button>
                                        </form>
                                        @can('deleteQuote', $quote)
                                            <a href="{{route('shipments.show', $quote->shipment)}}" type="button" class="btn btn-sm btn-primary"><i class="fe fe-eye"></i> View Shipment</a>
                                        @else
                                            <a href="{{ route('customer.services.index', $quote) }}" type="button" class="btn btn-sm btn-primary"><i class="fe fe-clipboard"></i> Get Estimate</a>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                    }
                ?>

            </div>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<!-- Generate Quote Modal -->
<div class="modal fade" id="getAQuote" tabindex="-1" role="dialog" aria-labelledby="getAQuoteLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{route('quotes.store')}}" method="POST">
                @csrf
                @method('POST')
                <div class="modal-header">
                    <h5 class="modal-title" id="getAQuoteLabel">Generate Quote</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="lastname">Collection</label>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div id="phone-number" class="form-group">
                                        <select name="senderCountry" class="form-control" id="senderCountry" data-error="Please select one option.">
                                            <?php
                                                foreach ($countries as $country) {
                                                    if(Str::lower($country->name) == "india") {
                                            ?>
                                                        <option data-countrycode="" value="{{$country->id}}">{{ $country->name}}</option>
                                            <?php
                                                    } elseif (Str::lower($country->name) == "united kingdom") {
                                            ?>
                                                <option data-countrycode="" value="{{$country->id}}">{{ $country->name}}</option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                        @error('senderCountry')
                                            <small class="text-danger">{{$errors->first('senderCountry')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="text" name="senderPin" class="form-control" id="senderPin" placeholder="Collection Pincode*" type="text" value="" data-error="Collection Pincode*">
                                    @error('senderPin')
                                        <small class="text-danger">{{$errors->first('senderPin')}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="lastname">Delivery</label>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div id="phone-number" class="form-group">
                                        <select name="receiverCountry" class="form-control" id="receiverCountry" data-error="Please select one option.">
                                            <?php
                                                foreach ($countries as $country) {
                                                    if(!(Str::lower($country->name) == "india")) {
                                            ?>
                                                        <option data-countrycode="" value="{{$country->id}}">{{ $country->name }}</option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </select>
                                        @error('receiverCountry')
                                            <small class="text-danger">{{$errors->first('receiverCountry')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="text" name="receiverPin" class="form-control" id="receiverPin" placeholder="Delivery Pincode*" type="text" value="" data-error="Delivery Pincode*">
                                    @error('receiverPin')
                                        <small class="text-danger">{{$errors->first('receiverPin')}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="form-group mb-3">
                                <label for="approxweight">Approx Weight</label>
                                <input type="text" name="weight" id="approxweight" class="form-control" placeholder="Approx Weight*">
                                @error('weight')
                                    <small class="text-danger">{{$errors->first('weight')}}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Shipment Type</label>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="shipmentType1" name="shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}" checked>
                                        <label class="custom-control-label" for="shipmentType1">Personal/Gift</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="shipmentType2" name="shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}">
                                        <label class="custom-control-label" for="shipmentType2">Commercial</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="shipmentType3" name="shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}">
                                        <label class="custom-control-label" for="shipmentType3">Others</label>
                                    </div>
                                </div>
                                @error('shipmentType')
                                    <small class="text-danger">{{$errors->first('shipmentType')}}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn mb-2 btn-primary">Save Quote</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Quote Modal -->
<div class="modal fade" id="editAQuote" tabindex="-1" role="dialog" aria-labelledby="editAQuoteLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="" id="edit-modal-form" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="editAQuoteLabel">Edit Quote</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="lastname">Collection</label>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div id="phone-number" class="form-group">
                                        <select name="senderCountry" class="form-control" id="countryname2" data-error="Please select one option.">
                                            <?php
                                                foreach ($countries as $country) {
                                            ?>
                                                <option data-countrycode="" id="senderCountryOptionModal-{{$country->id}}" value="{{$country->id}}">{{ $country->name }}</option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                        @error('senderCountry')
                                            <small class="text-danger">{{$errors->first('senderCountry')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="text" name="senderPin" class="form-control" id="edit-collection" placeholder="Collection Pincode*" type="text" value="" data-error="Collection Pincode*">
                                    @error('senderPin')
                                        <small class="text-danger">{{$errors->first('senderPin')}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="lastname">Delivery</label>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div id="phone-number" class="form-group">
                                        <select name="receiverCountry" class="form-control" id="countryname2" data-error="Please select one option.">
                                            <?php
                                                foreach ($countries as $country) {
                                            ?>
                                                <option data-countrycode="" id="receiverCountryOptionModal-{{$country->id}}" value="{{$country->id}}">{{ $country->name }}</option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                        @error('receiverCountry')
                                            <small class="text-danger">{{$errors->first('receiverCountry')}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <input type="text" name="receiverPin" class="form-control" id="edit-delivery" placeholder="Delivery Pincode*" type="text" value="" data-error="Delivery Pincode*">
                                    @error('receiverPin')
                                        <small class="text-danger">{{$errors->first('receiverPin')}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3">
                            <div class="form-group mb-3">
                                <label for="edit-approxweight">Approx Weight</label>
                                <input type="text" name="weight" id="edit-approxweight" class="form-control">
                                @error('weight')
                                    <small class="text-danger">{{$errors->first('weight')}}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Shipment Type</label>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="edit-shipmentType-{{config('constants.PERSONAL_SHIPMENT')}}" name="edit_shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}">
                                        <label class="custom-control-label" for="edit-shipmentType-{{config('constants.PERSONAL_SHIPMENT')}}">Personal/Gift</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="edit-shipmentType-{{config('constants.COMMERCIAL_SHIPMENT')}}" name="edit_shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}" checked>
                                        <label class="custom-control-label" for="edit-shipmentType-{{config('constants.COMMERCIAL_SHIPMENT')}}">Commercial</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="edit-shipmentType-{{config('constants.OTHERS_SHIPMENT')}}" name="edit_shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}">
                                        <label class="custom-control-label" for="edit-shipmentType-{{config('constants.OTHERS_SHIPMENT')}}">Others</label>
                                    </div>
                                </div>
                                @error('edit_shipmentType')
                                    <small class="text-danger">{{$errors->first('edit_shipmentType')}}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn mb-2 btn-primary">Save Quote</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Delete Quote Modal -->
<div class="modal fade" id="deleteQuoteModal" tabindex="-1" role="dialog" aria-labelledby="deleteQuoteModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteQuoteModalLabel">Delete Quote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body"> Are you sure you want to delete? </div>
            <div class="modal-footer">
                <button type="button" class="btn mb-2 btn-secondary" data-dismiss="modal">Cancel</button>
                <form action="" method="POST" id="deleteQuoteForm">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn mb-2 btn-danger">Yes, Delete</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/customer/js/quotes/dataTables.bootstrap4.min.js') }}"></script> --}}
    <script>
        $('#quoteDataTable').DataTable(
        {
            autoWidth: true,
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ]
        });

        $('.deleteQuoteBtn').click(function() {
            quoteID = $(this).parents().children('.deleteQuoteID').val();
            // alert('customer/quote/' + $('#deleteQuoteID').val());
            $('#deleteQuoteForm').attr('action', `quotes/${quoteID}`);

        });

        $('.editQuoteBtn').click(function(){
            quoteID = $(this).parents().children('.deleteQuoteID').val();
            route = "{{ route('ajax.getQuoteById') }}";
            $.ajax({
                url: route,
                method: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: quoteID,
                },
                dataType: 'json',
                success: function (success) {
                    $(`#senderCountryOptionModal-${success.pickup_country_id}`).attr({"selected": true});
                    $(`#receiverCountryOptionModal-${success.destination_country_id}`).attr({"selected": true});
                    $(`#quote-type-${success.destination_country_id}`);
                    $('#edit-approxweight').val(success.approx_weight);
                    $('#edit-collection').val(success.pickup_pincode);
                    $('#edit-delivery').val(success.destination_pincode);
                    $(`#edit-shipmentType-${success.shipment_type}`).prop("checked", true);
                    $('#edit-modal-form').attr('action', `quotes/${success.id}`)
                }
            });
        });

    </script>
@endsection
