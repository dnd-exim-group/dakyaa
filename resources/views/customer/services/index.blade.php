@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title d-inline">Services</h1>

            <div class="row my-4 justify-content-center">
                <div class="col-md-12">
                    <div class="alert alert-light text-center" role="alert">
                        <span class="fe fe-credit-card fe-16 mr-2"></span> The price is an estimate based on the data provided
                    </div>
                </div>
                {{-- <div class="col-md-3">
                    <div class="card shadow mb-3">
                        <div class="card-header">
                            <strong class="card-title mb-0">Sort By</strong>
                            <i class="fe fe-grid text-muted float-right"></i>
                        </div>
                        <div class="card-body">
                            <div class="custom-control custom-radio mb-2">
                                <input type="radio" class="custom-control-input" id="price_low_to_high" name="sort_price">
                                <label class="custom-control-label" for="price_low_to_high">Price - Low to High</label>
                            </div>
                            <div class="custom-control custom-radio mb-2 checked">
                                <input type="radio" class="custom-control-input" id="price_high_to_low" name="sort_price">
                                <label class="custom-control-label" for="price_high_to_low">Price - High to Low</label>
                            </div>
                        </div> <!-- / .card-body -->
                    </div>
                </div> --}}

                <div class="col-md-9">
                    <?php
                        foreach ($services as $service) {
                    ?>
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <h5 class="mb-3 card-title d-inline">{{$service->name}}</h5>
                            </div>
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <p class="mb-1">Service: <strong>{{$service->name}}</strong></p>
                                        <p class="mb-1">Origin: <strong> {{$quote->pickup->name .' '. $quote->pickupPincode}}</strong></p>
                                        <p class="mb-1">Destination: <strong>{{$quote->destination->name .' '. $quote->destinationPincode}}</strong></p>
                                        <p class="mb-1">Approx Weight: <strong>{{$quote->approx_weight}}</strong></p>
                                        <p class="mb-0">Duration: <strong>{{$service->getDuration()}}</strong></p>
                                    </div>
                                    <div class="col text-right">
                                        <?php
                                            if(Auth::user()) {
                                        ?>
                                            <h4 class="m-0"><strong>₹ {{$service->getPrice($quote->approx_weight)}}</strong></h4>
                                            <a href="{{ route('customer.shipment.create',[$quote ,$service]) }}" type="button" class="btn btn-primary mt-2"> Book Now</a>

                                        <?php
                                            } else {
                                        ?>
                                            <div class="">
                                                <h4 class="m-0"><strong>₹ {{ $service->getPrice($quote->approx_weight) }}</strong></h4>
                                            </div>
                                            <a href="{{ route('register') }}" type="button" class="btn btn-primary mt-2"> Book Now </a>

                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div> <!-- / .card-body -->
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $('#quoteDataTable').DataTable(
        {
            autoWidth: true,
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ]
        });
    </script>
@endsection
