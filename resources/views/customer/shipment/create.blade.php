@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <form action="{{ route('customer.shipment.store',[$quote, $service]) }}" method="POST" id="shipment-form" class="shipment" enctype="multipart/form-data">
                @csrf
                @method("POST")
                <div class="row my-4">
                    <div class="col">
                        <h1 class="h2 page-title d-inline">Book Shipment</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Shipment Details</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <section class="row">
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="senderCountryD">Origin Country</label>
                                                <input type="text" class="form-control" id="senderCountryD" value="{{ $quote->pickup->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPincodeD">Origin Pincode</label>
                                                <input type="text" class="form-control" id="senderPincodeD" value="{{ $quote->pickup_pincode }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="receiverCountryD">Destination Country</label>
                                                <input type="text" class="form-control" id="receiverCountryD" value="{{ $quote->destination->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPincodeD">Destination Pincode</label>
                                                <input type="text" class="form-control" id="receiverPincodeD" value="{{ $quote->destination_pincode }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="service">Services</label>
                                                <input type="text" class="form-control" id="service" value="{{ $service->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="approxTotalWeight">Approx Total Weight(As Quoted)</label>
                                                <input type="text" class="form-control" id="approxTotalWeight" value="{{ $quote->approx_weight }} Kg" readonly>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-2">Sender Details</h2>
                                        <div>
                                            <label for="senderAddress"></label>
                                            <div class="form-row">
                                                <div class="col-md-6 col-sm-12 mb-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="exists" id="senderAddressExisting" name="senderAddress" {{$addresses->count() ? 'checked' : ''}}>
                                                        <label class="custom-control-label" for="senderAddressExisting">Select From Existing Address</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="new" id="senderAddressNew" name="senderAddress" {{$addresses->count() ? '' : 'checked'}}>
                                                        <label class="custom-control-label" for="senderAddressNew">Add New Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="existingSenAddress" class="existingSenAddress">
                                            <div class="mb-3">
                                                <label for="sender">Select Sender</label>

                                                <select class="form-control select2" id="sender" name="senderStoredAddress">
                                                    <?php
                                                        if ($addresses->count()) {
                                                            foreach ($addresses as $address) {
                                                                if ($address->country->id == $quote->pickup->id) {
                                                    ?>
                                                        <option value="{{$address->id}}">{{$address->firstname}}</option>
                                                    <?php
                                                                }
                                                            }
                                                        } else {
                                                    ?>
                                                        <option value="none-sender" disabled selected>No Saved Address</option>
                                                    <?php
                                                        }
                                                    ?>
                                                    </select>
                                                @error('sender')
                                                    <small class="text-danger">{{$errors->first('sender')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="senderDetails" class="senderDetails">
                                            <div class="mb-3">
                                                <label for="senderFirstname">First Name</label>
                                                <input name="senderFirstname" value="{{old('senderFirstname')}}" type="text" class="form-control" id="senderFirstname">
                                                @error('senderFirstname')
                                                    <small class="text-danger">{{$errors->first('senderFirstname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderLastname">Last Name</label>
                                                <input name="senderLastname" value="{{old('senderLastname')}}" type="text" class="form-control" id="senderLastname">
                                                @error('senderLastname')
                                                    <small class="text-danger">{{$errors->first('senderLastname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderEmail">Email ID</label>
                                                <input name="senderEmail" value="{{old('senderEmail')}}" type="email" class="form-control" id="senderEmail">
                                                @error('senderEmail')
                                                    <small class="text-danger">{{$errors->first('senderEmail')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPhone">Phone Number</label>
                                                <div class="form-row">
                                                    <div class="col-3">
                                                        <select class="form-control select2" id="simple-select2" name="senderCountryCode">
                                                            <?php
                                                                foreach ($countries as $country) {
                                                            ?>
                                                                <option value="{{$country->id}}" {{ $country->id == 102 ? 'selected' : '' ; }}> {{$country->code}} {{$country->dial_code}} </option>
                                                            <?php
                                                                }
                                                            ?>
                                                            @error('senderCountryCode')
                                                                <small class="text-danger">{{$errors->first('senderCountryCode')}}</small>
                                                            @enderror
                                                        </select>
                                                    </div>
                                                    <div class="col-9">
                                                        <input type="text" name="senderPhone" value="{{old('senderPhone')}}" class="form-control" id="senderPhone">
                                                        @error('senderPhone')
                                                            <small class="text-danger">{{$errors->first('senderPhone')}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderAddress1">Address Line 1</label>
                                                <input name="senderAddress1" value="{{old('senderAddress1')}}" type="text" class="form-control" id="senderAddress1">
                                                @error('senderAddress1')
                                                    <small class="text-danger">{{$errors->first('senderAddress1')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderAddress2">Address Line 2</label>
                                                <input name="senderAddress2" value="{{old('senderAddress2')}}" type="text" class="form-control" id="senderAddress2">
                                                @error('senderAddress2')
                                                    <small class="text-danger">{{$errors->first('senderAddress2')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderCity">City</label>
                                                <input name="senderCity" value="{{old('senderCity')}}" type="text" class="form-control" id="senderCity">
                                                @error('senderCity')
                                                    <small class="text-danger">{{$errors->first('senderCity')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPincode">Pincode</label>
                                                <input type="text" class="form-control" id="senderPincode" value="{{ $quote->pickup_pincode }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderState">State</label>
                                                <input name="senderState" value="{{old('senderState')}}" type="text" class="form-control" id="senderState">
                                                @error('senderState')
                                                    <small class="text-danger">{{$errors->first('senderState')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderCountry">Country</label>
                                                <input type="text" class="form-control" id="senderCountry" name="senderCountry" value="{{ $quote->pickup->name }}" readonly>
                                            </div>
                                            <div>
                                                <label for="senderAddressType">Address Type</label>
                                                <div class="form-row">
                                                    <div class="col-md-4 mb-2">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.RESIDENT")}}" id="senderAddressTypeResidential" name="senderAddressType" checked>
                                                            <label class="custom-control-label" for="senderAddressTypeResidential">Residential</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="senderAddressTypeCommercial" name="senderAddressType">
                                                            <label class="custom-control-label" for="senderAddressTypeCommercial">Commercial</label>
                                                        </div>
                                                    </div>
                                                    @error('senderAddressType')
                                                        <small class="text-danger">{{$errors->first('senderAddressType')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="mt-4">
                                            <label for="senderDoc">Upload Sender ID</label>
                                            <div class="row mb-2">
                                                <div class="col">
                                                    <input accept="image" id="senderDoc" name="senderDoc" type="file">
                                                </div>
                                            </div>
                                            <span class="small text-muted">Sender ID will be required for customs purpose (Aadhar Card, Passport Copy, Driving Licence. Anyone form of ID is required)</span>
                                        </div> --}}
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-2">Receiver Details</h2>
                                        <div>
                                            <label for="receiverAddress"></label>
                                            <div class="form-row">
                                                <div class="col-md-6 col-sm-12  mb-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="exists" id="receiverAddressExisting" name="receiverAddress" {{$addresses->count() ? 'checked' : ''}}>
                                                        <label class="custom-control-label" for="receiverAddressExisting">Select From Existing Address</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="new" id="receiverAddressNew" name="receiverAddress" {{$addresses->count() ? '' : 'checked'}}>
                                                        <label class="custom-control-label" for="receiverAddressNew">Add New Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="existingRecAddress" class="existingRecAddress">
                                            <div class="mb-3">
                                                <label for="receiver">Select Receiver</label>
                                                <select name="receiverStoredAddress" class="form-control select2" id="receiver">
                                                    <?php
                                                        if ($addresses->count()) {
                                                            foreach ($addresses as $address) {
                                                                if ($address->country->id == $quote->destination->id){
                                                    ?>
                                                        <option value="{{$address->id}}">{{$address->firstname}}</option>
                                                    <?php
                                                                }
                                                            }
                                                        } else {
                                                    ?>
                                                        <option value="none-receiver" disabled selected>No Saved Address</option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                                @error('receiver')
                                                    <small class="text-danger">{{$errors->first('receiver')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="receiverDetails" class="receiverDetails">
                                            <div class="mb-3">
                                                <label for="receiverFirstname">First Name</label>
                                                <input name="receiverFirstname" value="{{old('receiverFirstname')}}" type="text" class="form-control" id="receiverFirstname">
                                                @error('receiverFirstname')
                                                    <small class="text-danger">{{$errors->first('receiverFirstname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverLastname">Last Name</label>
                                                <input name="receiverLastname" value="{{old('receiverLastname')}}" type="text" class="form-control" id="receiverLastname">
                                                @error('receiverLastname')
                                                    <small class="text-danger">{{$errors->first('receiverLastname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverEmail">Email ID</label>
                                                <input name="receiverEmail" value="{{old('receiverEmail')}}" type="email" class="form-control" id="receiverEmail">
                                                @error('receiverEmail')
                                                    <small class="text-danger">{{$errors->first('receiverEmail')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPhone">Phone Number</label>
                                                <div class="form-row">
                                                    <div class="col-3">
                                                        <select class="form-control select2" id="simple-select2" name="receiverCountryCode">
                                                            <?php
                                                                foreach ($countries as $country) {
                                                            ?>
                                                                <option value="{{$country->id}}" {{ $country->id == 102 ? 'selected' : '' ; }}> {{$country->code}} {{$country->dial_code}} </option>
                                                            <?php
                                                                }
                                                            ?>
                                                            @error('receiverCountryCode')
                                                                <small class="text-danger">{{$errors->first('receiverCountryCode')}}</small>
                                                            @enderror
                                                        </select>
                                                    </div>
                                                    <div class="col-9">
                                                        <input type="text" name="receiverPhone" value="{{old('receiverPhone')}}" class="form-control" id="receiverPhone">
                                                        @error('receiverPhone')
                                                            <small class="text-danger">{{$errors->first('receiverPhone')}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddress1">Address Line 1</label>
                                                <input name="receiverAddress1" value="{{old('receiverAddress1')}}" type="text" class="form-control" id="receiverAddress1">
                                                @error('receiverAddress1')
                                                    <small class="text-danger">{{$errors->first('receiverAddress1')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddress2">Address Line 2</label>
                                                <input name="receiverAddress2" value="{{old('receiverAddress2')}}" type="text" class="form-control" id="receiverAddress2">
                                                @error('receiverAddress2')
                                                    <small class="text-danger">{{$errors->first('receiverAddress2')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverCity">City</label>
                                                <input name="receiverCity" value="{{old('receiverCity')}}" type="text" class="form-control" id="receiverCity">
                                                @error('receiverCity')
                                                    <small class="text-danger">{{$errors->first('receiverCity')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPincode">Pincode</label>
                                                <input type="text" class="form-control" id="receiverPincode" name="receiverPincode" value="{{ $quote->destination_pincode }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverState">State</label>
                                                <input name="receiverState" value="{{old('receiverState')}}" type="text" class="form-control" id="receiverState">
                                                @error('receiverState')
                                                    <small class="text-danger">{{$errors->first('receiverState')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverCountry">Country</label>
                                                <input name="receiverCountry" type="text" class="form-control" id="receiverCountry" value="{{ $quote->destination->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddressType">Address Type</label>
                                                <div class="row">
                                                    <div class="col-md-4 mb-2">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" id="receiverResidential" value="{{config("constants.RESIDENT")}}" name="receiverAddressType" checked>
                                                            <label class="custom-control-label" for="receiverResidential">Residential</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="receiverCommercial" name="receiverAddressType">
                                                            <label class="custom-control-label" for="receiverCommercial">Commercial</label>
                                                        </div>
                                                    </div>
                                                    @error('receiverAddressType')
                                                        <small class="text-danger">{{$errors->first('receiverAddressType')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="mt-4">
                                            <label for="receiverDoc">Upload Receiver ID</label>
                                            <div class="row mb-2">
                                                <div class="col">
                                                    <input accept="image" id="receiverDoc" name="receiverDoc" type="file">
                                                </div>
                                            </div>
                                            <span class="small text-muted">Receiver ID will be required for customs purposes (Passport Copy, Driving Licence. Anyone form of ID is required)</span>
                                        </div> --}}
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <h2 class="h3 mb-4">Packing Details</h2>
                                <div id="boxes" class="row">

                                </div>
                                <div>
                                    <input type="button" id="" class="btn btn-danger d-block mr-3" value="Add Box" onclick="addBox()" />
                                </div>
                                <div class="alert alert-danger text-center mt-3" role="alert">
                                    <span class="fe fe-pad fe-16 mr-2"></span>
                                    <strong>NOTE: </strong>For custom purpose sender's & recevier's ID is neccessary please email your ID's at <span><strong>support@dakyaa.com</strong></span>
                                    <br>
                                    <div>
                                        <strong>Sender ID - </strong>Aadhar Card, Pan Card, Passport Copy, Driving Licence (Anyone form of ID is required)
                                    </div>
                                    <div>
                                        <strong>Receiver ID  - </strong>Passport Copy, Driving Licence (Anyone form of ID is required)
                                    </div>
                                    {{-- Sender ID (Aadhar Card, Passport Copy, Driving Licence. Anyone form of ID is required)
                                    <br>
                                    Receiver ID (Passport Copy, Driving Licence. Anyone form of ID is required) --}}

                                </div>
                                <button type="submit" class="btn btn-primary" id="createShipmentBtn">Submit</button>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

            </form>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<div class="modal fade show" id="senderReceiverSameError" tabindex="-1" role="dialog" aria-labelledby="senderReceiverSameErrorTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title small m-3" id="senderReceiverSameErrorTitle"><strong class="text-danger">Sender And Receiver Cannot Be Same. Please Try Again.</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="emptySenderOrReceiver" tabindex="-1" role="dialog" aria-labelledby="emptySenderOrReceiverTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title small m-3" id="emptySenderOrReceiverTitle"><strong class="text-danger">Please Select Proper Sender & Receiver</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/customer/js/shipment/script.js') }}"></script>
    <script src="{{ asset('assets/customer/js/shipment/validate.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#shipment-form').on('submit', function(e){
                var sender = document.getElementById("sender");
                var senderValue = sender.value;

                var receiver = document.getElementById("receiver");
                var receiverValue = receiver.value;

                if(senderValue == "none-sender" || receiverValue == "none-receiver") {
                    $('#emptySenderOrReceiver').modal('show');
                    e.preventDefault();
                }

                if(senderValue == receiverValue) {
                    $('#senderReceiverSameError').modal('show');
                    e.preventDefault();
                }
            });
        });

        addBox();
    </script>
@endsection
