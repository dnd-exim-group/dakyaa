@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <form action="{{ route('shipments.update', $shipment) }}" method="POST" id="shipment-form" class="shipment" enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <div class="row my-4">
                    <div class="col">
                        <h1 class="h2 page-title d-inline">Book Shipment</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Shipment Details</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <section class="row">
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="senderCountryD">Origin Country</label>
                                                <input type="text" class="form-control" id="senderCountryD" value="{{ $shipment->quote->pickup->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPincodeD">Origin Pincode</label>
                                                <input type="text" class="form-control" id="senderPincodeD" value="{{ $shipment->quote->pickup_pincode }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="receiverCountryD">Destination Country</label>
                                                <input type="text" class="form-control" id="receiverCountryD" value="{{ $shipment->quote->destination->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPincodeD">Destination Pincode</label>
                                                <input type="text" class="form-control" id="receiverPincodeD" value="{{ $shipment->quote->destination_pincode }}" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="service">Services</label>
                                                <input type="text" class="form-control" id="service" value="{{ $shipment->service->name }}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="approxTotalWeight">Approx Total Weight(As Quoted)</label>
                                                <input type="text" class="form-control" id="approxTotalWeight" value="{{ $quote->approx_weight }} Kg" readonly>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-2">Sender Details</h2>
                                        <div>
                                            <label for="senderAddress"></label>
                                            <div class="form-row">
                                                <div class="col-md-6 col-sm-12 mb-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="exists" id="senderAddressExisting" name="senderAddress" {{$addresses->count() ? 'checked' : ''}}>
                                                        <label class="custom-control-label" for="senderAddressExisting">Select From Existing Address</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="new" id="senderAddressNew" name="senderAddress" {{$addresses->count() ? '' : 'checked'}}>
                                                        <label class="custom-control-label" for="senderAddressNew">Add New Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="existingSenAddress" class="existingSenAddress">
                                            <div class="mb-3">
                                                <label for="sender">Select Sender</label>
                                                <select class="form-control select2" id="sender" name="senderStoredAddress">
                                                    <?php
                                                        if ($addresses->count()) {
                                                            foreach ($addresses as $address) {
                                                                if ($address->country->id == $quote->pickup->id) {
                                                    ?>
                                                            <option value="{{$address->id}}" {{$address->id == $shipment->senderAddress->id ? 'selected' : ''}}>{{$address->firstname .' '. $address->lastname}}</option>
                                                    <?php
                                                                }
                                                            }
                                                        } else {
                                                    ?>
                                                        <option value="none-sender" disabled selected>No Saved Address</option>
                                                    <?php
                                                        }
                                                    ?>
                                                    </select>
                                                @error('sender')
                                                    <small class="text-danger">{{$errors->first('sender')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="senderDetails" class="senderDetails">
                                            <div class="mb-3">
                                                <label for="senderFirstname">First Name</label>
                                                <input name="senderFirstname" value="{{$shipment->senderAddress->firstname}}" type="text" class="form-control" id="senderFirstname">
                                                @error('senderFirstname')
                                                    <small class="text-danger">{{$errors->first('senderFirstname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderLastname">Last Name</label>
                                                <input name="senderLastname" value="{{$shipment->senderAddress->lastname}}" type="text" class="form-control" id="senderLastname">
                                                @error('senderLastname')
                                                    <small class="text-danger">{{$errors->first('senderLastname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderEmail">Email ID</label>
                                                <input name="senderEmail" value="{{$shipment->senderAddress->email}}" type="email" class="form-control" id="senderEmail">
                                                @error('senderEmail')
                                                    <small class="text-danger">{{$errors->first('senderEmail')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPhone">Phone Number</label>
                                                <div class="form-row">
                                                    <div class="col-3">
                                                        <select class="form-control select2" id="simple-select2" name="senderCountryCode">
                                                            <?php
                                                                foreach ($countries as $country) {
                                                            ?>
                                                                <option value="{{$country->id}}" {{$shipment->senderAddress->country->id == $country->id ? 'selected':''}}> {{$country->code}} {{$country->dial_code}} </option>
                                                            <?php
                                                                }
                                                            ?>
                                                            @error('senderCountryCode')
                                                                <small class="text-danger">{{$errors->first('senderCountryCode')}}</small>
                                                            @enderror
                                                        </select>
                                                    </div>
                                                    <div class="col-9">
                                                        <input type="text" name="senderPhone" value="{{$shipment->senderAddress->phone_no}}" class="form-control" id="senderPhone">
                                                        @error('senderPhone')
                                                            <small class="text-danger">{{$errors->first('senderPhone')}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderAddress1">Address Line 1</label>
                                                <input name="senderAddress1" value="{{$shipment->senderAddress->address_line1}}" type="text" class="form-control" id="senderAddress1">
                                                @error('senderAddress1')
                                                    <small class="text-danger">{{$errors->first('senderAddress1')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderAddress2">Address Line 2</label>
                                                <input name="senderAddress2" value="{{$shipment->senderAddress->address_line2}}" type="text" class="form-control" id="senderAddress2">
                                                @error('senderAddress2')
                                                    <small class="text-danger">{{$errors->first('senderAddress2')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderCity">City</label>
                                                <input name="senderCity" value="{{$shipment->senderAddress->city}}" type="text" class="form-control" id="senderCity">
                                                @error('senderCity')
                                                    <small class="text-danger">{{$errors->first('senderCity')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderPincode">Pincode</label>
                                                <input type="text" class="form-control" id="senderPincode" value="{{$shipment->senderAddress->pincode}}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderState">State</label>
                                                <input name="senderState" value="{{$shipment->senderAddress->state_country}}" type="text" class="form-control" id="senderState">
                                                @error('senderState')
                                                    <small class="text-danger">{{$errors->first('senderState')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="senderCountry">Country</label>
                                                <input type="text" class="form-control" id="senderCountry" name="senderCountry" value="{{$shipment->senderAddress->country->name}}" readonly>
                                            </div>
                                            <div>
                                                <label for="senderAddressType">Address Type</label>
                                                <div class="form-row">
                                                    <div class="col-md-4 mb-2">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.RESIDENT")}}" id="senderAddressTypeResidential" name="senderAddressType" {{config("constants.RESIDENT") == $shipment->senderAddress->address_type ? 'checked':''}}>
                                                            <label class="custom-control-label" for="senderAddressTypeResidential">Residential</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="senderAddressTypeCommercial" name="senderAddressType" {{config("constants.COMMERCIAL") == $shipment->senderAddress->address_type ? 'checked':''}}>
                                                            <label class="custom-control-label" for="senderAddressTypeCommercial">Commercial</label>
                                                        </div>
                                                    </div>
                                                    @error('senderAddressType')
                                                        <small class="text-danger">{{$errors->first('senderAddressType')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="mt-4">
                                            <label for="senderDoc">Upload Sender ID</label>
                                            <div class="row mb-2">
                                                <div class="col">
                                                    <input accept="image" id="senderDoc" name="senderDoc" type="file">
                                                </div>
                                            </div>
                                            <span class="small text-muted">Sender ID will be required for customs purpose (Aadhar Card, Passport Copy, Driving Licence. Anyone form of ID is required)</span>
                                        </div> --}}
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <div class="row">
                                    <div class="col">
                                        <h2 class="h3 mb-3">Receiver Details</h2>
                                        <div>
                                            <label for="receiverAddress"></label>
                                            <div class="form-row">
                                                <div class="col-md-6 col-sm-12  mb-2">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" value="exists" id="receiverAddressExisting" name="receiverAddress" checked>
                                                        <label class="custom-control-label" for="receiverAddressExisting">Select From Existing Address</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="custom-control custom-radio mb-3">
                                                        <input type="radio" class="custom-control-input" value="new" id="receiverAddressNew" name="receiverAddress">
                                                        <label class="custom-control-label" for="receiverAddressNew">Add New Address</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="existingRecAddress" class="existingRecAddress">
                                            <div class="mb-3">
                                                <label for="receiver">Select Receiver</label>
                                                <select name="receiverStoredAddress" class="form-control select2" id="receiver">
                                                    <?php
                                                        if ($addresses->count()) {
                                                            foreach ($addresses as $address) {
                                                                if ($address->country->id == $quote->destination->id){
                                                    ?>
                                                        <option value="{{$address->id}}" {{$address->id == $shipment->receiverAddress->id ? 'selected' : ''}}>{{$address->firstname .' '. $address->lastname}}</option>
                                                    <?php
                                                                 }
                                                            }
                                                        } else {
                                                    ?>
                                                        <option value="none-receiver" disabled selected>No Saved Address</option>
                                                    <?php
                                                        }
                                                    ?>
                                                </select>
                                                @error('receiver')
                                                    <small class="text-danger">{{$errors->first('receiver')}}</small>
                                                @enderror
                                            </div>
                                        </div>
                                        <div id="receiverDetails" class="receiverDetails">
                                            <div class="mb-3">
                                                <label for="receiverFirstname">First Name</label>
                                                <input name="receiverFirstname" value="{{$shipment->receiverAddress->firstname}}" type="text" class="form-control" id="receiverFirstname">
                                                @error('receiverFirstname')
                                                    <small class="text-danger">{{$errors->first('receiverFirstname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverLastname">Last Name</label>
                                                <input name="receiverLastname" value="{{$shipment->receiverAddress->lastname}}" type="text" class="form-control" id="receiverLastname">
                                                @error('receiverLastname')
                                                    <small class="text-danger">{{$errors->first('receiverLastname')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverEmail">Email ID</label>
                                                <input name="receiverEmail" value="{{$shipment->receiverAddress->email}}" type="email" class="form-control" id="receiverEmail">
                                                @error('receiverEmail')
                                                    <small class="text-danger">{{$errors->first('receiverEmail')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPhone">Phone Number</label>
                                                <div class="form-row">
                                                    <div class="col-3">
                                                        <select class="form-control select2" id="simple-select2" name="receiverCountryCode">
                                                            <?php
                                                                foreach ($countries as $country) {
                                                            ?>
                                                                <option value="{{$country->id}}"  {{$shipment->senderAddress->country->id == $country->id ? 'selected':''}}> {{$country->code}} {{$country->dial_code}} </option>
                                                            <?php
                                                                }
                                                            ?>
                                                            @error('receiverCountryCode')
                                                                <small class="text-danger">{{$errors->first('receiverCountryCode')}}</small>
                                                            @enderror
                                                        </select>
                                                    </div>
                                                    <div class="col-9">
                                                        <input type="text" name="receiverPhone" value="{{$shipment->receiverAddress->phone_no}}" class="form-control" id="receiverPhone">
                                                        @error('receiverPhone')
                                                            <small class="text-danger">{{$errors->first('receiverPhone')}}</small>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddress1">Address Line 1</label>
                                                <input name="receiverAddress1" value="{{$shipment->receiverAddress->address_line1}}" type="text" class="form-control" id="receiverAddress1">
                                                @error('receiverAddress1')
                                                    <small class="text-danger">{{$errors->first('receiverAddress1')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddress2">Address Line 2</label>
                                                <input name="receiverAddress2" value="{{$shipment->receiverAddress->address_line2}}" type="text" class="form-control" id="receiverAddress2">
                                                @error('receiverAddress2')
                                                    <small class="text-danger">{{$errors->first('receiverAddress2')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverCity">City</label>
                                                <input name="receiverCity" value="{{$shipment->receiverAddress->city}}" type="text" class="form-control" id="receiverCity">
                                                @error('receiverCity')
                                                    <small class="text-danger">{{$errors->first('receiverCity')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverPincode">Pincode</label>
                                                <input type="text" class="form-control" id="receiverPincode" name="" value="{{$shipment->receiverAddress->pincode}}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverState">State</label>
                                                <input name="receiverState" value="{{$shipment->receiverAddress->state_country}}" type="text" class="form-control" id="receiverState">
                                                @error('receiverState')
                                                    <small class="text-danger">{{$errors->first('receiverState')}}</small>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverCountry">Country</label>
                                                <input name="receiverCountry" type="text" class="form-control" id="receiverCountry" value="{{$shipment->receiverAddress->country->name}}" readonly>
                                            </div>
                                            <div class="mb-3">
                                                <label for="receiverAddressType">Address Type</label>
                                                <div class="row">
                                                    <div class="col-md-4 mb-2">
                                                        <div class="custom-control custom-radio">
                                                            <input type="radio" class="custom-control-input" id="receiverResidential" value="{{config("constants.RESIDENT")}}" name="receiverAddressType" {{config("constants.RESIDENT") == $shipment->receiverAddress->address_type ? 'checked':''}}>
                                                            <label class="custom-control-label" for="receiverResidential">Residential</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-radio mb-3">
                                                            <input type="radio" class="custom-control-input" value="{{config("constants.COMMERCIAL")}}" id="receiverCommercial" name="receiverAddressType" {{config("constants.RESIDENT") == $shipment->receiverAddress->address_type ? 'checked':''}}>
                                                            <label class="custom-control-label" for="receiverCommercial">Commercial</label>
                                                        </div>
                                                    </div>
                                                    @error('receiverAddressType')
                                                        <small class="text-danger">{{$errors->first('receiverAddressType')}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="mt-4">
                                            <label for="receiverDoc">Upload Receiver ID</label>
                                            <div class="row mb-2">
                                                <div class="col">
                                                    <input accept="image" id="receiverDoc" name="receiverDoc" type="file">
                                                </div>
                                            </div>
                                            <span class="small text-muted">Receiver ID will be required for customs purposes (Passport Copy, Driving Licence. Anyone form of ID is required)</span>
                                        </div> --}}
                                    </div>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-body m-3">
                                <h2 class="h3 mb-4">Packing Details</h2>
                                <div id="boxes" class="row">
                                <?php
                                    $i = 1;
                                    foreach ($shipment->boxes as $box) {
                                ?>
                                    <div id="box_1" class="col-md-12">
                                        <input type="hidden"  name="box_no[]" value="{{$i}}">
                                        <h4 class="mr-3 d-inline">Box {{$i}}</h4>
                                        @if($i != 1)
                                            <button class="btn btn-sm btn-danger d-inline mb-2" id="box_btn_{{$i}}" onclick="removeBox('box_btn_{{$i}}')"><i class="fe fe-trash-2"></i></button>
                                        @endif
                                        <div class="row mt-3 mb-3">
                                            <div class="col-md-6 col-sm-12">
                                                <div>
                                                    <label>Dimensions (Length x Width x Height)</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Length</span>
                                                        </div>
                                                        <input name="length[]" value="{{$box->length}}" type="number" class="calculate form-control" id="length_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Height</span>
                                                        </div>
                                                        <input name="height[]" value="{{$box->height}}" type="number" class="calculate form-control" id="height_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Width</span>
                                                        </div>
                                                        <input name="width[]" value="{{$box->width}}" type="number" class="calculate form-control" id="width_{{$i}}" min="0" required>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">cm</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="mb-2">
                                                    <label for="volumeWeight_{{$i}}">Volume Weight</label>
                                                    <input name="" value="{{($box->length * $box->width * $box->height) / 5000}}" type="number" class="form-control" id="volumeWeight_{{$i}}" readonly>
                                                </div>
                                                <div class="">
                                                    <label for="weight_{{$i}}">Actual Weight</label>
                                                    <input name="weight[]" value="{{$box->weight}}" type="number" class="form-control" id="weight_{{$i}}" min="0.1" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <!-- table -->
                                                <table id="itemTable_{{$i}}" class="table table-bordered text-center">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th>Item</th>
                                                            <th>Quantity</th>
                                                            <th>Value</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="itemTableBody_{{$i}}">
                                                        <?php
                                                            $j = 1;
                                                            foreach ($box->items as $item) {
                                                        ?>
                                                            <tr>
                                                                <td><input type="text" class="form-control" name="itemName_{{$i}}[]" value="{{$item->name}}" required></td>
                                                                <td><input class="form-control" type="number" min="1" name="itemQuantity_{{$i}}[]" value="{{$item->count}}" required></td>
                                                                <td><input class="form-control" type="number" min="1" name="value_{{$i}}[]" value="{{$item->value}}" required></td>
                                                                <td>
                                                                    <button id="button_{{$i}}_{{$j}}" name="button_{{$i}}_{{$j}}" class="btn btn-danger" onclick="removeRow('button_{{$i}}_{{$j}}')">
                                                                        <i class="fe fe-trash-2"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                            $j++;
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <input type="button" class="btn btn-primary mb-4 float-right" value="Add New Item" onclick="addRow('itemTableBody_{{$i}}', {{$i}})" />
                                            </div>
                                        </div>
                                    </div>
                                <?php
                                        $i++;
                                    }
                                ?>
                                </div>
                                <div>
                                    <input type="button" id="" class="btn btn-danger float-right d-block mr-3" value="Add Box" onclick="addBox()" />
                                </div>
                                <div class="alert alert-danger text-center mt-3" role="alert">
                                    <span class="fe fe-pad fe-16 mr-2"></span>
                                    <strong>NOTE: </strong>For custom purpose sender's & recevier's ID is neccessary please email your ID's at <span><strong>support@dakyaa.com</strong></span>
                                    <br>
                                    <div>
                                        <strong>Sender ID - </strong>Aadhar Card, Pan Card, Passport Copy, Driving Licence (Anyone form of ID is required)
                                    </div>
                                    <div>
                                        <strong>Receiver ID  - </strong>Passport Copy, Driving Licence (Anyone form of ID is required)
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>
            </form>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<div class="modal fade show" id="senderReceiverSameError" tabindex="-1" role="dialog" aria-labelledby="senderReceiverSameErrorTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title small m-3" id="senderReceiverSameErrorTitle"><strong class="text-danger">Sender And Receiver Cannot Be Same. Please Try Again.</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="emptySenderOrReceiver" tabindex="-1" role="dialog" aria-labelledby="emptySenderOrReceiverTitle" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title small m-3" id="emptySenderOrReceiverTitle"><strong class="text-danger">Please Select Proper Sender & Receiver</strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/customer/js/shipment/script.js') }}"></script>
    <script src="{{ asset('assets/customer/js/shipment/validate.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#shipment-form').on('submit', function(e){
                var sender = document.getElementById("sender");
                var senderValue = sender.value;

                var receiver = document.getElementById("receiver");
                var receiverValue = receiver.value;

                if(senderValue == "none-sender" || receiverValue == "none-receiver") {
                    $('#emptySenderOrReceiver').modal('show');
                    e.preventDefault();
                }

                if(senderValue == receiverValue) {
                    $('#senderReceiverSameError').modal('show');
                    e.preventDefault();
                }
            });
        });

        addBox();
    </script>
@endsection
