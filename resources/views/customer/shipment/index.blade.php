@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <h1 class="h2 page-title d-inline">My Shipments</h1>
            <div class="row my-4">
                <?php
                    foreach ($shipments as $shipment) {
                        // dd($shipment->status);
                        // $shipment->status == config("constants.SHIPMENT_APPROVED") ? 'approved' : 'rejected';
                        // dd(config("constants.SHIPMENT_PENDING"));
                ?>
                <div class="col-md-4">
                    <div class="card shadow mb-4 {{ $shipment->status == config("constants.SHIPMENT_PENDING") ? 'pending' : ($shipment->status == config("constants.SHIPMENT_APPROVED") ? 'approved' : 'rejected') }}">
                        <div class="card-header">
                            <h5 class="mb-3 card-title d-inline">{{ $shipment->senderAddress->country->name.' - '.$shipment->receiverAddress->country->name }}</h5>
                            <div class="d-inline float-right">
                                {{-- <a class="text-danger" href="#" data-toggle="modal" data-target="#cancelShipmentModal"><i class="fe fe-x-circle"></i></a> --}}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <p class="mb-1">Sender: <strong>{{ $shipment->senderAddress->firstname.' '.$shipment->senderAddress->lastname }}</strong></p>
                                    <p class="mb-1">Receiver: <strong>{{ $shipment->receiverAddress->firstname.' '.$shipment->receiverAddress->lastname }}</strong></p>
                                    <p class="mb-1">Origin: <strong>{{ $shipment->senderAddress->country->name.' - '.$shipment->senderAddress->pincode }}</strong></p>
                                    <p class="mb-1">Destination: <strong>{{ $shipment->receiverAddress->country->name.' - '.$shipment->receiverAddress->pincode }}</strong></p>
                                    <p class="mb-1">Service: <strong>{{ $shipment->service->name }}</strong></p>
                                    <p class="mb-1">Approx Weight:
                                        <strong>
                                            <?php
                                                $boxes = $shipment->boxes;
                                                $approxWeight = 0;
                                                foreach ($boxes as $box) {
                                                    $approxWeight += $box->weight;
                                                }
                                            ?>
                                            {{ $approxWeight }}
                                        </strong>
                                    </p>
                                    <p class="mb-0">Created On: <strong>{{ $shipment->created_at->format('F j, Y') }}</strong></p>
                                    <p class="mb-0">
                                        Status:
                                        <strong>
                                            <?php
                                                if ($shipment->tracking)
                                                {
                                            ?>
                                                    {{ $shipment->tracking->lastStatus()->name }}
                                            <?php
                                                } else {
                                            ?>
                                                    {{ "NA" }}
                                            <?php
                                                }
                                            ?>
                                        </strong>
                                    </p>
                                </div>
                            </div>
                        </div> <!-- / .card-body -->
                        <div class="card-footer py-3">
                            <div class="row align-items-center float-right">
                                <div class="col">
                                    <a href="{{ route('shipments.show', $shipment) }}" type="button" class="btn btn-sm btn-primary mr-2"><i class="fe fe-eye"></i> View</a>
                                    <?php
                                        if($shipment->status == config("constants.SHIPMENT_APPROVED") && $shipment->tracking) {
                                    ?>
                                            <a href="{{ route('customer.shipment.track', $shipment) }}" type="button" class="btn btn-sm btn-info mr-2"><i class="fe fe-map-pin"></i> Track</a>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        if($shipment->status == config("constants.SHIPMENT_PENDING") || $shipment->status == config("constants.SHIPMENT_REJECTED")) {
                                    ?>
                                            <a href="" type="button" class="btn btn-sm btn-warning text-white mr-2"><i class="fe fe-edit"></i> Edit</a>
                                    <?php
                                        }
                                    ?>
                                    <?php
                                        if($shipment->status == config("constants.SHIPMENT_PENDING") || $shipment->status == config("constants.SHIPMENT_APPROVED")) {
                                    ?>
                                            <a href="" type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#cancelShipmentModal"><i class="fe fe-x-circle"></i> Cancel</a>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
                {{-- <div class="col-md-4">
                    <div class="card shadow mb-4 approved">
                        <div class="card-header">
                            <h5 class="mb-3 card-title d-inline">INDIA - UK</h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <p class="mb-1">Sender: <strong>Milind Parab</strong></p>
                                    <p class="mb-1">Receiver: <strong>Prem Parab</strong></p>
                                    <p class="mb-1">Origin: <strong>India - 400604</strong></p>
                                    <p class="mb-1">Destination: <strong>UK - DA1 4FR</strong></p>
                                    <p class="mb-1">Service: <strong>GR DPD</strong></p>
                                    <p class="mb-1">Approx Weight: <strong>36.5 Kg</strong></p>
                                    <p class="mb-0">Created On: <strong>02/08/2021</strong></p>
                                    <p class="mb-0">Status: <strong>In-Transit</strong></p>
                                </div>
                            </div>
                        </div> <!-- / .card-body -->
                        <div class="card-footer py-3">
                            <div class="row align-items-center float-right">
                                <div class="col">
                                    <a href="" type="button" class="btn btn-danger mr-2" data-toggle="modal" data-target="#cancelShipmentModal"><i class="fe fe-x-circle"></i> Cancel</a>

                                    <a href="{{ route('customer.shipment.track', $shipment) }}" type="button" class="btn btn-primary mr-2"><i class="fe fe-map-pin"></i> Track</a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card shadow mb-4 rejected">
                        <div class="card-header">
                            <h5 class="mb-3 card-title d-inline">INDIA - UK</h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col">
                                    <p class="mb-1">Sender: <strong>Milind Parab</strong></p>
                                    <p class="mb-1">Receiver: <strong>Prem Parab</strong></p>
                                    <p class="mb-1">Origin: <strong>India - 400604</strong></p>
                                    <p class="mb-1">Destination: <strong>UK - DA1 4FR</strong></p>
                                    <p class="mb-1">Service: <strong>GR DPD</strong></p>
                                    <p class="mb-1">Approx Weight: <strong>36.5 Kg</strong></p>
                                    <p class="mb-0">Created On: <strong>02/08/2021</strong></p>
                                    <p class="mb-0">Status: <strong>In-Transit</strong></p>
                                </div>
                            </div>
                        </div> <!-- / .card-body -->
                        <div class="card-footer py-3">
                            <div class="row align-items-center float-right">
                                <div class="col">
                                    <a href="" type="button" class="btn btn-warning text-white"><i class="fe fe-edit"></i> Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<div class="modal fade" id="cancelShipmentModal" tabindex="-1" role="dialog" aria-labelledby="cancelShipmentModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancelShipmentModalLabel">Cancel Shipment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">
                {{-- <p>Are you sure you want to cancel shipment?</p>
                <div class="form-group mb-3">
                    <textarea name="reason" id="reason" rows="3" class="form-control" placeholder="Please Specify Your Reason" required></textarea>
                </div>
                <span class="text-muted">Note: If you are facing any issue please mail us on</span> it@dndeximltd.co.uk --}}
                {{-- <p>Are you sure you want to cancel shipment?</p> --}}
                <div class="form-group mb-3">
                    <p> Note: To cancel the shipment mail your shipment details and reason at <strong>support@dakyaa.com</strong></p>
                </div>

            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn mb-2 btn-danger" data-dismiss="modal">Yes</button>
                <button type="button" class="btn mb-2 btn-secondary">No</button>
            </div> --}}
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $('#quoteDataTable').DataTable(
        {
            autoWidth: true,
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ]
        });
    </script>
@endsection
