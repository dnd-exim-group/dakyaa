@extends('layouts.customer')

@section('styles')
<!-- DataTables CSS -->
<link rel="stylesheet" href="{{ asset('assets/admin/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
                <div class="row my-2">
                    <div class="col">
                        <a class="btn text-primary mb-2" href="{{route('shipments.index')}}"> <i class="fe fe-24 fe-arrow-left-circle" style="vertical-align: middle"></i></a>
                        <h1 class="h2 page-title d-inline">Shipment</h1>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Shipment Details</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <section class="row">
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="small text-muted">Sender</label>
                                                <p class="large mb-1"> <i class="fe fe-12 fe-user mr-2"></i><strong>{{ $shipment->senderAddress->firstname.' '.$shipment->senderAddress->lastname }}</strong></p>
                                                {{-- <p class="mb-2"><strong>Sender</strong></p>
                                                <h5 class="mb-1">{{ $shipment->senderAddress->firstname.' '.$shipment->senderAddress->lastname }}</h5> --}}
                                                <p class="mb-1"> <i class="fe fe-12 fe-phone mr-2"></i>{{ $shipment->senderAddress->countryCode->dial_code.' '.$shipment->senderAddress->phone_no }}</p>
                                                <p class="mb-1"> <i class="fe fe-12 fe-mail mr-2"></i>{{ $shipment->senderAddress->email }}</p>
                                                <p class="mb-1"><i class="fe fe-12 fe-home mr-2"></i>{{ $shipment->senderAddress->address_line1.' '.$shipment->senderAddress->address_line2.', '.$shipment->senderAddress->city.', '.$shipment->senderAddress->state_country.', '.$shipment->senderAddress->country->name.' - '. $shipment->senderAddress->pincode }} ({{ $shipment->senderAddress->address_type==config("constants.RESIDENT") ? "Residential" : "Commercial" }})</p>
                                                {{-- <p class="mb-1">{{  }}</p>
                                                <p class="mb-1">{{ $shipment->senderAddress->city.' - '. $shipment->senderAddress->pincode }}</p>
                                                <p class="mb-1">{{ $shipment->senderAddress->state_country.', '.$shipment->senderAddress->country->name }}</p>
                                                <p class="mb-1">{{ $shipment->senderAddress->address_type==config("constants.RESIDENT") ? "Residential" : "Commercial" }}</p> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="small text-muted">Receiver</label>
                                                <p class="large mb-1"><i class="fe fe-12 fe-user mr-2"></i><strong>{{ $shipment->receiverAddress->firstname.' '.$shipment->receiverAddress->lastname }}</strong></p>
                                                {{-- <p class="mb-2"><strong>Receiver</strong></p>
                                                <h5 class="mb-1">{{ $shipment->receiverAddress->firstname.' '.$shipment->receiverAddress->lastname }}</h5> --}}
                                                <p class="large mb-1"><i class="fe fe-12 fe-phone mr-2"></i>{{ $shipment->receiverAddress->countryCode->dial_code.' '.$shipment->receiverAddress->phone_no }}</p>
                                                <p class="mb-1"><i class="fe fe-12 fe-mail mr-2"></i>{{ $shipment->receiverAddress->email }}</p>
                                                <p class="mb-1"><i class="fe fe-12 fe-home mr-2"></i>{{ $shipment->receiverAddress->address_line1.' '.$shipment->receiverAddress->address_line2.', '.$shipment->receiverAddress->city.', '.$shipment->receiverAddress->state_country.', '.$shipment->receiverAddress->country->name.' - '. $shipment->receiverAddress->pincode }} ({{ $shipment->receiverAddress->address_type==config("constants.RESIDENT") ? "Residential" : "Commercial" }})</p>
                                                {{-- <p class="mb-1">{{ $shipment->receiverAddress->address_line2 }}</p>
                                                <p class="mb-1">{{ $shipment->receiverAddress->city.' - '. $shipment->receiverAddress->pincode }}</p>
                                                <p class="mb-1">{{ $shipment->receiverAddress->state_country.', '.$shipment->receiverAddress->country->name }}</p>
                                                <p class="mb-1">({{ $shipment->receiverAddress->address_type==config("constants.RESIDENT") ? "Residential" : "Commercial" }})</p> --}}
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Booking Date</label>
                                                <p class="large"><i class="fe fe-12 fe-calendar mr-2"></i><strong>{{ $shipment->created_at->format('F j, Y') }}</strong></p>
                                            </div>
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Services</label>
                                                <p class="large"><i class="fe fe-12 fe-layers mr-2"></i><strong>{{ $shipment->service->name }}</strong></p>
                                                {{-- <p class="mb-2"><strong>Services</strong></p>
                                                <h5 class="mb-1">{{ $shipment->service->name }}</h5> --}}
                                            </div>
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Approx Weight</label>
                                                <p class="large"><i class="fe fe-12 fe-shopping-bag mr-2"></i>
                                                    <?php
                                                        $boxes = $shipment->boxes;
                                                        $weight = 0;
                                                        foreach ($boxes as $box) {
                                                            $weight += $box->weight;
                                                        }
                                                    ?>
                                                    <strong>{{$weight}} Kg</strong>
                                                </p>

                                                {{-- <p class="mb-2"><strong>Approx Weight</strong></p>
                                                <h5 class="mb-1">
                                                    <?php
                                                        // $boxes = $shipment->boxes;
                                                        // $weight = 0;
                                                        // foreach ($boxes as $box) {
                                                        //     $weight += $box->weight;
                                                        // }
                                                    ?>
                                                        {{ $weight }} Kg
                                                </h5> --}}
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-4">
                            <div class="card-header">
                                <strong class="card-title ml-1">Package Details</strong>
                            </div>
                            <div class="card-body mr-3 ml-3">
                                <div>
                                    <?php
                                        $i = 1;
                                        foreach ($shipment->boxes as $box) {
                                    ?>
                                    <section class="row mb-4">
                                        <div class="col-md-12">
                                            <h5 class="mb-2"><i class="fe fe-package mr-2"></i>Box {{ $i }} </h5>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Dimensions (Units in cm)</label>
                                                <p class="large"><i class="fe fe-12 fe-box mr-2"></i><strong>{{ $box->length.' x '.$box->width.' x '.$box->height }}</strong> <small class="text-muted">(Length x Width x Height)</small></p>
                                                {{-- <p class="mb-2"><strong>Dimensions (Units in cm)</strong></p>
                                                <h5 class="mb-1">
                                                    {{ $box->length.' x '.$box->width.' x '.$box->height }}
                                                    <small class="text-muted">(Length x Width x Height)</small>
                                                </h5> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Volume Weight</label>
                                                <p class="large"><i class="fe fe-12 fe-shopping-bag mr-2"></i><strong>{{ ($box->length * $box->width * $box->height)/5000 }} Kg</strong></p>
                                                {{-- <p class="mb-2"><strong>Volume Weight</strong></p>
                                                <h5 class="mb-1">
                                                    {{ ($box->length * $box->width * $box->height)/5000 }} Kg
                                                </h5> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mb-3">
                                                <label for="" class="mr-2 small text-muted">Actual Weight</label>
                                                <p class="large"><i class="fe fe-12 fe-shopping-bag mr-2"></i><strong>{{ $box->weight }} Kg</strong></p>
                                                {{-- <p class="mb-2"><strong>Actual Weight</strong></p>
                                                <h5 class="mb-1"> {{ $box->weight }} Kg </h5> --}}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="table table-bordered mb-0">
                                                <thead>
                                                    <tr>
                                                        <th>Item Name</th>
                                                        <th>Qty</th>
                                                        <th>Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        $j = 1;
                                                        foreach ($box->items as $item) {
                                                    ?>
                                                        <tr>
                                                            <td> {{ $item->name }} </td>
                                                            <td> {{ $item->count }} </td>
                                                            <td> {{ $item->value }} </td>
                                                        </tr>
                                                    <?php
                                                        $j++;
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </section>
                                    <?php
                                            $i++;
                                        }
                                    ?>
                                </div>
                            </div> <!-- ./card-text -->
                        </div>
                    </div>
                </div>

        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

<div class="modal fade" id="cancelShipmentModal" tabindex="-1" role="dialog" aria-labelledby="cancelShipmentModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancelShipmentModalLabel">Cancel Shipment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <p>Are you sure you want to cancel shipment?</p>
                <div class="form-group mb-3">
                    <textarea name="reason" id="reason" rows="3" class="form-control" placeholder="Please Specify Your Reason" required></textarea>
                </div>
                <span class="text-muted">Note: If you are facing any issue please mail us on</span> it@dndeximltd.co.uk
            </div>
            <div class="modal-footer">
                <button type="button" class="btn mb-2 btn-danger" data-dismiss="modal">Yes</button>
                <button type="button" class="btn mb-2 btn-secondary">No</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
    <script src="{{ asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/customer/js/shipment/script.js') }}"></script>
    <script>
        $('#quoteDataTable').DataTable(
        {
            autoWidth: true,
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ]
        });
    </script>
@endsection
