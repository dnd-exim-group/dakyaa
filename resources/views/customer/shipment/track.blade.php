@extends('layouts.customer')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row my-4">
                {{-- <div class="col text-center">
                    <h1 class="h2 page-title">Your Shipment Details</h1>
                </div> --}}
                <div class="col">
                    <a class="btn text-primary mb-2" href="{{route('shipments.index')}}"> <i class="fe fe-24 fe-arrow-left-circle" style="vertical-align: middle"></i></a>
                    <h1 class="h2 page-title d-inline">Your Tracking Details</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card shadow mb-4">
                        <div class="card-header">
                            <h4 class="d-inline-block m-0"><strong class="card-title">Details</strong></h4>
                        </div>
                        <div class="card-body pb-1 mx-3">
                            {{-- <h2 class="h4 mb-3">Details</h2> --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Customer Name</label>
                                        <strong>
                                            <p class="large">{{ $shipment->user->firstname.' '.$shipment->user->lastname }}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Sender Name</label>
                                        <strong>
                                            <p class="large">{{ $shipment->senderAddress->firstname.' '.$shipment->senderAddress->lastname }}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Collection Address</label>
                                        <strong>
                                            <p class="large">{{ $shipment->senderAddress->address_line1.', '.$shipment->senderAddress->address_line2.', '.$shipment->senderAddress->city.', '.$shipment->senderAddress->state_country.', '.$shipment->senderAddress->country->name.' - '.$shipment->senderAddress->pincode }}</p>
                                        </strong>
                                    </div>


                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Service</label>
                                        <strong>
                                            <p class="large">{{ $shipment->service->name }}</p>
                                        </strong>
                                    </div>

                                    <div>
                                        <label for="" class="mr-2 small text-muted">Receiver Name</label>
                                        <strong>
                                            <p class="large">{{ $shipment->receiverAddress->firstname.' '.$shipment->receiverAddress->lastname }}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Delivery Address</label>
                                        <strong>
                                            <p class="large">{{ $shipment->receiverAddress->address_line1.', '.$shipment->receiverAddress->address_line2.', '.$shipment->receiverAddress->city.', '.$shipment->receiverAddress->state_country.', '.$shipment->receiverAddress->country->name.' - '.$shipment->receiverAddress->pincode }}</p>
                                        </strong>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Approx Weight</label>
                                        <strong>
                                            <p class="large">
                                                <?php
                                                    $boxes = $shipment->boxes;
                                                    $approxWeight = 0;
                                                    foreach ($boxes as $box) {
                                                        $approxWeight += $box->weight;
                                                    }
                                                ?>
                                                {{ $approxWeight }} Kg
                                            </p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Booking Date</label>
                                        <strong>
                                            <p class="large">{{ $shipment->created_at->format('F j, Y') }}</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Shipment Status</label>
                                        <strong>
                                            <p class="large">{{ $shipment->tracking->lastStatus()->name }}</p>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- ./card-text -->
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="card timeline">
                        <div class="card-header">
                            <h4 class="d-inline-block m-0"><strong class="card-title">Tracking History</strong></h4>
                        </div>
                        <div class="card-body">
                            <?php
                                $statuses = $shipment->tracking->statuses;
                                foreach ($statuses as $status) {
                            ?>
                            <div class="pb-4 timeline-item item-success">
                                <div class="pl-5">
                                    <div class="mb-2">
                                        <strong>{{ $status->pivot->created_at->format('D F j, Y') }}</strong>
                                    </div>
                                    <p class="mb-2 large"><strong>{{ $status->name }} <span class="badge badge-light">{{ ($status->pivot->created_at)->diffForHumans(); }}</span></strong></p>
                                    <p> {{ $status->pivot->note }} </p>
                                </div>
                            </div>
                            <?php
                                }
                            ?>
                        </div> <!-- / .card-body -->
                    </div> <!-- / .card -->
                </div> <!-- / .col-md-6 -->
            </div>

        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection
