@extends('layouts.customer')

@section('content')

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 text-center">
            <h1 class="h5 page-title mb-4">Track Your Shipment</h1>

            <form id="track-shipment" action="" method="POST">
                @csrf
                @method("GET")
                <div class="form-row">
                    <div class="col">
                        <label for="lastname">Enter AWB Number</label>
                        <input type="text" class="form-control mb-3 mx-auto" id="awbno" onkeypress="return onlyNumberKey(event)" required>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection

@section('scripts')

<script>
    function onlyNumberKey(evt) {
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
</script>

@endsection
