@extends('layouts.customer')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="row my-4">
                <div class="col text-center">
                    <h1 class="h2 page-title">Your Shipment Details</h1>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card shadow mb-4">
                        <div class="card-body pb-1 m-3">
                            <h2 class="h3 mb-3">Details</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Customer Name</label>
                                        <strong>
                                            <p class="large">Raj Panchal</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Vendor</label>
                                        <strong>
                                            <p class="large">DND Express</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Service</label>
                                        <strong>
                                            <p class="large">Dubai Fedex</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Approx Weight</label>
                                        <strong>
                                            <p class="large">23 kg</p>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Sender Name</label>
                                        <strong>
                                            <p class="large">Raj Panchal</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Collection Address</label>
                                        <strong>
                                            <p class="large">Kisan Nagar 3, Wagle Estate, Thane - 400604, Maharashtra, India</p>
                                        </strong>
                                    </div>

                                    <div>
                                        <label for="" class="mr-2 small text-muted">Receiver Name</label>
                                        <strong>
                                            <p class="large">Jack</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Delivery Address</label>
                                        <strong>
                                            <p class="large">Alcock Crescent, Crayford, 84, London, London DA1 4FR, United Kingdom</p>
                                        </strong>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Booking Date</label>
                                        <strong>
                                            <p class="large">08 Jun 2021</p>
                                        </strong>
                                    </div>
                                    <div>
                                        <label for="" class="mr-2 small text-muted">Shipment Status</label>
                                        <strong>
                                            <p class="large">In-Transit</p>
                                        </strong>
                                    </div>
                                </div>
                            </div>                            
                        </div> <!-- ./card-text -->
                    </div>
                </div>
                <div class="col-md-12 mb-4">
                    <div class="card timeline">
                        <div class="card-header">
                            <h4 class="d-inline-block m-0"><strong class="card-title">Tracking History</strong></h4>
                        </div>
                        <div class="card-body">
                            <div class="pb-4 timeline-item item-success">
                                <div class="pl-5">
                                    <div class="mb-2">
                                        <strong>04:12pm June 10, 2021</strong>
                                    </div>
                                    <p class="mb-2 large"><strong>Booking In Process <span class="badge badge-light">5 days ago</span></strong></p>
                                    <div class="text-muted">
                                        Some note for customer
                                    </div>
                                </div>
                            </div>
                            <div class="pb-4 timeline-item item-success">
                                <div class="pl-5">
                                    <div class="mb-2">
                                        <strong>11:42am June 11, 2021</strong>
                                    </div>
                                    <p class="mb-2 large"><strong>Out For Pickup<span class="badge badge-light">4 days ago</span></strong></p>
                                    <div class="text-muted">
                                        Some note for customer
                                    </div>
                                </div>
                            </div>
                            <div class="pb-4 timeline-item item-success">
                                <div class="pl-5">
                                    <div class="mb-2">
                                        <strong>01:24pm June 13, 2021</strong>
                                    </div>
                                    <p class="mb-2 large"><strong>In-Transit <span class="badge badge-light">3 days ago</span></strong></p>
                                    <div class="text-muted">
                                        Some note for customer
                                    </div>
                                </div>
                            </div>
                        </div> <!-- / .card-body -->
                    </div> <!-- / .card -->
                </div> <!-- / .col-md-6 -->
            </div>
            
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->

@endsection
