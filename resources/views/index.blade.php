<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dakyaa</title>

        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet"> --}}
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        {{-- <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

        <link href="{{ asset('assets/global/css/feather.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/global/css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/css/media.css') }}" rel="stylesheet">

        <style>
            html {
                scroll-behavior: smooth;

            }
            html,
            body {
                height: 100%;
            }
            body {
                font-family: 'Poppins', sans-serif;
                background-color: #F1FAEE;
            }
        </style>

    </head>
    <body id="main" class="">
        <div id="app">
            <nav id="header" class="header navbar navbar-expand-lg navbar-light py-3 fixed-header shadow-sm">
                <div class="logoDiv">
                    <span class="logo ml-2">Dakyaa.com</span>
                </div>
                <div>
                    <a id="menu-btn" class="btn mr-auto" onclick="toggleMenu()">
                        <span class="navbar-toggler-icon"></span>
                    </a>
                </div>

                <div id="mobile-menu" class="navbar shadow" style="left: -50%;">
                    <div class="py-2">
                        <a class="btn btn-close-menu ml-auto" onclick="toggleMenu()"><i class="fe fe-x fe-24"></i></a>
                    </div>
                    <ul class="navbar-nav py-2 pl-3">
                        <li class="nav-item active">
                            <a class="nav-link" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#process">Process</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#track">Track</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>

                <div class="menubar mr-2">
                    <div id="menu" class="">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link mr-4" href="#home">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mr-4" href="#about">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mr-4" href="#process">Process</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mr-4" href="#track">Track</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mr-4" href="#contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                    @if (Route::has('login'))
                        <div class="auth-btns">
                            @auth
                                <a href="{{ url('/home') }}" class="btn btn-login ml-3 text-sm text-gray-700">
                                    Dashboard
                                    {{-- <span class="avatar mt-2">
                                        <img src="{{ asset('assets/images/avatars/face-1.jpg') }}" alt="..." width="32px" class="avatar-img rounded-circle">
                                    </span> --}}
                                </a>
                            @else
                                <a href="{{ route('login') }}" class="btn btn-login ml-3 text-sm text-gray-700">Log in</a>

                                @if (Route::has('register'))
                                    <a href="{{ route('register') }}" class="btn btn-register ml-2 text-sm text-gray-700">Register</a>
                                @endif
                            @endauth
                        </div>
                    @endif
                </div>
            </nav>

            <section id="home">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="left">
                                <h1 class="mb-4">Dakyaa.com</h1>
                                <p class="tagline">Your Local International Postman</p>
                                {{-- <form action="{{ route('shipment.track') }}" method="GET">
                                    @csrf
                                    @method("GET")
                                    <div class="input-group shadow mb-5">
                                        <input type="text" name="tracking_number" class="form-control-sm form-control form-control-lg" id="tracking-field" placeholder="Enter tracking no">
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-track" type="submit" >
                                                <i class="fe fe-map-pin mr-1 vertical-align"></i> Track
                                            </button>
                                        </div>
                                    </div>
                                </form> --}}
                                {{-- <div class="track">
                                    <input type="text" class="form-control">
                                    <button type="button" class="btn btn-track">Track</button>
                                </div> --}}
                                <button type="button" class="btn btn-get-quote shadow" data-toggle="modal" data-target="#getAQuote"> Get A Quote </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="vector-part">
                                <img src="{{asset('assets/images/dakyaa1.png')}}" alt="" width="120%">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="mx-3">
                                <h2 class="heading mb-4">About Us</h2>
                                <div class="text">
                                    <p><span class="brand-name">"Dakyaa.com"</span> Your local international postman, is an Aggregator (Comparison) website for International courier services offering a wide range of selection of All major worldwide brands like DND Express, DHL Express, Aramex, TNT / FedEx to choose from.</p>

                                    <p>The word Dakyaa is an Ancient word that means Postman who collects your Parcel and delivers it to your loved one all over the world.</p>

                                    <p>Dakyaa.com is a One-Stop-Shop for all your International courier service-related requirements. Send Documents, General Household items, medicines, Homemade snacks and sweets, Non-Perishable Food items like Spices, Lentils, etc, Books, Clothes, Toys, Kitchen utensils, and Commercial items goods.</p>

                                    <p>If you are Student, Relocating, Sending Gifts, Home-based Entrepreneur, or Business, Dakyaa.com is the right place to choose a courier partner for sending your parcel abroad, If you wish to send One Single envelope or multiple boxes, Dakyaa.com can offer a wide range of options to choose from!</p>
                                </div>

                                <button type="button" class="btn btn-get-quote shadow mt-4" data-toggle="modal" data-target="#getAQuote"> Get A Quote </button>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="vector-part">
                                <img src="{{asset('assets/images/homer.png')}}" alt="" width="100%">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="process">
                <div class="container">
                    <h2 class="heading mb-5">Process</h2>
                    <div class="row mb-3">
                        <div class="col-md-6 col-sm-12 mb-3">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h2 class="steps d-inline">01 </h2> <span class="process-name">Get A Quote</span>
                                    <p class="text-justify tagline">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad adipisci facere deserunt labore dicta rem sunt at earum aliquam saepe dolores alias ipsa, nisi voluptatem praesentium, sequi odit. Sint</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h2 class="steps d-inline">02 </h2> <span class="process-name">Choose Service</span>
                                    <p class="text-justify tagline">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad adipisci facere deserunt labore dicta rem sunt at earum aliquam saepe dolores alias ipsa, nisi voluptatem praesentium, sequi odit. Sint</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12 mb-3">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h2 class="steps d-inline">03 </h2> <span class="process-name">Book Now</span>
                                    <p class="text-justify tagline">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad adipisci facere deserunt labore dicta rem sunt at earum aliquam saepe dolores alias ipsa, nisi voluptatem praesentium, sequi odit. Sint</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="card shadow">
                                <div class="card-body">
                                    <h2 class="steps d-inline">04 </h2> <span class="process-name">Payment</span>
                                    <p class="text-justify tagline">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ad adipisci facere deserunt labore dicta rem sunt at earum aliquam saepe dolores alias ipsa, nisi voluptatem praesentium, sequi odit. Sint</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="track">
                <div class="container">
                    <div class="row">
                        <div class="col-6 mx-auto text-center">
                            <h2 class="heading mb-5">Track</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mx-auto">
                            <form action="{{ route('shipment.track') }}" method="GET">
                                @csrf
                                @method("GET")
                                <div class="input-group shadow mb-5">
                                    <input type="text" name="tracking_number" class="form-control-sm form-control form-control-lg" id="tracking-field" placeholder="Enter tracking no">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-track" type="submit" >
                                            <i class="fe fe-map-pin mr-1 vertical-align"></i> Track
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

            <footer id="contact">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="mx-2">
                                <h2 class="heading">Reach Us</h2>
                                <p class="text my-4"><i class="fe fe-mail mr-2"></i> support@dakyaa.com</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="footer-social-icons mx-2">
                                <h4 class="_14">Follow us on</h4>
                                <ul class="social-icons">
                                    <li><a href="" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                                    <li><a href="" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                                    <li><a href="" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                                    <li><a href="" class="social-icon"> <i class="fa fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center">
                                <p class="my-4">© 2021 DAKYAA. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

    </body>

    <!-- Generate Quote Modal -->
    <div class="modal fade" id="getAQuote" tabindex="-1" role="dialog" aria-labelledby="getAQuoteLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{route('quotes.store')}}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="modal-header">
                        <h5 class="modal-title" id="getAQuoteLabel">Generate Quote</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="firstname" class="small text-muted">Firstname</label>
                                <input type="text" name="firstname" class="form-control form-control-sm" id="firstname" type="text">
                                @error('senderPin')
                                    <small class="text-danger">{{$errors->first('senderPin')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="lastname" class="small text-muted">Lastname</label>
                                <input type="text" name="lastname" class="form-control form-control-sm" id="lastname">
                                @error('lastname')
                                    <small class="text-danger">{{$errors->first('lastname')}}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="email" class="small text-muted">Email</label>
                                <input type="email" name="email" class="form-control form-control-sm" id="email">
                                @error('senderPin')
                                    <small class="text-danger">{{$errors->first('senderPin')}}</small>
                                @enderror
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="phone" class="small text-muted">Phone Number</label>
                                <div class="form-row">
                                    <div class="col-4">
                                        <select class="form-control form-control-sm select2" id="country_id" name="country_id">
                                            <?php
                                                foreach ($countries as $country) {
                                                    if(Str::lower($country->name) == "india") {
                                            ?>
                                                        <option value="{{$country->id}}" {{ $country->id == 102 ? 'selected' : '' ; }}> {{$country->dial_code.' '.$country->code}}</option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                            @error('country_id')
                                                <small class="text-danger">{{$errors->first('country_id')}}</small>
                                            @enderror
                                        </select>
                                    </div>
                                    <div class="col-8">
                                        <input type="text" name="phone" value="{{old('phone')}}" class="form-control form-control-sm" id="phone">
                                        @error('phone')
                                            <small class="text-danger">{{$errors->first('phone')}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="lastname" class="small text-muted">Collection</label>
                                <div class="form-row">
                                    <div class="col">
                                        <div id="phone-number" class="form-group">
                                            <select name="senderCountry" class="form-control form-control-sm" id="senderCountry" data-error="Please select one option.">
                                                <?php
                                                    foreach ($countries as $country) {
                                                        if(Str::lower($country->name) == "india") {
                                                ?>
                                                            <option data-countrycode="" value="{{$country->id}}">{{ $country->name}}</option>
                                                <?php
                                                        } elseif (Str::lower($country->name) == "united kingdom") {
                                                ?>
                                                    <option data-countrycode="" value="{{$country->id}}">{{ $country->name}}</option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            @error('senderCountry')
                                                <small class="text-danger">{{$errors->first('senderCountry')}}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <input type="text" name="senderPin" class="form-control form-control-sm" id="senderPin" placeholder="Collection Pincode*" type="text" value="" data-error="Collection Pincode*">
                                        @error('senderPin')
                                            <small class="text-danger">{{$errors->first('senderPin')}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="lastname" class="small text-muted">Delivery</label>
                                <div class="form-row">
                                    <div class="col">
                                        <div id="phone-number" class="form-group">
                                            <select name="receiverCountry" class="form-control form-control-sm" id="receiverCountry" data-error="Please select one option.">
                                                <?php
                                                    foreach ($countries as $country) {
                                                        if(!(Str::lower($country->name) == "india")) {
                                                ?>
                                                            <option data-countrycode="" value="{{$country->id}}">{{ $country->name }}</option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                            </select>
                                            @error('receiverCountry')
                                                <small class="text-danger">{{$errors->first('receiverCountry')}}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col mb-3">
                                        <input type="text" name="receiverPin" class="form-control form-control-sm" id="receiverPin" placeholder="Delivery Pincode*" type="text" value="" data-error="Delivery Pincode*">
                                        @error('receiverPin')
                                            <small class="text-danger">{{$errors->first('receiverPin')}}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                <div class="form-group mb-2">
                                    <label for="approxweight" class="small text-muted">Approx Weight</label>
                                    <input type="number" name="weight" id="approxweight" class="form-control form-control-sm" placeholder="Approx Weight*" min="0" step="any">
                                    @error('weight')
                                        <small class="text-danger">{{$errors->first('weight')}}</small>
                                    @enderror
                                </div>
                            </div>
                            <div class="col">
                                <label class="small text-muted">Shipment Type</label>
                                <div class="form-row">
                                    <div class="col-md-4">
                                        <div class="custom-control custom-radio mb-2">
                                            <input type="radio" class="custom-control-input" id="shipmentType1" name="shipmentType" value="{{config('constants.PERSONAL_SHIPMENT')}}" checked>
                                            <label class="custom-control-label" for="shipmentType1">Personal/Gift</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-radio mb-2">
                                            <input type="radio" class="custom-control-input" id="shipmentType2" name="shipmentType" value="{{config('constants.COMMERCIAL_SHIPMENT')}}">
                                            <label class="custom-control-label" for="shipmentType2">Commercial</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="shipmentType3" name="shipmentType" value="{{config('constants.OTHERS_SHIPMENT')}}">
                                            <label class="custom-control-label" for="shipmentType3">Others</label>
                                        </div>
                                    </div>
                                    @error('shipmentType')
                                        <small class="text-danger">{{$errors->first('shipmentType')}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                        {{-- <button type="submit" class="btn btn-primary">Get Quote</button> --}}
                        <button type="submit" class="btn btn-sm btn-get-quote shadow mx-auto"> Get Quote </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <script src="{{ asset('assets/global/js/custom.js') }}"></script>

</html>
