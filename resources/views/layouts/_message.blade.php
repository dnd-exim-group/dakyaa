@if(session()->has('success') || session()->has('error'))
    <div class="toast {{ session()->has('success') ? 'success' : (session()->has('error') ? 'danger' : 'primary')}} fade show ml-auto p-2" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header {{ session()->has('success') ? 'success' : (session()->has('error') ? 'danger' : 'primary')}}">
            <strong class="mr-auto">Message</strong>
            <button type="button" class="ml-2 mb-1 close" onclick="$('.toast').hide();"  aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="toast-body"> {{ session()->has('success') ? session()->get('success') : (session()->has('error') ? session()->get('error') : 'Please Try Again')}} </div>
    </div>
@endif
