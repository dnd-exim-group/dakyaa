<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{ asset('assets/icons/favicon.ico') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Overpass:ital,wght@0,100;0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Styles -->

    <link href="{{ asset('assets/global/css/feather.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/css/simplebar.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/admin/css/app-light.css') }}" rel="stylesheet" id="lightTheme">
    <link href="{{ asset('assets/admin/css/app-dark.css') }}" rel="stylesheet" id="darkTheme" disabled>

    <link href="{{ asset('assets/admin/css/custom.css') }}" rel="stylesheet">
    @yield('styles')

</head>
<body class="vertical">
    <div id="app" class="wrapper">
        <nav class="topnav navbar navbar-light">
            <button type="button" class="navbar-toggler text-muted mt-2 p-0 mr-3 collapseSidebar">
                <i class="fe fe-menu navbar-toggler-icon"></i>
            </button>
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link text-muted my-2" href="#" id="modeSwitcher" data-mode="light">
                      <i class="fe fe-sun fe-16"></i>
                    </a>
                  </li>
                <li class="nav-item">
                    <a class="nav-link text-muted my-2" href="./#" data-toggle="modal" data-target=".modal-shortcut">
                        <span class="fe fe-grid fe-16"></span>
                    </a>
                </li>
                <li class="nav-item nav-notif">
                    <a class="nav-link text-muted my-2" href="./#" data-toggle="modal" data-target=".modal-notif">
                        <span class="fe fe-bell fe-16"></span>
                        <span class="dot dot-md bg-success"></span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted pr-0" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="avatar avatar-sm mt-2">
                            <img src="{{ asset('assets/images/avatars/face-1.jpg') }}" alt="..." class="avatar-img rounded-circle">
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Settings</a>
                        <a class="dropdown-item" href="#">Activities</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                        <!-- <a class="dropdown-item" href="#">Logout</a> -->
                    </div>
                </li>
            </ul>
        </nav>

        <aside class="sidebar-left border-right bg-white shadow" id="leftSidebar" data-simplebar>
            <a href="#" class="btn collapseSidebar toggle-btn d-lg-none text-muted ml-2 mt-3" data-toggle="toggle">
                <i class="fe fe-x"><span class="sr-only"></span></i>
            </a>
            <nav class="vertnav navbar navbar-light">
                <!-- nav bar -->
                <div class="w-100 mt-3 mb-4 d-flex">
                    <a class="navbar-brand mx-auto mt-2 flex-fill text-center" href="{{route('admin')}}">
                        <svg id="logo" class="navbar-brand-img" width="50" height="50" viewBox="0 0 154 200" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M124.938 107C124.938 132.407 104.352 153 78.9618 153C53.5718 153 32.9858 132.407 32.9858 107C32.9858 81.5931 53.5718 61 78.9618 61C104.352 61 124.938 81.5931 124.938 107ZM128.938 107C128.938 134.614 106.563 157 78.9618 157C51.3608 157 28.9858 134.614 28.9858 107C28.9858 79.3858 51.3608 57 78.9618 57C106.563 57 128.938 79.3858 128.938 107ZM122.94 107C122.94 131.301 103.25 151 78.9618 151C54.6736 151 34.9834 131.301 34.9834 107C34.9834 82.699 54.6736 63 78.9618 63C103.25 63 122.94 82.699 122.94 107ZM123.94 107C123.94 131.853 103.803 152 78.9618 152C54.1209 152 33.9834 131.853 33.9834 107C33.9834 82.1472 54.1209 62 78.9618 62C103.803 62 123.94 82.1472 123.94 107Z" fill="black"/>
                            <path d="M15.9922 106L78.682 102L143.931 106L78.682 110L15.9922 106Z" fill="black"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M32.106 58.1213L37.2345 67.495L45.8476 72.055L76.52 108.213L113.401 139.732L118.196 148.495L127.202 153.263L122.436 144.252L113.678 139.456L82.1742 102.557L46.0327 71.869L41.4751 63.2524L32.106 58.1213Z" fill="black"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M117.833 64.2524L127.202 59.1213L122.074 68.495L113.429 73.0718L82.6797 109.305L45.8949 140.754L41.1127 149.495L32.106 154.263L36.8721 145.252L45.5486 140.501L77.0255 103.648L113.289 72.8436L117.833 64.2524Z" fill="black"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M79.9615 26L73.9644 51L78.8046 71.1775L75.9633 106L78.7084 141.017L74.9644 156L79.962 175L84.9595 156L81.2147 141.013L83.9595 106L81.1182 71.1778L85.9586 51L79.9615 26Z" fill="black"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M84.1367 69.2918L85.2481 62.989L86.2329 63.1627L85.1215 69.4654L84.1367 69.2918ZM86.8517 69.8464L88.4256 63.0256L89.4 63.2507L87.8261 70.0715L86.8517 69.8464ZM91.5426 64.0552L89.7834 70.6237L90.7493 70.8826L92.5084 64.3141L91.5426 64.0552ZM92.5075 71.5373L94.8322 65.147L95.7719 65.4891L93.4471 71.8794L92.5075 71.5373ZM98.1678 66.2905L95.1004 72.5827L95.9991 73.0212L99.0665 66.7291L98.1678 66.2905ZM98.0833 74.1217L101.382 68.4053L102.248 68.9055L98.9492 74.6219L98.0833 74.1217ZM104.638 70.5693L100.854 75.9757L101.673 76.5495L105.457 71.1431L104.638 70.5693ZM103.446 78.0411L107.879 72.7554L108.645 73.3983L104.211 78.6841L103.446 78.0411ZM85.4321 144.537L86.8025 150.789L87.7793 150.574L86.4088 144.323L85.4321 144.537ZM88.1218 143.871L89.9764 150.62L90.9406 150.355L89.086 143.605L88.1218 143.871ZM93.0481 149.463L91.0188 142.973L91.9732 142.674L94.0024 149.164L93.0481 149.463ZM93.7028 141.947L96.2898 148.236L97.2145 147.855L94.6275 141.566L93.7028 141.947ZM99.5752 146.955L96.2502 140.795L97.13 140.32L100.455 146.48L99.5752 146.955ZM99.1669 139.134L102.699 144.709L103.544 144.174L100.011 138.599L99.1669 139.134ZM105.863 142.412L101.859 137.167L102.654 136.56L106.658 141.805L105.863 142.412ZM104.363 134.996L109.01 140.094L109.749 139.42L105.101 134.322L104.363 134.996ZM113.248 78.7971L108.204 82.7327L108.819 83.5215L113.863 79.5858L113.248 78.7971ZM115.583 80.9536L109.849 84.9635L110.421 85.7834L116.156 81.7735L115.583 80.9536ZM111.505 87.5053L117.209 83.8069L117.752 84.6463L112.049 88.3447L111.505 87.5053ZM118.92 86.822L112.916 90.009L113.385 90.8926L119.389 87.7056L118.92 86.822ZM114.142 92.5229L119.942 90L120.341 90.9173L114.541 93.4402L114.142 92.5229ZM121.603 93.6306L115.327 95.6645L115.635 96.6161L121.911 94.5822L121.603 93.6306ZM116.142 98.8983L122.572 97.4194L122.796 98.3942L116.366 99.8731L116.142 98.8983ZM123.514 101.214L116.684 102.168L116.822 103.159L123.653 102.205L123.514 101.214ZM116.588 109.461L122.934 110.269L122.808 111.261L116.462 110.453L116.588 109.461ZM116.164 112.201L123.049 113.447L122.871 114.431L115.986 113.185L116.164 112.201ZM122.17 116.611L115.528 115.168L115.316 116.145L121.958 117.588L122.17 116.611ZM114.746 117.934L121.237 119.95L120.94 120.905L114.449 118.889L114.746 117.934ZM119.942 123L113.827 120.574L113.459 121.504L119.574 123.93L119.942 123ZM112.59 123.817L119.165 127.206L118.707 128.095L112.132 124.706L112.59 123.817ZM116.863 130.736L110.777 126.893L110.244 127.739L116.329 131.582L116.863 130.736ZM109.015 129.356L114.265 133.352L113.66 134.148L108.41 130.152L109.015 129.356Z" fill="black"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M74.4985 69.3028L73.3872 63L72.4024 63.1736L73.5137 69.4764L74.4985 69.3028ZM71.7839 69.8573L70.21 63.0366L69.2356 63.2616L70.8096 70.0824L71.7839 69.8573ZM67.0931 64.0661L68.8522 70.6346L67.8863 70.8936L66.1272 64.3251L67.0931 64.0661ZM66.1281 71.5482L63.8034 65.1579L62.8637 65.5001L65.1885 71.8904L66.1281 71.5482ZM60.4678 66.3015L63.5352 72.5936L62.6365 73.0322L59.5691 66.74L60.4678 66.3015ZM60.5524 74.1327L57.2536 68.4162L56.3876 68.9164L59.6864 74.6329L60.5524 74.1327ZM53.9976 70.5803L57.7813 75.9867L56.9623 76.5605L53.1785 71.154L53.9976 70.5803ZM55.19 78.052L50.7569 72.7663L49.991 73.4093L54.4241 78.695L55.19 78.052ZM75.2026 145.548L73.8315 151.8L72.8548 151.586L74.2259 145.334L75.2026 145.548ZM72.2478 144.846L70.6581 150.632L69.6939 150.366L71.2836 144.581L72.2478 144.846ZM66.9676 149.917L68.8775 143.809L67.9232 143.51L66.0133 149.619L66.9676 149.917ZM65.9322 142.958L63.3452 149.247L62.4206 148.866L65.0075 142.577L65.9322 142.958ZM60.2037 147.139L63.1521 141.667L62.272 141.193L59.3235 146.664L60.2037 147.139ZM60.4681 140.145L56.9357 145.72L56.0912 145.185L59.6236 139.609L60.4681 140.145ZM53.772 143.423L57.776 138.178L56.9814 137.571L52.9774 142.816L53.772 143.423ZM55.2724 136.007L50.6245 141.105L49.8859 140.431L54.5337 135.333L55.2724 136.007ZM45.3872 77.8081L50.9046 82.1127L50.2897 82.9015L44.7724 78.5968L45.3872 77.8081ZM43.052 80.9646L48.7869 84.9744L48.2142 85.7943L42.4793 81.7844L43.052 80.9646ZM47.1304 87.5162L41.4269 83.8178L40.8831 84.6572L46.5867 88.3556L47.1304 87.5162ZM39.7151 86.833L45.7191 90.02L45.2505 90.9036L39.2465 87.7166L39.7151 86.833ZM44.1103 92.802L37.6938 90.011L37.2952 90.9283L43.7116 93.7193L44.1103 92.802ZM37.0329 93.6416L43.3087 95.6754L43.0006 96.627L36.7248 94.5931L37.0329 93.6416ZM42.4931 98.9093L36.064 97.4304L35.84 98.4052L42.2691 99.8841L42.4931 98.9093ZM35.1211 101.225L41.9515 102.179L41.8133 103.17L34.9828 102.216L35.1211 101.225ZM41.6425 109.396L34.7019 110.28L34.8283 111.272L41.7689 110.388L41.6425 109.396ZM41.4715 112.211L34.5866 113.457L34.7648 114.442L41.6497 113.195L41.4715 112.211ZM35.466 116.622L42.1079 115.178L42.3203 116.156L35.6784 117.599L35.466 116.622ZM42.8889 117.943L36.3982 119.961L36.6948 120.916L43.1855 118.899L42.8889 117.943ZM37.6938 123.011L43.8085 120.585L44.177 121.515L38.0622 123.941L37.6938 123.011ZM45.6899 124.011L39.4704 127.217L39.9282 128.106L46.1478 124.9L45.6899 124.011ZM41.7724 130.747L47.5199 127.118L48.0535 127.963L42.306 131.593L41.7724 130.747ZM49.6206 130.367L44.3702 134.363L44.9755 135.159L50.2259 131.163L49.6206 130.367Z" fill="black"/>
                            <path d="M75.2637 12.7998C75.2637 12.0296 75.2552 11.3229 75.2383 10.6797C75.2298 10.0365 75.2171 9.47786 75.2002 9.00391C75.1917 8.52148 75.179 8.14062 75.1621 7.86133C75.1536 7.58203 75.1494 7.41699 75.1494 7.36621C74.9801 7.24772 74.7855 7.18424 74.5654 7.17578C74.3538 7.16732 74.1423 7.16309 73.9307 7.16309C73.6344 7.16309 73.279 7.23503 72.8643 7.37891C72.4495 7.51432 72.0475 7.71322 71.6582 7.97559C71.2689 8.22949 70.9388 8.54688 70.668 8.92773C70.3971 9.30013 70.2617 9.71908 70.2617 10.1846C70.2617 10.4554 70.2956 10.6712 70.3633 10.832C70.4395 10.9928 70.5241 11.1156 70.6172 11.2002C70.7188 11.2764 70.8161 11.3271 70.9092 11.3525C71.0107 11.3695 71.0827 11.3779 71.125 11.3779C71.3366 11.3779 71.5101 11.3441 71.6455 11.2764C71.7809 11.2002 71.9036 11.124 72.0137 11.0479C72.1237 10.9632 72.2253 10.887 72.3184 10.8193C72.4199 10.7432 72.5342 10.7051 72.6611 10.7051C72.7627 10.7051 72.8685 10.7305 72.9785 10.7812C73.097 10.8236 73.2028 10.8955 73.2959 10.9971C73.3975 11.0986 73.4821 11.234 73.5498 11.4033C73.6175 11.5641 73.6514 11.763 73.6514 12C73.6514 12.3385 73.5837 12.6305 73.4482 12.876C73.3213 13.1214 73.152 13.3288 72.9404 13.498C72.7373 13.6589 72.5003 13.7816 72.2295 13.8662C71.9671 13.9424 71.7048 13.9805 71.4424 13.9805C71.0361 13.9805 70.668 13.9128 70.3379 13.7773C70.0163 13.6335 69.7412 13.4388 69.5127 13.1934C69.2842 12.9479 69.1107 12.6602 68.9922 12.3301C68.8737 12 68.8145 11.6488 68.8145 11.2764C68.8145 10.3962 68.9964 9.63021 69.3604 8.97852C69.7327 8.31836 70.2236 7.76823 70.833 7.32812C71.4508 6.88802 72.1576 6.56217 72.9531 6.35059C73.7572 6.13053 74.5866 6.02051 75.4414 6.02051C76.2116 6.02051 76.8464 6.07129 77.3457 6.17285C77.8451 6.27441 78.2513 6.40983 78.5645 6.5791C78.8776 6.74837 79.123 6.9515 79.3008 7.18848C79.4785 7.41699 79.6309 7.6582 79.7578 7.91211C80.2741 8.96159 80.7734 9.99414 81.2559 11.0098C81.7467 12.0254 82.2165 13.0156 82.665 13.9805C83.1221 14.9453 83.5622 15.8763 83.9854 16.7734C84.417 17.6621 84.8359 18.5042 85.2422 19.2998V17.4844V13.5615C85.2422 12.859 85.2591 12.1735 85.293 11.5049C85.3353 10.8278 85.4157 10.193 85.5342 9.60059C85.6611 8.99967 85.8473 8.44954 86.0928 7.9502C86.3382 7.45085 86.6641 7.01921 87.0703 6.65527C87.485 6.29134 87.9971 6.00781 88.6064 5.80469C89.2158 5.60156 89.9479 5.5 90.8027 5.5C91.2344 5.5 91.6322 5.53385 91.9961 5.60156C92.36 5.66081 92.6732 5.74967 92.9355 5.86816C93.2064 5.98665 93.418 6.13477 93.5703 6.3125C93.7227 6.48177 93.7988 6.6722 93.7988 6.88379C93.7988 7.01921 93.7777 7.16309 93.7354 7.31543C93.7015 7.45931 93.6465 7.59049 93.5703 7.70898C93.4941 7.82747 93.3968 7.92904 93.2783 8.01367C93.1683 8.08984 93.0371 8.12793 92.8848 8.12793C92.7578 8.12793 92.6393 8.03906 92.5293 7.86133C92.4277 7.68359 92.2965 7.48893 92.1357 7.27734C91.9834 7.05729 91.776 6.8584 91.5137 6.68066C91.2598 6.50293 90.9128 6.41406 90.4727 6.41406C89.7871 6.41406 89.2158 6.53678 88.7588 6.78223C88.3102 7.01921 87.9463 7.35775 87.667 7.79785C87.3877 8.23796 87.1803 8.77539 87.0449 9.41016C86.918 10.0365 86.8376 10.7347 86.8037 11.5049C86.7868 12.0381 86.7699 12.5586 86.7529 13.0664C86.7445 13.5658 86.736 14.0736 86.7275 14.5898C86.7275 15.1061 86.7275 15.6436 86.7275 16.2021C86.7275 16.7523 86.7275 17.3447 86.7275 17.9795C86.7275 18.8766 86.7318 19.6807 86.7402 20.3916C86.7487 21.1025 86.7614 21.6865 86.7783 22.1436C87.1592 22.7952 87.5358 23.3835 87.9082 23.9082C88.2806 24.4245 88.6445 24.8604 89 25.2158C89.3639 25.5798 89.7279 25.859 90.0918 26.0537C90.4557 26.2484 90.8239 26.3457 91.1963 26.3457C91.3148 26.3457 91.429 26.3288 91.5391 26.2949C91.6491 26.2611 91.7464 26.223 91.8311 26.1807C91.9242 26.1383 92.0173 26.0833 92.1104 26.0156L92.5166 26.5107C92.3981 26.5954 92.2288 26.6969 92.0088 26.8154C91.7972 26.9339 91.5348 27.0482 91.2217 27.1582C90.917 27.2682 90.5658 27.3613 90.168 27.4375C89.7702 27.5137 89.3343 27.5518 88.8604 27.5518C88.1748 27.5518 87.5146 27.3825 86.8799 27.0439C86.2536 26.7139 85.6484 26.2568 85.0645 25.6729C84.4805 25.0889 83.9219 24.4076 83.3887 23.6289C82.8555 22.8418 82.3392 21.9997 81.8398 21.1025C81.349 20.2054 80.875 19.2702 80.418 18.2969C79.9609 17.3236 79.5166 16.363 79.085 15.415C78.6618 14.4587 78.2513 13.5361 77.8535 12.6475C77.4642 11.7503 77.0833 10.9294 76.7109 10.1846C76.7109 10.6755 76.7152 11.1833 76.7236 11.708C76.7321 12.2327 76.7363 12.7617 76.7363 13.2949V17.2305C76.7363 17.9329 76.7194 18.6185 76.6855 19.2871C76.6517 19.9557 76.5713 20.5905 76.4443 21.1914C76.3174 21.7839 76.1312 22.334 75.8857 22.8418C75.6488 23.3411 75.3229 23.7728 74.9082 24.1367C74.4935 24.5007 73.9814 24.7842 73.3721 24.9873C72.7627 25.1904 72.0306 25.292 71.1758 25.292C70.7441 25.292 70.3464 25.2581 69.9824 25.1904C69.6185 25.1312 69.3011 25.0423 69.0303 24.9238C68.7679 24.8053 68.5648 24.6615 68.4209 24.4922C68.2686 24.3229 68.1924 24.1283 68.1924 23.9082C68.1924 23.7728 68.2135 23.6331 68.2559 23.4893C68.2897 23.3369 68.3447 23.2015 68.4209 23.083C68.4971 22.9645 68.5902 22.8672 68.7002 22.791C68.8102 22.7064 68.9414 22.6641 69.0938 22.6641C69.1784 22.6641 69.2546 22.7064 69.3223 22.791C69.3984 22.8757 69.4746 22.9814 69.5508 23.1084C69.6354 23.2354 69.7285 23.375 69.8301 23.5273C69.9401 23.6712 70.0671 23.8066 70.2109 23.9336C70.3633 24.0605 70.5452 24.1663 70.7568 24.251C70.9684 24.3356 71.2223 24.3779 71.5186 24.3779C72.2041 24.3779 72.7712 24.2594 73.2197 24.0225C73.6768 23.777 74.0449 23.4342 74.3242 22.9941C74.6035 22.5456 74.8066 22.0081 74.9336 21.3818C75.0605 20.7555 75.141 20.0531 75.1748 19.2744C75.2002 18.7412 75.2171 18.2249 75.2256 17.7256C75.234 17.2262 75.2425 16.7184 75.251 16.2021C75.2594 15.6859 75.2637 15.1484 75.2637 14.5898C75.2637 14.0312 75.2637 13.4346 75.2637 12.7998Z" fill="black"/>
                            <path d="M2.28125 101.75C2.44792 101.75 3.15625 101.719 4.40625 101.656C4.42708 101.719 4.4375 101.828 4.4375 101.984C4.44792 102.141 4.4375 102.224 4.40625 102.234C3.92708 102.234 3.6875 102.411 3.6875 102.766L3.875 103.25L6.75 108.953C6.80208 109.068 6.88021 109.042 6.98438 108.875L7.95312 107.156L6.23438 103.766C5.91146 103.13 5.6875 102.75 5.5625 102.625C5.30208 102.365 5 102.234 4.65625 102.234C4.61458 102.234 4.59375 102.135 4.59375 101.938C4.59375 101.771 4.60417 101.677 4.625 101.656C6.0625 101.719 6.78646 101.75 6.79688 101.75C6.92188 101.75 7.60938 101.719 8.85938 101.656C8.88021 101.719 8.89062 101.828 8.89062 101.984C8.90104 102.141 8.89062 102.224 8.85938 102.234C8.69271 102.234 8.53125 102.276 8.375 102.359C8.21875 102.432 8.14062 102.557 8.14062 102.734L8.32812 103.219L9.23438 105.016L9.98438 103.531C10.1927 103.104 10.2969 102.833 10.2969 102.719C10.2969 102.583 10.2031 102.479 10.0156 102.406C9.83854 102.323 9.67188 102.271 9.51562 102.25C9.36979 102.229 9.24479 102.219 9.14062 102.219C9.09896 102.219 9.07292 102.135 9.0625 101.969C9.0625 101.802 9.08333 101.698 9.125 101.656C10.125 101.719 10.7448 101.75 10.9844 101.75C11.0365 101.75 11.6875 101.719 12.9375 101.656C12.9583 101.719 12.9688 101.828 12.9688 101.984C12.9792 102.13 12.9688 102.208 12.9375 102.219C12.7812 102.219 12.5781 102.271 12.3281 102.375C12.0781 102.469 11.901 102.573 11.7969 102.688C11.5156 102.99 11.224 103.438 10.9219 104.031L9.82812 106.188L11.1875 108.859C11.2396 108.964 11.3073 108.943 11.3906 108.797L14.0625 103.5C14.25 103.125 14.3438 102.865 14.3438 102.719C14.3438 102.542 14.2031 102.417 13.9219 102.344C13.651 102.26 13.4219 102.219 13.2344 102.219C13.1927 102.219 13.1667 102.135 13.1562 101.969C13.1562 101.802 13.1771 101.698 13.2188 101.656C14.2188 101.719 14.8385 101.75 15.0781 101.75C15.1302 101.75 15.7812 101.719 17.0312 101.656C17.0625 101.74 17.0781 101.87 17.0781 102.047C17.0781 102.161 17.0625 102.219 17.0312 102.219C16.875 102.219 16.6771 102.266 16.4375 102.359C16.1979 102.453 16.026 102.552 15.9219 102.656C15.6302 102.969 15.3281 103.427 15.0156 104.031L11.1562 111.703C10.9896 112.036 10.8125 112.203 10.625 112.203C10.5417 112.203 10.4844 112.177 10.4531 112.125L8.53125 108.359L6.65625 111.703C6.46875 112.036 6.29167 112.203 6.125 112.203C6.0625 112.203 6.01562 112.177 5.98438 112.125L1.75 103.766C1.46875 103.214 1.22917 102.812 1.03125 102.562C0.947917 102.458 0.802083 102.38 0.59375 102.328C0.395833 102.266 0.229167 102.234 0.09375 102.234C0.0520833 102.234 0.03125 102.135 0.03125 101.938C0.03125 101.771 0.0416667 101.677 0.0625 101.656C1.5 101.719 2.23958 101.75 2.28125 101.75Z" fill="black"/>
                            <path d="M76.4478 180.359C76.4478 179.578 76.7915 178.911 77.479 178.359C78.1769 177.807 78.9425 177.531 79.7759 177.531C79.9946 177.531 80.2186 177.547 80.4478 177.578C80.6873 177.609 80.8853 177.641 81.0415 177.672C81.2082 177.703 81.4321 177.755 81.7134 177.828C82.005 177.891 82.2342 177.938 82.4009 177.969C82.5155 178.979 82.5728 179.844 82.5728 180.562C82.5311 180.667 82.453 180.719 82.3384 180.719C82.0363 180.719 81.8748 180.682 81.854 180.609C81.7811 180.089 81.5571 179.594 81.1821 179.125C80.8071 178.646 80.3436 178.406 79.7915 178.406C79.4373 178.406 79.1353 178.516 78.8853 178.734C78.6457 178.953 78.5259 179.302 78.5259 179.781C78.5259 179.99 78.578 180.193 78.6821 180.391C78.7863 180.589 78.9582 180.792 79.1978 181C79.4478 181.208 79.6509 181.365 79.8071 181.469C79.9634 181.573 80.2186 181.74 80.5728 181.969C80.6665 182.031 80.7394 182.078 80.7915 182.109C80.8748 182.161 81.0467 182.266 81.3071 182.422C81.5675 182.578 81.7498 182.698 81.854 182.781C81.9686 182.854 82.1196 182.979 82.3071 183.156C82.505 183.333 82.6457 183.51 82.729 183.688C82.8228 183.854 82.9061 184.078 82.979 184.359C83.0519 184.63 83.0884 184.932 83.0884 185.266C83.0884 186.099 82.7446 186.792 82.0571 187.344C81.3696 187.885 80.578 188.156 79.6821 188.156C78.8592 188.156 77.8696 188.021 76.7134 187.75C76.63 187.417 76.5467 186.964 76.4634 186.391C76.3905 185.807 76.354 185.396 76.354 185.156C76.3853 185.052 76.479 185 76.6353 185C76.9582 185 77.1248 185.031 77.1353 185.094C77.2186 185.573 77.479 186.057 77.9165 186.547C78.354 187.026 78.8384 187.266 79.3696 187.266C80.4009 187.266 80.9165 186.698 80.9165 185.562C80.9165 185.385 80.8696 185.214 80.7759 185.047C80.6821 184.87 80.5415 184.698 80.354 184.531C80.1665 184.365 79.9998 184.229 79.854 184.125C79.7082 184.021 79.4998 183.885 79.229 183.719C78.9686 183.552 78.8019 183.443 78.729 183.391C78.6769 183.359 78.5988 183.307 78.4946 183.234C78.1509 183.005 77.8905 182.823 77.7134 182.688C77.5363 182.552 77.328 182.365 77.0884 182.125C76.8592 181.885 76.6925 181.625 76.5884 181.344C76.4946 181.052 76.4478 180.724 76.4478 180.359Z" fill="black"/>
                            <path d="M146.755 98.6292C147.213 98.6292 148.021 98.6187 149.177 98.5979C150.333 98.5667 151.208 98.551 151.802 98.551C151.812 98.6552 151.963 99.426 152.255 100.864C152.203 100.916 152.083 100.942 151.896 100.942C151.781 100.942 151.708 100.905 151.677 100.832C151.479 100.436 151.166 100.098 150.739 99.8167C150.312 99.5354 149.896 99.3948 149.489 99.3948H148.896C148.239 99.3948 147.901 99.4625 147.88 99.5979C147.828 99.8479 147.802 100.254 147.802 100.817V103.223C147.802 103.285 148.026 103.317 148.474 103.317H149.302C149.948 103.317 150.302 103.296 150.364 103.254C150.416 103.212 150.458 103.061 150.489 102.801C150.51 102.645 150.541 102.494 150.583 102.348C150.593 102.275 150.672 102.239 150.817 102.239C150.994 102.239 151.109 102.265 151.161 102.317V105.27C151.119 105.311 151.01 105.332 150.833 105.332C150.698 105.332 150.614 105.291 150.583 105.207C150.541 105.061 150.505 104.936 150.474 104.832C150.442 104.728 150.416 104.65 150.396 104.598C150.375 104.546 150.349 104.499 150.317 104.457C150.297 104.416 150.281 104.39 150.271 104.379C150.26 104.369 150.239 104.358 150.208 104.348C150.177 104.327 150.156 104.311 150.146 104.301C150.01 104.218 149.604 104.176 148.927 104.176C148.177 104.176 147.802 104.223 147.802 104.317V106.723C147.802 107.223 147.843 107.593 147.927 107.832C147.979 107.999 148.333 108.082 148.989 108.082H149.396C150.041 108.082 150.625 107.947 151.146 107.676C151.323 107.582 151.521 107.405 151.739 107.145C151.968 106.884 152.146 106.593 152.271 106.27C152.281 106.207 152.354 106.176 152.489 106.176C152.739 106.176 152.896 106.207 152.958 106.27C152.448 107.707 152.166 108.603 152.114 108.957C151.583 108.957 150.739 108.947 149.583 108.926C148.427 108.905 147.489 108.895 146.771 108.895L144.489 108.989C144.448 108.947 144.427 108.858 144.427 108.723C144.427 108.515 144.448 108.41 144.489 108.41C144.614 108.41 144.807 108.374 145.067 108.301C145.338 108.228 145.489 108.15 145.521 108.067C145.583 107.827 145.614 107.369 145.614 106.692V100.785C145.614 100.212 145.583 99.8062 145.521 99.5667C145.489 99.4833 145.333 99.3896 145.052 99.2854C144.781 99.1812 144.588 99.1292 144.474 99.1292C144.432 99.1292 144.411 99.0354 144.411 98.8479C144.411 98.6812 144.427 98.5823 144.458 98.551C145.291 98.6031 146.057 98.6292 146.755 98.6292Z" fill="black"/>
                        </svg>
                    </a>
                </div>
                <ul class="navbar-nav flex-fill w-100 mt-2">
                    <li class="nav-item">
                        <a href="{{ route('admin') }}" class="nav-link active">
                            <i class="fe fe-home fe-16"></i>
                            <span class="ml-3 item-text">Dashboard</span>
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>

                <p class="text-muted nav-heading mt-4 mb-1">
                    <span>Work</span>
                </p>
                <ul class="navbar-nav flex-fill w-100 mb-2">
                    <li class="nav-item w-100">
                        <a class="nav-link" href="{{ route('admin.quotes') }}">
                            <i class="fe fe-clipboard fe-16"></i>
                            <span class="ml-3 item-text">Quotes</span>
                        </a>
                    </li>
                    <li class="nav-item w-100">
                        <a class="nav-link" href="">
                            <i class="fe fe-credit-card fe-16"></i>
                            <span class="ml-3 item-text">Payment/Invoices</span>
                        </a>
                    </li>
                </ul>

                <p class="text-muted nav-heading mt-4 mb-1">
                    <span>Add & Manage</span>
                </p>
                <ul class="navbar-nav flex-fill w-100">
                    <li class="nav-item dropdown">
                        <a href="#customers" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                            <i class="fe fe-users fe-16"></i>
                            <span class="ml-3 item-text">Customers</span>
                        </a>
                        <ul class="collapse list-unstyled pl-4 w-100" id="customers">
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.customer.create') }}"><span class="ml-1 item-text">Add Customer</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.customers') }}"><span class="ml-1 item-text">Manage Customers</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.customer.trash') }}"><span class="ml-1 item-text">Restore Customers</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#shipments" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                            <i class="fe fe-package fe-16"></i>
                            <span class="ml-3 item-text">Shipments</span>
                        </a>
                        <ul class="collapse list-unstyled pl-4 w-100" id="shipments">
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.shipment.create') }}"><span class="ml-1 item-text">Add Shipment</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.shipments') }}"><span class="ml-1 item-text">Manage Shipments</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.shipment.trash') }}"><span class="ml-1 item-text">Restore Shipments</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#trackings" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                            <i class="fe fe-map-pin fe-16"></i>
                            <span class="ml-3 item-text">Trackings</span>
                        </a>
                        <ul class="collapse list-unstyled pl-4 w-100" id="trackings">
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.trackings') }}"><span class="ml-1 item-text">Manage Trackings</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#vendors" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                            <i class="fe fe-briefcase fe-16"></i>
                            <span class="ml-3 item-text">Vendors</span>
                        </a>
                        <ul class="collapse list-unstyled pl-4 w-100" id="vendors">
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.vendor.create') }}"><span class="ml-1 item-text">Add Vendor</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.vendors') }}"><span class="ml-1 item-text">Manage Vendors</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.vendor.trash') }}"><span class="ml-1 item-text">Restore Vendors</span></a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#services" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link">
                            <i class="fe fe-layers fe-16"></i>
                            <span class="ml-3 item-text">Services</span>
                        </a>
                        <ul class="collapse list-unstyled pl-4 w-100" id="services">
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.service.create') }}"><span class="ml-1 item-text">Add Service</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.services') }}"><span class="ml-1 item-text">Manage Services</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link pl-3" href="{{ route('admin.service.trash') }}"><span class="ml-1 item-text">Restore Services</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </aside>

        <main  role="main" class="main-content py-4">
            @include('layouts._message')
            @yield('content')
        </main>

        <div class="modal fade modal-notif modal-slide" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Notifications</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="list-group list-group-flush my-n3">
                            <div class="list-group-item bg-transparent">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <span class="fe fe-box fe-24"></span>
                                    </div>
                                    <div class="col">
                                        <small><strong>Package has uploaded successfull</strong></small>
                                        <div class="my-0 text-muted small">Package is zipped and uploaded</div>
                                        <small class="badge badge-pill badge-light text-muted">1m ago</small>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item bg-transparent">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <span class="fe fe-download fe-24"></span>
                                    </div>
                                    <div class="col">
                                        <small><strong>Widgets are updated successfull</strong></small>
                                        <div class="my-0 text-muted small">Just create new layout Index, form, table</div>
                                        <small class="badge badge-pill badge-light text-muted">2m ago</small>
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item bg-transparent">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <span class="fe fe-inbox fe-24"></span>
                                    </div>
                                    <div class="col">
                                        <small><strong>Notifications have been sent</strong></small>
                                        <div class="my-0 text-muted small">Fusce dapibus, tellus ac cursus commodo</div>
                                        <small class="badge badge-pill badge-light text-muted">30m ago</small>
                                    </div>
                                </div> <!-- / .row -->
                            </div>
                            <div class="list-group-item bg-transparent">
                                <div class="row align-items-center">
                                    <div class="col-auto">
                                        <span class="fe fe-link fe-24"></span>
                                    </div>
                                    <div class="col">
                                        <small><strong>Link was attached to menu</strong></small>
                                        <div class="my-0 text-muted small">New layout has been attached to the menu</div>
                                        <small class="badge badge-pill badge-light text-muted">1h ago</small>
                                    </div>
                                </div>
                            </div> <!-- / .row -->
                        </div> <!-- / .list-group -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Clear All</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade modal-shortcut modal-slide" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="defaultModalLabel">Shortcuts</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body px-5">
                        <div class="row align-items-center">
                            <div class="col-6 text-center">
                                <div class="squircle bg-success justify-content-center">
                                    <i class="fe fe-cpu fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Control area</p>
                            </div>
                            <div class="col-6 text-center">
                                <div class="squircle bg-primary justify-content-center">
                                    <i class="fe fe-activity fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Activity</p>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6 text-center">
                                <div class="squircle bg-primary justify-content-center">
                                    <i class="fe fe-droplet fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Droplet</p>
                            </div>
                            <div class="col-6 text-center">
                                <div class="squircle bg-primary justify-content-center">
                                    <i class="fe fe-upload-cloud fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Upload</p>
                            </div>
                        </div>
                        <div class="row align-items-center">
                            <div class="col-6 text-center">
                                <div class="squircle bg-primary justify-content-center">
                                    <i class="fe fe-users fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Users</p>
                            </div>
                            <div class="col-6 text-center">
                                <div class="squircle bg-primary justify-content-center">
                                    <i class="fe fe-settings fe-32 align-self-center text-white"></i>
                                </div>
                                <p>Settings</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- .wrapper -->

    <!-- Scripts -->

    <script src="{{ asset('assets/global/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/jquery.stickOnScroll.js') }}"></script>
    <script src="{{ asset('assets/admin/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/simplebar.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/global/js/jquery.customMessage.js') }}"></script>
    <script src="{{ asset('assets/admin/js/tinycolor-min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/config.js') }}"></script>
    @yield('scripts')

    <script src="{{ asset('assets/admin/js/app.js') }}"></script>

</body>
</html>
