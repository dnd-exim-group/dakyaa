<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Dakyaa - Track Shipment</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

        <link href="{{ asset('assets/global/css/feather.css') }}" rel="stylesheet">

        <link href="{{ asset('assets/global/css/custom.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/global/css/media.css') }}" rel="stylesheet">

        <style>
            html {
                scroll-behavior: smooth;

            }
            html,
            body {
                height: 100%;
            }
            body {
                font-family: 'Poppins', sans-serif;
                background-color: #F1FAEE;
            }
        </style>
    </head>
    <body id="main" class="">
        <nav id="header" class="header navbar navbar-expand-lg navbar-light py-3 fixed-header shadow-sm">
            <div class="col-2">
                <a id="menu-btn" class="btn mr-auto" onclick="toggleMenu()">
                    <span class="navbar-toggler-icon"></span>
                </a>
            </div>

            <div id="mobile-menu" class="navbar shadow" style="left: -50%;">
                <div class="py-2">
                    <a class="btn btn-close-menu ml-auto" onclick="toggleMenu()"><i class="fe fe-x fe-24"></i></a>
                </div>
                <ul class="navbar-nav py-2 pl-3">
                    <li class="nav-item active">
                        <a class="nav-link" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#process">Process</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>

            <div class="col-8 menubar">
                <div id="menu" class="mr-2">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link mr-4" href="#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-4" href="#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-4" href="#process">Process</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link mr-4" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
                @if (Route::has('login'))
                    <div class="auth-btns">
                        @auth
                            <a href="{{ url('/home') }}" class="ml-3 text-sm text-gray-700">
                                Dashboard
                            </a>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-login ml-3 text-sm text-gray-700">Log in</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="btn btn-register ml-2 text-sm text-gray-700">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </div>

        </nav>

        <section id="trackPage">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <?php
                                if(!($tracking == null)) {
                            ?>
                                <h1 class="mt-5 mb-4"><i class="fe fe-box mr-2"></i> Your Shipment</h1>
                                <div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div>
                                                <label for="name" class="m-0 text-muted"><small>Customer Name</small></label>
                                                <p id="name" class="text-line">{{ $tracking->shipment->user->firstname . " " . $tracking->shipment->user->lastname; }}</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <label for="booking_date" class="mb-0 text-muted"><small>Booking Date</small></label>
                                                <p class="mb-2 text-line" id="booking_date">{{ $tracking->shipment->created_at->format('d-m-Y') }}</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <label for="" class="m-0 text-muted"><small>Tracking number</small></label>
                                                <p id="tracking_number" class="text-line">{{ $tracking->tracking_number; }}</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <label for="" class="m-0 text-muted"><small>Service</small></label>
                                                <p id="tracking_number" class="text-line">{{ $tracking->shipment->service->name; }}</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <label for="" class="m-0 text-muted"><small>AWB number</small></label>
                                                <p id="tracking_number" class="text-line">{{ $tracking->awb_number ? $tracking->awb_number : '-'; }}</p>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div>
                                                <label for="" class="m-0 text-muted"><small>Shipment Last Status</small></label>
                                                <p id="tracking_number" class="text-line">{{ $tracking->lastStatus()->name; }}</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="" class="m-0 text-muted"><small>Pickup</small></label>
                                            <p id="tracking_number" class="text-line">
                                                <?php
                                                    echo $tracking->shipment->senderAddress->address_line1.', '. $tracking->shipment->senderAddress->address_line2. ', '. $tracking->shipment->senderAddress->city. ', '.
                                                    $tracking->shipment->senderAddress->state_country. ', '.
                                                    $tracking->shipment->senderAddress->country->name. ' '.
                                                    $tracking->shipment->senderAddress->pincode;
                                                ?>
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="" class="m-0 text-muted"><small> Destination</small></label>
                                            <p id="tracking_number" class="text-line">
                                                {{-- {{ $tracking->shipment->receiverAddress->country->name; }} --}}
                                                <?php
                                                    echo $tracking->shipment->receiverAddress->address_line1.', '. $tracking->shipment->receiverAddress->address_line2. ', '. $tracking->shipment->receiverAddress->city. ', '.
                                                    $tracking->shipment->receiverAddress->state_country. ', '.
                                                    $tracking->shipment->receiverAddress->country->name. ' '.
                                                    $tracking->shipment->receiverAddress->pincode;
                                                ?>
                                            </p>
                                        </div>
                                    </div>
                                    <h2 class="mt-4 mb-3"><i class="fe fe-map-pin mr-2"></i> Tracking History</h2>
                                    <div class="row">
                                        <div class="timeline">
                                            <div class="card-body">
                                                <?php
                                                    foreach ($tracking->statuses->reverse() as $status) {
                                                ?>

                                                    <div class="pb-4 timeline-item item-primary">
                                                        <div class="pl-5">
                                                            <div class="mb-2">
                                                                {{$status->pivot->created_at->format('d M, Y')}}
                                                            </div>
                                                            <p class="mb-2 large">{{$status->name}} <span class="badge badge-light">{{$status->created_at->diffForHumans()}}</span></p>
                                                            <div class="text-muted">
                                                                {{$status->pivot->note}}
                                                            </div>
                                                        </div>
                                                    </div>

                                                <?php
                                                    }
                                                ?>
                                            </div> <!-- / .card-body -->
                                        </div>
                                    </div>
                                </div>
                            <?php
                                } else {
                            ?>
                                <div id="shipmentNotFound">
                                    <div class="row text-center">
                                        <div class="col-md-12">
                                            <h2 class="not-found-text my-5">Shipment Not Found!!</h2>
                                            <img class="box-vector" src="{{ asset('assets/images/shipmentnotfound.png') }}" alt="">
                                        </div>
                                    </div>
                                </div>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="mx-2">
                            <h2 class="heading">Reach Us</h2>
                            <p class="text my-4"><i class="fe fe-mail mr-2"></i> support@dakyaa.com</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="footer-social-icons mx-2">
                            <h4 class="_14">Follow us on</h4>
                            <ul class="social-icons">
                                <li><a href="" class="social-icon"> <i class="fa fa-facebook"></i></a></li>
                                <li><a href="" class="social-icon"> <i class="fa fa-instagram"></i></a></li>
                                <li><a href="" class="social-icon"> <i class="fa fa-linkedin"></i></a></li>
                                <li><a href="" class="social-icon"> <i class="fa fa-twitter"></i></a></li>
                                <li><a href="" class="social-icon"> <i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <p class="my-4">© 2021 DAKYAA. All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </body>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <script src="{{ asset('assets/global/js/custom.js') }}"></script>

</html>
