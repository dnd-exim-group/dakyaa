<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\CustomersController;
use App\Http\Controllers\Admin\QuotesController;
use App\Http\Controllers\Admin\ShipmentsController;
use App\Http\Controllers\Admin\ServicesController;
use App\Http\Controllers\Admin\TrackingsController;
use App\Http\Controllers\Admin\VendorsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'isAdmin'], function () {

    // Route::prefix('admin')->group(function () {

        // ADMIN DASHBBOARD
        Route::get('/', function(){
            return view('admin.index');
        })->name('admin');

        // QUOTES
        // Route::get('/quotes', function(){
        //     return view('admin.quote.index');
        // })->name('admin.quotes');
        Route::get('/quotes', [QuotesController::class, 'index'])->name('admin.quotes');

        // CUSTOMER
        Route::get('/customers', [CustomersController::class, 'index'])->name('admin.customers');
        Route::get('/customer/create', [CustomersController::class, 'create'])->name('admin.customer.create');
        Route::post('/customer/store', [CustomersController::class, 'store'])->name('admin.customer.store');
        Route::get('/customer/edit/{user}', [CustomersController::class, 'edit'])->name('admin.customer.edit');
        Route::put('/customer/update/{user}', [CustomersController::class, 'update'])->name('admin.customer.update');
        Route::get('/customer/show/{user}', [CustomersController::class, 'show'])->name('admin.customer.show');

        Route::put('/customer/trash/inactive/{user}',[CustomersController::class, 'inactive'])->name('admin.customer.inactive');
        Route::put('/customer/trash/restore/{user}',[CustomersController::class, 'restore'])->name('admin.customer.restore');
        Route::get('/customer/trash/index', [CustomersController::class, 'customerTrash'])->name('admin.customer.trash');


        // VENDOR
        Route::get('/vendors', [VendorsController::class, 'index'])->name('admin.vendors');
        Route::get('/vendor/create', [VendorsController::class, 'create'])->name('admin.vendor.create');
        Route::post('/vendor/store', [VendorsController::class, 'store'])->name('admin.vendor.store');
        Route::get('/vendor/edit/{vendor}', [VendorsController::class, 'edit'])->name('admin.vendor.edit');
        Route::put('/vendor/update/{vendor}', [VendorsController::class, 'update'])->name('admin.vendor.update');
        Route::get('/vendor/show/{vendor}', [VendorsController::class, 'show'])->name('admin.vendor.show');

        Route::get('/vendor/trash/index', [VendorsController::class, 'vendorTrash'])->name('admin.vendor.trash');
        Route::put('/vendor/trash/inactive/{vendor}', [VendorsController::class, 'inactive'])->name('admin.vendor.inactive');
        Route::put('/vendor/trash/restore/{vendor}', [VendorsController::class, 'restore'])->name('admin.vendor.restore');


        // Shipment
        Route::get('/shipments', [ShipmentsController::class, 'index'])->name('admin.shipments');
        Route::get('/shipment/create', [ShipmentsController::class, 'create'])->name('admin.shipment.create');
        Route::post('/shipment/store', [ShipmentsController::class, 'store'])->name('admin.shipment.store');
        Route::get('/shipment/edit/{shipment}', [ShipmentsController::class, 'edit'])->name('admin.shipment.edit');
        Route::put('/shipment/update/{shipment}', [ShipmentsController::class, 'update'])->name('admin.shipment.update');
        Route::get('/shipment/show/{shipment}', [ShipmentsController::class, 'show'])->name('admin.shipment.show');
        Route::post('/shipment/pending/{shipment}', [ShipmentsController::class, 'shipmentPending'])->name('admin.shipment.pending');
        Route::post('/shipment/approved/{shipment}', [ShipmentsController::class, 'shipmentApproved'])->name('admin.shipment.approved');
        Route::post('/shipment/rejected/{shipment}', [ShipmentsController::class, 'shipmentReject'])->name('admin.shipment.rejected');

        Route::delete('/shipment/delete/{shipment}', [ShipmentsController::class, 'delete'])->name('admin.shipment.delete');
        Route::get('/shipment/trash/index', [ShipmentsController::class, 'shipmentTrash'])->name('admin.shipment.trash');
        Route::put('/shipment/trash/inactive/{shipment}', [ShipmentsController::class, 'inactive'])->name('admin.shipment.inactive');
        Route::put('/shipment/trash/restore/{shipment}', [ShipmentsController::class, 'restore'])->name('admin.shipment.restore');


        //Services
        Route::post('/service/country', [ServicesController::class, 'serviceByCountryId'])->name('ajax.serviceByCountry');
        Route::get('/services', [ServicesController::class, 'index'])->name('admin.services');
        Route::get('/service/create', [ServicesController::class, 'create'])->name('admin.service.create');
        Route::post('/service/store', [ServicesController::class, 'store'])->name('admin.service.store');
        Route::get('/service/show/{service}', [ServicesController::class, 'show'])->name('admin.service.show');
        Route::get('/service/edit/{service}', [ServicesController::class, 'edit'])->name('admin.service.edit');
        Route::put('/service/update/{service}', [ServicesController::class, 'update'])->name('admin.service.update');

        Route::get('/service/trash/index', [ServicesController::class, 'serviceTrash'])->name('admin.service.trash');
        Route::put('/service/trash/inactive/{service}', [ServicesController::class, 'inactive'])->name('admin.service.inactive');
        Route::put('/service/trash/restore/{service}', [ServicesController::class, 'restore'])->name('admin.service.restore');


        //Tracking
        Route::get('/trackings', [TrackingsController::class, 'index'])->name('admin.trackings');
        Route::get('/shipment/tracking/create/{shipment}', [TrackingsController::class, 'create'])->name('admin.tracking.create');
        Route::get('/tracking/edit/{tracking}', [TrackingsController::class, 'edit'])->name('admin.tracking.edit');
        Route::put('/tracking/update/{tracking}', [TrackingsController::class, 'update'])->name('admin.tracking.update');
        Route::get('/tracking/editStatus/{tracking}', [TrackingsController::class, 'editStatus'])->name('admin.tracking.editStatus');
        Route::post('/tracking/store/{shipment}', [TrackingsController::class, 'store'])->name('admin.tracking.store');
        Route::post('/tracking/update-status/{tracking}', [TrackingsController::class, 'updateStatus'])->name('admin.tracking.updateStatus');
        Route::get('/tracking/show/{tracking}', [TrackingsController::class, 'show'])->name('admin.tracking.show');


    // });

});

?>
