<?php

use App\Http\Controllers\Customer\AddressesController;
use App\Http\Controllers\Customer\CustomersController;
use App\Http\Controllers\Customer\QuotesController;
use App\Http\Controllers\Customer\ShipmentsController;
use Illuminate\Support\Facades\Route;

Route::post('quotes/store', [QuotesController::class, 'store'])->name('quotes.store');
// SERVICE
Route::get('services/{quote}', [App\Http\Controllers\Customer\ServicesController::class, 'index'])->name('customer.services.index');

Route::group(['middleware' => ['auth', 'verified']], function () {

    // CUSTOMER END ROUTES
    Route::get('/', function(){
        return view('customer.index');
    })->name('customer');

    // QUOTE
    // Route::resource('quotes', 'Customer\QuotesController')->except(['show', 'edit', 'update']);
    Route::resource('quotes', 'Customer\QuotesController')->except(['show', 'edit', 'store']);
    Route::post('repeat/quote/{quote}', [QuotesController::class, 'repeatQuote'])->name('customer.quote.repeatQuote');
    Route::post('quotes/getQuoteById', [QuotesController::class, 'getQuoteById'])->name('ajax.getQuoteById');



    // SHIPMENT
    Route::get('shipments', [ShipmentsController::class, 'index'])->name('customer.shipments');
    Route::get('shipment/quote/{quote}/service/{service}', [ShipmentsController::class, 'create'])->name('customer.shipment.create');
    Route::post('shipment/store/quote/{quote}/service/{service}', [ShipmentsController::class, 'store'])->name('customer.shipment.store');
    Route::get('shipment/track/{shipment}', [ShipmentsController::class, 'track'])->name('customer.shipment.track');

    // PROFILE
    Route::get('profile', [CustomersController::class, 'index'])->name('customer.profile');
    Route::put('profile/update/{user}', [CustomersController::class, 'update'])->name('customer.profile.update');
    Route::put('profile/updatePassword/{user}', [CustomersController::class, 'updatePassword'])->name('customer.profile.updatePassword');

    // ADDRESS
    // Route::get('profile/address', [AddressesController::class, 'index'])->name('customer.profile.address');
    Route::get('address/create', [AddressesController::class, 'create'])->name('customer.address.create');
    Route::post('address/store', [AddressesController::class, 'store'])->name('customer.address.store');
    Route::get('address/edit/{address}', [AddressesController::class, 'edit'])->name('customer.address.edit');
    Route::put('address/update/{address}', [AddressesController::class, 'update'])->name('customer.address.update');
    Route::delete('address/delete/{address}', [AddressesController::class, 'delete'])->name('customer.address.delete');
    Route::resource('shipments', 'Customer\ShipmentsController')->except(['create', 'store']);

    Route::post('repeat/quote/{quote}', [QuotesController::class, 'repeatQuote'])->name('customer.quote.repeatQuote');

    // Route::get('shipment/show/{shipment}', [ShipmentsController::class, 'show'])->name('customer.shipment.show');
    // Route::get('shipments', [ShipmentsController::class, 'index'])->name('customer.shipment.index');

});
