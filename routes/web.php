<?php

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', [App\Http\Controllers\HomeController::class, 'greet'])->name('greet');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//  TRACKING API ROUTE
Route::get('/tracking/{trackingNumber}', [App\Http\Controllers\Admin\TrackingsController::class, 'trackShipmentByTrackingNumber'])->name('trackShipment');

//  EMAIL VERIFICATION ROUTES
Route::get('/auth/verify', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::get('/auth/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/auth/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');


// Route::get('/auth/verifyPhone', function () {
//     return view('auth.verifyPhone');
// })->name('verify');

Route::post('/auth/verifyPhone/{quote}', [App\Http\Controllers\Customer\QuotesController::class, 'verify'])->name('verifyPhone');


//  SOCIALITE ROUTES
// Login OR SignUp - redirecting from DND Express to Provider(goggle, facebook)
Route::get('/login/{provider}', [App\Http\Controllers\SocialController::class, 'redirect']);

// callback route - Provider will send the response on this route(URL)
Route::get('/login/{provider}/callback', [App\Http\Controllers\SocialController::class, 'Callback']);

// Route::get('/customer/services', function(){
//     return view('customer.services.index');
// })->name('customer.services');

// Route::get('/customer/shipments', function(){
//     return view('customer.shipment.index');
// })->name('customer.shipments');

// Route::get('/customer/profile', function(){
//     return view('customer.profile.index');
// })->name('customer.profile');

// Route::get('/customer/profile/addAddress', function(){
//     return view('customer.profile.address');
// })->name('customer.addAddress');

Route::get('shipments/export', [App\Http\Controllers\Admin\ShipmentsController::class, 'export'])->name('shipments.export');
Route::get('shipments/masterReport', [App\Http\Controllers\Admin\ShipmentsController::class, 'masterReportExport'])->name('shipments.masterReportExport');

// LANDAING PAGE TRACKING ROUTE
Route::get('/track', [App\Http\Controllers\Admin\TrackingsController::class, 'trackShipment'])->name('shipment.track');
